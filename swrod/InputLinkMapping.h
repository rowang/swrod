/*
 * InputLinkMapping.h
 *
 *  Created on: Jan 11, 2022
 *      Author: kolos
 */

#ifndef SWROD_INPUTLINKMAPPING_H_
#define SWROD_INPUTLINKMAPPING_H_

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/property_tree/ptree.hpp>

#include "InputLinkId.h"

namespace swrod {
    namespace mi=boost::multi_index;

    class InputLinkMapping {
    public:
        InputLinkMapping(const boost::property_tree::ptree & config);

        InputLinkId felixId(DetectorLinkId id) const;

        DetectorLinkId detectorResourceId(InputLinkId id) const;

    private:
        struct Link {
            Link(InputLinkId fid, DetectorLinkId did) :
                m_felix_id(fid), m_detector_id(did)
            {}

            InputLinkId m_felix_id;
            DetectorLinkId m_detector_id;
        };

        typedef boost::multi_index_container<
                Link,
                mi::indexed_by<
                  mi::hashed_unique<mi::member<Link, InputLinkId, &Link::m_felix_id>>,
                  mi::hashed_unique<mi::member<Link, DetectorLinkId, &Link::m_detector_id>>
                >
        > Mapping;

        Mapping m_links;
    };
}

#endif /* SWROD_INPUTLINKMAPPING_H_ */

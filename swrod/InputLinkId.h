/*
 * InputLinkId.h
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#ifndef SWROD_INPUTLINKID_H_
#define SWROD_INPUTLINKID_H_

#include <cstdint>
#include <vector>

namespace swrod {
    /**
     * \typedef ID of a FELIX input link.
     */
    typedef uint64_t InputLinkId;

    /**
     * \typedef Detector specific input link ID
     */
    typedef uint32_t DetectorLinkId;

    /**
     * \typedef A vector of input links IDs that defines a mapping of FELIX link IDs to detector IDs.
     */
    typedef std::vector<std::pair<InputLinkId, DetectorLinkId>> InputLinkVector;
}

#endif /* SWROD_INPUTLINKID_H_ */

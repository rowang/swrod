/*
 * CustomProcessor.h
 *
 *  Created on: May 29, 2019
 *      Author: kolos
 */

#ifndef SWROD_CUSTOMPROCESSOR_H_
#define SWROD_CUSTOMPROCESSOR_H_

#include <swrod/Component.h>
#include <swrod/InputLinkId.h>
#include <swrod/ROBFragment.h>

namespace swrod {

    /**
     * This is an abstract interface for detector specific processing of
     * fully built ROB fragments.
     */
    class CustomProcessor: public Component {
    public:
        virtual ~CustomProcessor() = default;

        const std::string& getName() const override {
            static std::string s("ROBProcessor");
            return s;
        }

        /**
         * Called when an input link has been disabled.
         *
         * @param[in] link_id The ID of the input link that has been disabled.
         */
        virtual void linkDisabled(const InputLinkId &link_id) {
        }

        /**
         * Called when an input link has been re enabled.
         *
         * @param[in] link_id The ID of the input link that has been re enabled.
         */
        virtual void linkEnabled(const InputLinkId &link_id) {
        }

        /**
         * This function is used to pass a fully built ROB fragment to this
         * processor. An implementation of this method can modify any non-constant
         * attribute of the ROBFragment object.
         *
         * @param[in, out] fragment ROB fragment to be processed.
         */
        virtual void processROBFragment(ROBFragment &fragment) = 0;
    };
}

#endif /* SWROD_CUSTOMPROCESSOR_H_ */

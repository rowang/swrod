/*
 * CustomProcessingFramework.h
 *
 *  Created on: May 29, 2019
 *      Author: kolos
 */

#ifndef SWROD_CUSTOMPROCESSINGFRAMEWORK_H_
#define SWROD_CUSTOMPROCESSINGFRAMEWORK_H_

#include <map>
#include <memory>
#include <string>
#include <tuple>
#include <optional>

#include <boost/property_tree/ptree.hpp>

#include <swrod/CustomProcessor.h>

namespace swrod {

    /**
     * This class manages custom detector procedures provided by detector plugins.
     */
    class CustomProcessingFramework {
    public:
        typedef std::tuple<uint32_t, uint32_t, uint16_t> (*TriggerInfoExtractor)(const uint8_t *, uint32_t);
        typedef std::optional<bool> (*DataIntegrityChecker)(const uint8_t *, uint32_t);

        /**
         * Creates a new instance of custom processing framework for the given configuration.
         * This constructor loads all the plugins that are defined in the given configuration
         * and resolves the custom functions using the names provided by the plugins configuration.
         *
         * @param[in] config SW ROD configuration.
         */
        explicit CustomProcessingFramework(const boost::property_tree::ptree & config);

        /**
         * Returns a pointer to the trigger information extraction function for the given ROB.
         * Such a function shall be implemented for each input data type and shall be capable of
         * extracting L1ID and BCID values from a data packet of that type.
         *
         * @param[in] ROB_id The ID of the ROB for which the trigger information extraction function
         *              has to be returned
         * @returns Trigger information extraction function for the given ROB.
         * @throws swrod::BadConfigurationException This exception is thrown if no trigger information
         *              extraction function had been previously registered with the given ROB ID.
         */
        TriggerInfoExtractor getTriggerInfoExtractor(uint32_t ROB_id) const;

        /**
         * Returns a pointer to the data integrity check function for the given ROB.
         * This is an optional function that can be implemented for a specific type of input data to
         * be used to check a data packet of this type for corruption. If no data integrity check
         * function is provided by the detector plugin, this function returns a pointer to the default
         * integrity checker that always returns undefined result.
         *
         * @param[in] ROB_id The ID of the ROB for which the data integrity checking function
         *              has to be returned
         * @returns Data integrity check function for the given ROB.
         */
        DataIntegrityChecker getDataIntegrityChecker(uint32_t ROB_id) const noexcept;

        /**
         * Creates a new instance of the custom processing procedure for the given ROB. This
         * procedure will be applied to all data fragments produced for the given ROB. Note that
         * for scalability reasons multiple instances of custom processor can be created for
         * the same ROB.
         *
         * @param[in] ROB_id The ID of the ROB for which a new custom processor has to be created.
         * @param[in] config Configuration to be passed to the new custom processor constructor.
         * @returns A new custom processor instance.
         * @throws swrod::BadConfigurationException This exception is thrown if no custom processor
         *      factory had been previously registered with the given ROB ID.
         */
        std::unique_ptr<CustomProcessor> createCustomProcessor(uint32_t ROB_id,
                const boost::property_tree::ptree & config) const;

    private:
        typedef CustomProcessor*(*ProcessorFactory)(const boost::property_tree::ptree &);

        class SharedLibrary {
        public:
            /**
             * Loads the given shared library and resolves the given functions.
             *
             * @param library_name The name of the shared library to be loaded.
             * @param trigger_info_extractor_name The name of the trigger information extraction function.
             * @param processor_factory_name The name of the custom processor factory function.
             */
            SharedLibrary(const std::string & library_name,
                    const std::string & trigger_info_extractor_name,
                    const std::string & processor_factory_name,
                    const std::string & data_integrity_checker_name);

            /** Unload this dynamic library  */
            ~SharedLibrary();

            TriggerInfoExtractor getTriggerInfoExtractor() const {
                return m_tie_function;
            }

            ProcessorFactory getProcessingFactory() const {
                return m_pf_function;
            }

            DataIntegrityChecker getDataIntegrityChecker() const {
                return m_dic_function;
            }

        private:
            const std::string m_name;
            void * m_handle;
            TriggerInfoExtractor m_tie_function;
            ProcessorFactory m_pf_function;
            DataIntegrityChecker m_dic_function;
        };

        std::shared_ptr<SharedLibrary> loadSharedLibrary(const std::string & library_name,
                const std::string & trigger_info_extractor_name,
                const std::string & processor_factory_name,
                const std::string & data_integrity_checker_name);

    private:
        typedef std::map<uint32_t, TriggerInfoExtractor> ExtractorsMap;
        typedef std::map<uint32_t, ProcessorFactory> FactoriesMap;
        typedef std::map<uint32_t, DataIntegrityChecker> CheckersMap;
        typedef std::map<std::string, std::shared_ptr<SharedLibrary>> LibrariesMap;

        static DataIntegrityChecker s_default_data_integrity_checker;

        ExtractorsMap m_trigger_info_extractors;
        FactoriesMap m_processing_factories;
        CheckersMap m_data_intergrity_checkers;
        LibrariesMap m_libraries;
    };
}

#endif /* SWROD_CUSTOMPROCESSINGFRAMEWORK_H_ */

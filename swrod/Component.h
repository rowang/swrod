/*
 * Component.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_COMPONENT_H_
#define SWROD_COMPONENT_H_

#include <rc/RunParams.h>
#include <RunControl/Common/RunControlCommands.h>

class ISInfo;

namespace swrod {
    /**
     * This class defines a basic interface for SW ROD a internal component.
     *
     */
    class Component {
    public:
        virtual ~Component() = default;

        /**
         * A subclass shall override this function to return a string ID of the
         * current object. This ID has to be unique in the scope of the current
         * SW ROD application if getStatistics() function of the current object
         * returns non-null pointer to an IS information object. This will guarantee
         * that this IS object can be published correctly. For objects that don't
         * provide statistics to be published to IS this ID is not required
         * to be unique.
         *
         * @return The object ID
         */
        virtual const std::string & getName() const = 0;

        /**
         * Returns monitoring statistics for this component.
         * Default implementation returns null pointer.
         *
         * @return A pointer to the IS statistics object.
         */
        virtual ISInfo * getStatistics() { return nullptr; }

        /**
         * Called to inform this component that a new run is about to be started.
         *
         * @param[in] run_params The new run parameters.
         */
        virtual void runStarted(const RunParams & run_params) {}

        /**
         * Called to inform this component that the current run is being stopped.
         */
        virtual void runStopped() {}

        /**
         * Called to pass a custom Run Control command to this component.
         *
         * @param[in] cmd user defined Run Control command.
         */
        virtual void userCommand(const daq::rc::UserCmd & cmd) {}
    };
}

#endif /* SWROD_COMPONENT_H_ */

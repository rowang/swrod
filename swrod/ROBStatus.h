/*
 * ROBStatus.h
 *
 *  Created on: Nov 20, 2020
 *      Author: kolos
 */

#ifndef SWROD_ROBSTATUS_H_
#define SWROD_ROBSTATUS_H_

#include <cstdint>

namespace swrod {
    const uint32_t RODHeaderTrailerSize = 12;

    namespace ROBStatus {
        namespace Generic {
            const uint32_t
            BCIDCheckFailed = 1<<0,
            L1IDCheckFailed = 1<<1,
            DataTimedout = 1<<2,
            CorruptData = 1<<3,
            BufferOverflow = 1<<4,
            DummyData = 1<<5;
        }

        namespace Specific {
            namespace Transmission {
                const uint32_t
                SoftwareTrunc = 1<<16,
                CRCError = 1<<18,
                SoftwareMalf = 1<<19,
                FirmwareTrunc = 1<<20,
                FirmwareMalf = 1<<21;
            }
            namespace Format {
                const uint32_t
                FragmentSizeError = 1<<17,
                HeaderMarkerError = 1<<22,
                MajorVersionError = 1<<23;
            }
            const uint32_t
            DuplicateEvent = 1<<24,
            SequenceError = 1<<25,
            TxError = 1<<26,
            Truncated = 1<<27,
            ShortFragment = 1<<28,
            Lost = 1<<29,
            Pending = 1<<30,
            Discard = 1<<31;
        }
    }
}

#endif /* SWROD_ROBSTATUS_H_ */

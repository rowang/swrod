/*
 * Barrier.h
 *
 *  Created on: Jun 17, 2019
 *      Author: kolos
 */

#ifndef SWROD_TEST_BARRIER_H_
#define SWROD_TEST_BARRIER_H_

#include <cstdint>

#include <mutex>
#include <condition_variable>

namespace swrod {
    namespace test {
        /**
         * Helper class used by the test applications to synchronise threads that are used for
         * generating input data for the SW ROD.
         */
        class Barrier {
        public:
            explicit Barrier(uint32_t thread_number) :
                m_thread_count(thread_number), m_counter(thread_number), m_generation(0), m_active(true) {
            }

            ~Barrier() {
                deactivate();
            }

            void wait() {
                std::unique_lock<std::mutex> lock(m_mutex);
                if (!m_active) {
                    return;
                }
                auto gen = m_generation;
                if (!--m_counter) {
                    m_generation++;
                    m_counter = m_thread_count;
                    m_condition.notify_all();
                } else {
                    m_condition.wait(lock, [&] {return gen != m_generation || !m_active;});
                }
            }

            void deactivate() {
                std::unique_lock<std::mutex> lock(m_mutex);
                m_active = false;
                m_condition.notify_all();
            }

            void reactivate() {
                std::unique_lock<std::mutex> lock(m_mutex);
                if (!m_active) {
                    m_active = true;
                    m_counter = m_thread_count;
                    m_generation = 0;
                }
            }

            uint32_t getThreadNumber() const {
                return m_thread_count;
            }

        private:
            std::mutex m_mutex;
            std::condition_variable m_condition;
            const uint64_t m_thread_count;
            uint64_t m_counter;
            uint64_t m_generation;
            bool m_active;
        };
    }
}

#endif /* SWROD_TEST_BARRIER_H_ */

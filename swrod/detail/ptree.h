/*
 * ptree.h
 *
 *  Created on: Oct 14, 2020
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_PTREE_H_
#define SWROD_DETAIL_PTREE_H_

#include <boost/property_tree/ptree.hpp>
#include <swrod/exceptions.h>

#define PTREE_GET_VALUE(node, type, parameter) [&]() { \
    try { \
        return node.get<type>(parameter); \
    } catch (std::exception & ex) { \
        throw swrod::GetParameterException(ERS_HERE, \
                node.count("UID") ? node.get<std::string>("UID") : std::string("Unknown"), \
                        parameter, ex); \
    } \
}()

#define PTREE_GET_CHILD(node, parameter) [&]() { \
    try { \
        return node.get_child(parameter); \
    } catch (std::exception & ex) { \
        throw swrod::GetParameterException(ERS_HERE, \
                node.count("UID") ? node.get<std::string>("UID") : std::string("Unknown"), \
                        parameter, ex); \
    } \
}()

#endif /* SWROD_DETAIL_PTREE_H_ */

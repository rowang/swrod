/*
 * WaitFor.h
 *
 *  Created on: Mar 19, 2022
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_WAITFOR_H_
#define SWROD_DETAIL_WAITFOR_H_

#include <chrono>
#include <thread>

namespace swrod {
    namespace detail {
        namespace ch = std::chrono;

        template <class Rep, class Period, class Predicate,
                  class RepS = Rep, class PeriodS = Period>
        bool waitFor(const ch::duration<Rep, Period>& rel_time,
                    Predicate condition,
                    const ch::duration<RepS, PeriodS>& sleep_time = ch::duration<Rep, Period>(1)) {
            auto passed_since = [](const ch::time_point<ch::steady_clock> & start) {
                return ch::duration_cast<ch::duration<Rep, Period>>(
                        ch::steady_clock::now() - start);
            };
            if (condition()) {
                return true;
            }
            auto start = ch::steady_clock::now();
            while (passed_since(start) < rel_time) {
                if (condition()) {
                    return true;
                }
                std::this_thread::sleep_for(sleep_time);
            }
            return false;
        }
    }
}

#endif /* SWROD_DETAIL_WAITFOR_H_ */

/*
 * Timer.h
 *
 *  Created on: Dec 7, 2021
 *      Author: kolos
 */

#ifndef SWROD_DETAIL_TIMER_H_
#define SWROD_DETAIL_TIMER_H_

#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>

#include "WorkerThread.h"

namespace swrod {
    namespace detail {
        class Timer {
        public:
            typedef std::function<void()> Callback;

            Timer(const Callback &callback, uint32_t timeout_ms,
                    const std::string & name = "Timer") :
                    m_timeout_ms(timeout_ms),
                    m_callback(callback),
                    m_thread([this](const bool &) { run(); }, name) {
            }

            ~Timer() {
                stop();
            }

            void start() {
                if (m_timeout_ms) {
                    m_stop = false;
                    m_thread.start();
                }
            }

            void stop() {
                {
                    std::scoped_lock lock(m_mutex);
                    m_stop = true;
                }
                m_condition.notify_one();
                m_thread.stop();
            }

        private:
            void run() {
                using namespace std::chrono;

                std::unique_lock lock(m_mutex);

                while (not m_stop) {
                    time_point<steady_clock> timeout(
                            steady_clock::now() + milliseconds(m_timeout_ms));

                    if (not m_condition.wait_until(lock, timeout, [this]{return m_stop;})) {
                        m_callback();
                    }
                }
            }

            const uint32_t m_timeout_ms;
            Callback m_callback;
            bool m_stop = false;
            std::mutex m_mutex;
            std::condition_variable m_condition;
            detail::WorkerThread m_thread;
        };
    }
}

#endif /* SWROD_DETAIL_TIMER_H_ */

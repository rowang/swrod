/*
 * Core.h
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#ifndef SWROD_CORE_H_
#define SWROD_CORE_H_

#include <algorithm>
#include <iterator>
#include <memory>
#include <map>
#include <set>
#include <string>
#include <vector>
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>

#include <RunControl/Common/CommandSender.h>
#include <RunControl/Common/RunControlCommands.h>
#include <rc/RunParams.h>

#include <swrod/exceptions.h>
#include <swrod/CustomProcessingFramework.h>
#include <swrod/DataInput.h>
#include <swrod/InputLinkId.h>
#include <swrod/InputLinkMapping.h>
#include <swrod/L1AInputHandler.h>
#include <swrod/ROBFragmentBuilder.h>
#include <swrod/ROBFragmentConsumer.h>
#include <swrod/detail/PluginManager.h>

class ISInfo;

namespace swrod {

    std::ostream & operator<<(std::ostream & out, const boost::property_tree::ptree & config);

    /**
     * This is a top level class of a SW ROD application. A SW ROD application
     * shall create exactly one instance of this class with a desired configuration.
     */
    class Core {
    public:
        typedef std::vector<std::shared_ptr<ROBFragmentBuilder>> FragmentBuilders;
        typedef std::vector<std::shared_ptr<ROBFragmentConsumer>> FragmentConsumers;

        /**
         * Creates a new instance of SW ROD using the given configuration.
         *
         * @param[in] config SW ROD configuration.
         */
        explicit Core(const boost::property_tree::ptree & config);

        ~Core();

        /**
         * The Core instance passes this request to all implementations of the DataInputHanler
         * interface instructing them to connect to the read-out system. If any connection
         * attempt fails this function will throw a swrod::Exception exception. This function
         * is expected to be called from the Run Control CONNECT transition.
         */
        void connectToFelix();

        /**
         * The Core instance passes this request to all implementations of the
         * DataInputHanler interface instructing them to disconnect from the read-out
         * system. This function should be called from the Run Control DISCONNECT transition.
         */
        void disconnectFromFelix();

        /**
         * This function demands the Core instance to stop receiving data packets
         * from the given input links.
         * The Core passes this request to all implementations of the DataInputHanler
         * interface which typically have to unsubscribe from these input links.
         * The Core also informs all ROB Fragment Consumers that the given set of
         * links has been disabled. This function can be called at any moment and
         * does not require putting the Trigger on hold.
         *
         * @param[in, out] link_ids A vector of input link IDs to be disabled.
         *      The function may modify this vector by removing IDs of the links,
         *      for which recovery attempt has failed.
         */
        void disableInputLinks(std::vector<InputLinkId> & link_ids);

        /**
         * Demands the Core instance to stop producing data fragments for the given ROB(s).
         * The Core instance passes this request to the corresponding ROB Fragment Builder
         * components, which typically unsubscribe from all input links in order to stop
         * getting data. The Core also informs all ROB Fragment Consumers that the given
         * set of ROBs has been disabled. This function can be called at any moment and
         * does not require putting the Trigger on hold.
         *
         * @param[in, out] ROB_ids IDs of the ROBs that have to be disabled. The function
         *      may modify this vector by removing IDs of the ROBs, which couldn't be disabled.
         */
        void disableROBs(std::vector<uint32_t> & ROB_ids);

        /**
         * This function shall be used to request the Core instance to start getting data
         * from the input links that had been previously disabled. The Core passes
         * this request to all implementations of the DataInputHanler interface which
         * typically have to re subscribe to the given input links. The Core also informs
         * all ROB Fragment Consumers that the given set of links has been re enabled.
         * The Trigger must be put on hold before this function is called and therefore no
         * new data shall be produced until this function returns. A failure to respect this
         * condition may result in a fatal crash of the SW ROD application.
         *
         * @param[in, out] link_ids A vector of input link IDs to be re enabled.
         *      The function may modify this vector by removing IDs of the links,
         *      for which recovery attempt has failed.
         * @param[in] lastL1ID The last L1ID that has been produced by the Trigger
         *      before it was put on hold.
         */
        void enableInputLinks(std::vector<InputLinkId> & link_ids, uint32_t lastL1ID);

        /**
         * Demands the Core instance to restart data fragments production for the given ROBs.
         * The Core passes this request to the corresponding ROB Fragment Builders, which
         * typically re subscribe to all input links in order to start getting data. The
         * Core also informs all ROB Fragment Consumers that the given set of ROBs has been
         * enabled.
         * The Trigger must be put on hold before this function is called and therefore no
         * new data shall be produced until this function returns. A failure to respect this
         * condition may result in a fatal crash of the SW ROD application.
         *
         * @param[in, out] ROB_ids A vector of ROB IDs to be re enabled. The function
         *      can modify this vector by removing IDs of the ROBs, for which an attempt
         *      of recovery fails.
         * @param[in] lastL1ID The last L1ID that has been produced by the Trigger
         *      before it was put on hold.
         */
        void enableROBs(std::vector<uint32_t> & ROB_ids, uint32_t lastL1ID);

        /**
         * Returns a reference to the vector containing all ROB Fragment Builders
         * for the current configuration.
         *
         * @returns A vector of ROBFragmentBuilder interface implementations.
         */
        const FragmentBuilders& getBuilders() const {
            return m_all_builders;
        }

        /**
         * Returns a reference to the configuration object that was passed to the constructor of
         * this Core instance.
         *
         * @returns The original configuration object that was used for creating this Core instance.
         */
        const boost::property_tree::ptree& getConfiguration() const {
            return m_configuration;
        }

        /**
         * Returns a reference to the vector containing all ROB Fragment Consumers
         * for the current configuration.
         *
         * @returns A vector of ROBFragmentConsumer interface implementations.
         */
        const FragmentConsumers& getConsumers() const {
            return m_all_consumers;
        }

        /**
         * Returns input objects to be used for receiving input data for the given ROB.
         *
         * @param[in] ROB_id
         * @return A vector of input objects that are used for receiving input data for the given ROB.
         * @throws BadConfigurationException if no input objects are found for the given ROB ID
         */
        const std::vector<std::shared_ptr<DataInput>> & getInputs(uint32_t ROB_id) const;

        /**
         * Returns a mapping of FELIX IDs of input links to detector specific IDs.
         *
         * @return An object that can be used to find detector resource ID by FELIX ID and vice versa.
         */
        const InputLinkMapping & getLinkMapping() const {
            return m_mapping;
        }

       /**
         * Returns a reference to the CustomProcessingFramework object.
         *
         * @returns A reference to the CustomProcessingFramework object.
         */
        const CustomProcessingFramework& getProcessingFramework() const {
            return m_processing_framework;
        }

        /**
         * Returns a vector of pointers to monitoring statistics objects for all SW ROD
         * internal components (ROB Fragment Builders and Fragment Consumers). It is guaranteed
         * that this vector will contain no null pointers.
         *
         * @returns A vector of monitoring statistics objects.
         */
        std::vector<ISInfo*>  getStatistics();

        /**
         * This function has to be called when a new run is about to be started.
         * It passes the new run parameters to all internal components.
         *
         * @param[in] run_params The new run parameters.
         */
        void runStarted(const RunParams & run_params);

        /**
         * This function has to be called when the current run is being stopped.
         * It passes this notification to all internal components.
         */
        void runStopped();

        /**
         * This function shall be used as part of TTC Restart procedure to re synch
         * SW ROD Fragment Builders after the last restart of the SW ROD application.
         * The Trigger must be put on hold before this function is called and therefore no
         * new data shall be produced until this function returns. A failure to respect this
         * condition may result in a fatal crash of the SW ROD application.
         *
         * @param[in] lastL1ID The last L1ID that has been produced when the Trigger was put on hold.
         */
        void resynchAfterRestart(uint32_t lastL1ID);

        /**
         * This function passes a given Run Control user command to all internal components
         * (ROB Fragment Builders and Fragment Consumers).
         *
         * @param[in] cmd The Run Control user command.
         */
        void userCommand(const daq::rc::UserCmd & cmd);

    private:
        class RCStatusChecker {
        public:
            RCStatusChecker(const std::string & partition_name,
                    const std::string app_name) :
                m_rc_sender(IPCPartition(partition_name), app_name) {}

            bool inRunningState(const std::string & controller_name);

        private:
            daq::rc::CommandSender m_rc_sender;
        };

        struct ROB {
            const uint32_t m_id;
            const std::string m_TTC_controller_name;
            const bool m_update_ECR;
            bool m_enabled = true;
            FragmentConsumers m_consumers;
            std::shared_ptr<ROBFragmentBuilder> m_builder;
            std::set<InputLinkId> m_disabled_elinks;

            void addConsumer(const std::shared_ptr<ROBFragmentConsumer> &c);
            void disable();
            void disableLinks(std::vector<InputLinkId> & links);
            void enable(uint32_t lastL1ID);
            void enableLinks(std::vector<InputLinkId> & links, uint32_t lastL1ID);
            void runStarted(const RunParams & run_params);
            void runStopped();
        };

        struct Module {
            typedef std::vector<std::shared_ptr<DataInput>> Inputs;
            typedef std::map<uint32_t, ROB> ROBMap;

            RCStatusChecker m_status_checker;
            std::string m_TTC_controller_name;
            std::optional<bool> m_update_ECR;
            std::unique_ptr<L1AInputHandler> m_l1a_handler;
            Inputs m_inputs;
            ROBMap m_ROBs;

            Module(const std::string & partition_name, const std::string & app_name) :
                m_status_checker(partition_name, app_name) {}

            void addConsumer(const std::shared_ptr<ROBFragmentConsumer> & c);
            ROB & addROB(ROB && rob);
            bool containsROB(uint32_t ROB_id) const;
            void disableLinks(std::vector<InputLinkId> & link_ids);
            bool disableROB(uint32_t ROB_id);
            void enableLinks(std::vector<InputLinkId> & link_ids, uint32_t lastL1ID);
            bool enableROB(uint32_t ROB_id, uint32_t lastL1ID);
            void resynchAfterRestart(uint32_t lastL1ID);
            void runStarted(const RunParams & run_params);
            void runStopped();
            void synchronise(uint8_t ECR);

            template <class M, class...Args>
            void call(M &&method, Args&&...args) {
                if (m_l1a_handler) {
                    ((*m_l1a_handler).*method)(std::forward<Args>(args)...);
                }
                for (auto & pair : m_ROBs) {
                    ((*pair.second.m_builder).*method)(std::forward<Args>(args)...);
                }
            }
        };

        boost::property_tree::ptree m_configuration;
        InputLinkMapping m_mapping;
        detail::PluginManager m_plugin_manager;
        CustomProcessingFramework m_processing_framework;
        FragmentBuilders m_all_builders;
        FragmentConsumers m_all_consumers;
        std::vector<Module> m_modules;
    };
}

#endif /* SWROD_CORE_H_ */

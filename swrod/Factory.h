/*
 * Factory.h
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */

#ifndef SWROD_FACTORY_H_
#define SWROD_FACTORY_H_

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <type_traits>

#include <boost/property_tree/ptree.hpp>

#include <swrod/exceptions.h>

namespace swrod {
    class Core;
    class L1AInputHandler;

    template <class T> struct IsUnique { static constexpr bool value = false; };
    template <> struct IsUnique<L1AInputHandler> { static constexpr bool value = true; };

    /**
     * This is a helper class that can be used to register a factory of a custom implementation
     * of a SW ROD component interface. This can be done by declaring a global instance of the
     * Factory::Registrator class in one of the compilation modules.
     */
    template <class Product>
    class Factory {
    public:
        typedef typename std::conditional<IsUnique<Product>::value,
                std::unique_ptr<Product>, std::shared_ptr<Product>>::type ReturnType;

        typedef std::function<
                ReturnType(const boost::property_tree::ptree &, const Core &)> Creator;

        struct Registrator {
            /**
             * Use this constructor to declare an instance that registers a new factory.
             *
             * @param type The type name of the new factory.
             * @param creator The factory procedure that can be used to create an instance
             *  of a given class.
             */
            Registrator(const std::string & type, const Creator & creator) :
                m_type(type) {
                Factory::instance().registerCreator(type, creator);
            }

            ~Registrator() {
                Factory::instance().unregisterCreator(m_type);
            }
        private:
            const std::string m_type;
        };

        static Factory & instance() {
            static Factory * factory = new Factory(); // this will cause memory leak but we don't care
            return *factory;
        }

        ReturnType create(const std::string & type,
                const boost::property_tree::ptree & config, const Core & core) {
            std::unique_lock lock(m_mutex);
            auto it = m_creators.find(type);
            if (it != m_creators.end()) {
                return it->second(config, core);
            }
            throw swrod::BadConfigurationException(ERS_HERE, "No '" + type + "' Factory is found");
        }

        void registerCreator(const std::string & type, const Creator & creator) {
            std::unique_lock lock(m_mutex);
            if (not m_creators.count(type)) {
                m_creators[type] = creator;
                ERS_DEBUG(1, "'" << type << "' object factory has been registered");
            } else {
                ers::error(swrod::BadConfigurationException(
                        ERS_HERE, "The '" + type + "' Factory is already registered"));
            }
        }

        void unregisterCreator(const std::string & type) {
            std::unique_lock lock(m_mutex);
            if (m_creators.count(type)) {
                m_creators.erase(type);
                ERS_DEBUG(1, "'" << type << "' object factory has been unregistered");
            } else {
                ERS_LOG("'" << type << "' object factory is not known");
            }
        }

    private:
        Factory() = default;

    private:
        std::mutex m_mutex;
        std::map<std::string, Creator> m_creators;
    };
}

#endif /* SWROD_FACTORY_H_ */

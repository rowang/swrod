# NSW specific ROD data parser

#import struct
from collections import namedtuple

Packet = namedtuple('Packet', 'elink l1id_mask l1id bcid')

def parse_rod_payload(rod_data):
    # rod_data is a list of u32 values
    # It can be converted to list of bytes if necessary
    # rod_data_bytes = list(struct.pack("{}I".format(len(rod_data)), *rod_data))

    result = []
    i = 0
    while i < len(rod_data)-2:
        # packet header
        size = rod_data[i] & 0xffff
        elink = rod_data[i + 1]

        if (rod_data[i + 2] >> 30) & 1:  # null packet
            l1id = (rod_data[i + 2] >> 16) & 0xff
            result.append(Packet(elink, 0xff, l1id, 0xffff))
        else:
            l1id = rod_data[i + 2] & 0xffff
            bcid = (rod_data[i + 2] >> 16) & 0x0fff
            result.append(Packet(elink, 0xffff, l1id, bcid))
        i += (size+3)//4
    return result

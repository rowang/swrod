/*
 * fragment_building_timeout.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#include <iostream>
#include <iterator>
#include <vector>

#include <boost/bind/bind.hpp>
#include <boost/test/included/unit_test.hpp>
#include <boost/test/parameterized_test.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <ipc/core.h>

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/Factory.h>

#include "core/ROBFragmentValidator.h"
#include "test/core/InternalL1AGenerator.h"

using namespace boost::placeholders;
using namespace boost::unit_test;
using namespace boost::property_tree;
using namespace swrod;
using namespace swrod::detail;
using namespace swrod::test;

const uint64_t L1ALinkID = 12345;

ptree createConfiguration(uint32_t timeout, uint32_t buffer_size,
        uint32_t events_number, uint32_t links_number, uint32_t workers_number,
        uint32_t ECR_interval, const std::vector<uint64_t> & bad_links = {})
{
    ptree configuration;
    configuration.add("Partition.Name", "initial");
    configuration.add("Application.Name", "unit_test_data_assembling");
    configuration.add("ISServerName", "DF");

    ptree plugins;

    ptree plugin1;
    plugin1.add("LibraryName", "libswrod_core_unit_test.so");
    plugins.add_child("Plugin", plugin1);
    ptree plugin2;
    plugin2.add("LibraryName", "libswrod_core_test.so");
    plugins.add_child("Plugin", plugin2);
    ptree plugin3;
    plugin3.add("LibraryName", "libswrod_core_impl.so");
    plugins.add_child("Plugin", plugin3);
    configuration.add_child("Plugins", plugins);

    const uint32_t packet_size = 40;
    ptree input;
    input.add("Type", "TestData");
    input.add("PacketSize", packet_size);
    for (auto l : bad_links) {
        input.add("links_dropped.link", l);
    }
    configuration.add_child("InputMethod", input);

    ptree rob;
    rob.add("Id", 12345);
    rob.add("CPU", "");

    ptree event_builder;
    event_builder.add("Type", "GBTModeBuilder");
    event_builder.add("BuildersNumber", 0); // must be zero as otherwise events counting may be broken
    event_builder.add("MaxMessageSize", packet_size + 4);
    event_builder.add("BufferSize", buffer_size);
    event_builder.add("MinimumBufferSize", 1024);
    event_builder.add("L1AWaitTimeout", 1000);
    event_builder.add("ResynchTimeout", 1000);
    event_builder.add("DataReceivingTimeout", timeout);
    event_builder.add("ReadyQueueSize", 10000);
    event_builder.add("RecoveryDepth", 10);
    event_builder.add("FlushBufferAtStop", false);
    event_builder.add("DropCorruptedPackets", false);
    rob.add_child("FragmentBuilder", event_builder);

    ptree custom_proc;
    custom_proc.add("LibraryName", "libswrod_test_plugin.so");
    custom_proc.add("TrigInfoExtractor", "testTriggerInfoExtractor");
    custom_proc.add("DataIntegrityChecker", "testDataIntegrityChecker");
    custom_proc.add("ProcessorFactory", "createTestCustomProcessor");

    ptree data_channel;
    data_channel.add("TTCControllerName", "RootController");
    data_channel.add("UpdateECRCounter", true);
    for (size_t e = 0; e < links_number; ++e) {
        ptree link;
        link.add("FelixId", e);
        link.add("DetectorResourceId", e);
        data_channel.add_child("Contains.InputLink", link);
    }
    data_channel.add_child("CustomLib", custom_proc);

    rob.add_child("Contains", data_channel);

    ptree consumers;
    ptree validator;
    validator.add("Type", "ROBFragmentValidator");
    validator.add("FlushBufferAtStop", false);
    validator.add("QueueSize", 10000);
    validator.add("WorkersNumber", 1);
    validator.add("CPU", "");
    consumers.add_child("Consumer", validator);
    rob.add_child("Consumers", consumers);

    ptree module;
    module.add("WorkersNumber", workers_number);
    module.add("CPU", "");
    module.add_child("ROBs.ROB", rob);
    configuration.add_child("Modules.Module", module);

    ptree l1a_input;
    l1a_input.add("Type", "InternalL1A");
    l1a_input.add("EcrInterval", ECR_interval);
    l1a_input.add("EventsNumber", events_number);
    l1a_input.add("IsSingleton", true);

    ptree l1a_config;
    l1a_config.add("Type", "L1AInputHandler");
    l1a_config.add("Link", L1ALinkID);
    l1a_config.add("CPU", "");
    l1a_config.add("ResynchTimeout", 1000);
    l1a_config.add_child("InputMethod", l1a_input);
    configuration.add_child("L1AHandler", l1a_config);

    return configuration;
}

namespace boost {
    namespace unit_test {
        template<class T>
        std::ostream& operator<<(std::ostream& out, const std::vector<T> & v) {
            out << '{' << std::hex;
            for (auto e : v) {
                out << "0x" << e << ", ";
            }
            if (!v.empty()) {
                out << "\b\b";
            }
            out << '}' << std::dec;
            return out;
        }
    }
}

const uint32_t workers_number = 2;
const uint32_t events_number = 0xffff;
const uint32_t buffer_size = 0xffff;
const uint32_t ECR_interval = 2000;
const uint32_t links_number = 100;

void run_test(uint32_t timeout, const std::vector<uint64_t> & bad_links)
{
    ptree config = createConfiguration(timeout, buffer_size,
            events_number, links_number, workers_number, ECR_interval, bad_links);

    BOOST_TEST_MESSAGE("Data aggregation timeout test with with timeout = " << timeout
            << " and disabled links = " << bad_links);

    uint32_t totalFragments = timeout || bad_links.empty() ? events_number : 0;
    uint32_t corruptFragments = bad_links.empty() ? 0 : totalFragments;
    uint32_t totalPackets = totalFragments * links_number;
    uint32_t receivedPackets = totalPackets - totalFragments * bad_links.size();
    uint32_t missedPackets = totalPackets - receivedPackets;

    try {
        Core core(config);
        std::vector<std::shared_ptr<ROBFragmentValidator>> validators;
        for (auto c : core.getConsumers()) {
            validators.push_back(
                    std::static_pointer_cast<ROBFragmentValidator>(c));
        }

        std::shared_ptr<InternalL1AGenerator> l1aGen = InternalL1AGenerator::instance();

        core.connectToFelix();

        RunParams rp;
        rp.run_number = 0;
        core.runStarted(rp);

        l1aGen->runStopped();

        if (timeout > 333) {
            usleep(timeout * 1000 * 3);
        } else {
            usleep(1000000);
        }

        uint32_t v_fragments = 0, v_packets = 0, v_missed_packets = 0, v_corrupt_fragments = 0;
        for (auto v : validators) {
            v->runStopped();
            v_fragments += v->getFragmentsNumber();
            v_packets += v->getPacketsNumber();
            v_missed_packets += v->getMissedPacketsNumber();
            v_corrupt_fragments += v->getCorruptFragmentsNumber();
       }

        BOOST_TEST_MESSAGE("Fragments built = " << v_fragments);
        BOOST_TEST_MESSAGE("Corrupt fragments = " << v_corrupt_fragments);
        BOOST_TEST_MESSAGE("Received packets = " << v_packets);
        BOOST_TEST_MESSAGE("Missed packets = " << v_missed_packets);

        BOOST_CHECK(v_fragments == totalFragments);
        BOOST_CHECK(v_corrupt_fragments == corruptFragments);
        BOOST_CHECK(v_packets == receivedPackets);
        BOOST_CHECK(v_missed_packets == missedPackets);

        core.runStopped();
        core.disconnectFromFelix();

    } catch (ers::Issue & ex) {
        ers::fatal(ex);
    }
}

test_suite* init_unit_test_suite(int argc, char*argv[])
{
    if (argc != 2 || std::string("verbose") != argv[1]) {
        setenv("TDAQ_ERS_LOG", "null", 1);
        setenv("TDAQ_ERS_ERROR", "null", 1);
    }

    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::Exception & ex) {
        ers::fatal(ex);
        return 0;
    }

    std::vector<std::vector<uint64_t>> bad_links = {{}, {2}, {2, 4}, {3, 70}, {75, 95}};
    std::vector<uint32_t> timeouts = {0, 1, 10, 100, 1000};

    boost::function<void (const std::vector<uint64_t>&)> test_method_1 =
            boost::bind(&run_test, 0, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_1, bad_links.begin(), bad_links.end()));

    boost::function<void (const std::vector<uint64_t>&)> test_method_2 =
            boost::bind(&run_test, 100, _1);
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_2, bad_links.begin(), bad_links.end()));

    boost::function<void (uint32_t)> test_method_3 =
            boost::bind(&run_test, _1, std::vector<uint64_t>({2, 4, 94}));
    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(test_method_3, timeouts.begin(), timeouts.end()));

   return 0;
}

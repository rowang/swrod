/*
 * TestDataGenerator.h
 *
 *  Created on: May 14, 2019
 *      Author: kolos
 */

#ifndef UNIT_TESTS_INPUT_TESTDATAGENERATOR_H_
#define UNIT_TESTS_INPUT_TESTDATAGENERATOR_H_

#include <unordered_set>

#include <test/core/InternalDataGenerator.h>

namespace swrod {
    namespace test {
        class TestDataGenerator : public InternalDataGenerator {
        public:
            TestDataGenerator(const boost::property_tree::ptree & config);

        protected:
            void beforeStart() override;

            void generatePacket(InputLinkId link, uint32_t l1id, uint16_t bcid) override;

        private:
            uint32_t m_l1id_bit_mask1;
            uint32_t m_l1id_bit_mask2;
            uint32_t m_l1id_bit_mask_variation_step;
            uint32_t m_corrupt_data;
            uint32_t m_corrupt_l1id;
            uint32_t m_corrupt_bcid;
            uint32_t m_corrupt_data_cnt;
            uint32_t m_corrupt_l1id_cnt;
            uint32_t m_corrupt_bcid_cnt;
            bool m_send_duplicates;

            std::unordered_set<uint32_t> m_l1ids_dropped;
            std::unordered_set<InputLinkId> m_links_dropped;
        };
    }
}

#endif /* UNIT_TESTS_INPUT_TESTDATAGENERATOR_H_ */

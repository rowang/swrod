/* -*- c++ -*-
 *  Controller.h
 *
 *  Created on: Apr 24, 2019
 */

#ifndef _SWROD_CONTROLLER_H_
#define _SWROD_CONTROLLER_H_

#include <memory>
#include <string>
#include <set>
#include <vector>

#include <RunControl/Common/Controllable.h>
#include "is/infodictionary.h"
#include "ipc/partition.h"                   // for IPCPartition

namespace monsvc {
   class PublishingController;
}

namespace swrod {
    class Configuration;
    class Core;
    class Controller: public daq::rc::Controllable {
    public:

        Controller(const std::string & db_connect_string,
                const IPCPartition & partition, const std::string & name);

       ~Controller();
    private:
        void configure(const daq::rc::TransitionCmd & cmd) override;

        void connect(const daq::rc::TransitionCmd & cmd) override;

        void prepareForRun(const daq::rc::TransitionCmd & cmd) override;

        void stopArchiving(const daq::rc::TransitionCmd & cmd) override;

        void disconnect(const daq::rc::TransitionCmd & cmd) override;

        void unconfigure(const daq::rc::TransitionCmd & cmd) override;

        void publish() override;

        void disable(const std::vector<std::string>&) override;

        void resynch(const daq::rc::ResynchCmd& cmd) override;

        void user(const daq::rc::UserCmd & cmd) override;

       void removeIsInfo();
    private:
        const std::string m_connect_string;
        const IPCPartition m_partition;
        const std::string m_name;
        std::unique_ptr<Configuration> m_config;

       std::unique_ptr<Core> m_core;
       ISInfoDictionary m_dictionary;
       std::string m_isInfoName;
       std::set<std::string> m_isItems;
       std::shared_ptr<monsvc::PublishingController> m_publishingController;
   };
}

#endif

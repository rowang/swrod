/*
 * Core.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#include <cstdint>
#include <iostream>

#include <boost/property_tree/json_parser.hpp>

#include <is/infodictionary.h>

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/ROBFragmentConsumer.h>
#include <swrod/Factory.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;

namespace swrod {
    std::ostream& operator<<(std::ostream &out, const ptree &config) {
        write_json(out, config);
        return out;
    }

    template<class T>
    std::ostream& operator<<(std::ostream &out, const std::vector<T> &v) {
        out << '{' << std::hex;
        for (auto e : v) {
            out << "0x" << e << ", ";
        }
        if (!v.empty()) {
            out << "\b\b";
        }
        out << "}" << std::dec;
        return out;
    }

    std::unique_ptr<L1AInputHandler> createL1AHandler(
            const ptree & parent_config, const ptree & defaultInputConfig, const Core & core) {

        if (not parent_config.count("L1AHandler")) {
            return std::unique_ptr<L1AInputHandler>();
        }
        ptree l1a_config = PTREE_GET_CHILD(parent_config, "L1AHandler");
        if (not l1a_config.count("InputMethod")) {
            l1a_config.add_child("InputMethod", defaultInputConfig);
        }
        ptree & inConfig = l1a_config.get_child("InputMethod");
        inConfig.put("CPU", PTREE_GET_VALUE(l1a_config, std::string, "CPU"));

        return Factory<L1AInputHandler>::instance().create(
                PTREE_GET_VALUE(l1a_config, std::string, "Type"), l1a_config, core);
    }
}

Core::Core(const ptree & config)
try :   m_configuration(config),
        m_mapping(m_configuration),
        m_plugin_manager(config),
        m_processing_framework(config)
{
    ptree defaultInputConfig(PTREE_GET_CHILD(config, "InputMethod"));

    L1AInputHandler * current_l1a_handler = 0;
    std::string partition = PTREE_GET_VALUE(config, std::string, "Partition.Name");
    std::string application = PTREE_GET_VALUE(config, std::string, "Application.Name");

    m_modules.reserve(config.get_child("Modules").count("Module") + 1);
    m_modules.push_back(Module(partition, application));
    Module & default_module = *m_modules.begin();

    for (const ptree::value_type & m : PTREE_GET_CHILD(config, "Modules")) {
        const ptree & module_config = m.second;
        ptree moduleInputConfig(defaultInputConfig);
        if (module_config.count("InputMethod")) {
            moduleInputConfig = PTREE_GET_CHILD(module_config, "InputMethod");
        }

        m_modules.push_back(Module(partition, application));
        Module & module = *m_modules.rbegin();
        module.m_l1a_handler = createL1AHandler(module_config, moduleInputConfig, *this);
        if (not module.m_l1a_handler) {
            if (not default_module.m_l1a_handler) {
                default_module.m_l1a_handler = createL1AHandler(config, defaultInputConfig, *this);
            }
            current_l1a_handler = default_module.m_l1a_handler.get();
        } else {
            current_l1a_handler = module.m_l1a_handler.get();
        }

        ptree inConfig(moduleInputConfig);
        inConfig.put("CPU", PTREE_GET_VALUE(module_config, std::string, "CPU"));

        uint32_t workers = PTREE_GET_VALUE(module_config, uint32_t, "WorkersNumber");
        for (uint32_t i = 0; i < workers; ++i) {

            // This parameter is used only by the InternalData input in FULL mode
            // TODO: Try to get rid of it
            inConfig.put("WorkerIndex", i);

            std::shared_ptr<swrod::DataInput> in = Factory<DataInput>::instance().create(
                    PTREE_GET_VALUE(inConfig, std::string, "Type"),
                    inConfig, *this);
            module.m_inputs.push_back(in);
        }

        for (const ptree::value_type & r : PTREE_GET_CHILD(module_config, "ROBs")) {
            ptree rob_config = r.second;

            if (current_l1a_handler) {
                rob_config.put("L1AHandler", true);
            }
            ERS_DEBUG(1, "Creating ROB data handler with the following configuration: "
                    << rob_config);

            std::string builder_type = PTREE_GET_VALUE(rob_config, std::string,
                    "FragmentBuilder.Type");

            uint32_t ROB_id = PTREE_GET_VALUE(rob_config, uint32_t, "Id");
            std::string controller_name = PTREE_GET_VALUE(rob_config, std::string,
                    "Contains.TTCControllerName");
            bool update_ECR = PTREE_GET_VALUE(rob_config, bool, "Contains.UpdateECRCounter");
            ROB & rob = module.addROB(ROB{ROB_id, controller_name, update_ECR});

            rob.m_builder = Factory<ROBFragmentBuilder>::instance().create(
                    builder_type, rob_config, *this);

            if (current_l1a_handler) {
                current_l1a_handler->subscribe(rob.m_builder);
            }

            // ROB specific consumers
            if (rob_config.count("Consumers")) {
                for (const ptree::value_type & c : PTREE_GET_CHILD(rob_config, "Consumers")) {
                    ptree consumer_config(c.second);
                    consumer_config.add_child("RobConfig", rob_config);

                    std::string type = PTREE_GET_VALUE(consumer_config, std::string, "Type");
                    auto consumer = Factory<ROBFragmentConsumer>::instance().create(
                            type, consumer_config, *this);
                    rob.addConsumer(consumer);

                    m_all_consumers.push_back(consumer);
                    ERS_LOG("ROB 0x" << std::hex << ROB_id << " specific consumer of '" << type
                            << "' type has been created");
                }
            }

            m_all_builders.push_back(rob.m_builder);
            ERS_LOG("ROB 0x" << std::hex << ROB_id << " data handler of '" << builder_type
                    << "' type has been created");
        }
    }

    // Common consumers for all ROBs
    if (config.count("Consumers")) {
        for (const ptree::value_type & c : PTREE_GET_CHILD(config, "Consumers")) {
            const ptree & consumer_config = c.second;
            ERS_DEBUG(1, "Creating ROB fragment consumer with the following configuration: "
                    << consumer_config);

            std::string type = PTREE_GET_VALUE(consumer_config, std::string, "Type");
            auto consumer = Factory<ROBFragmentConsumer>::instance().create(
                    type, consumer_config, *this);
            for (auto & m : m_modules) {
                m.addConsumer(consumer);
            }
            m_all_consumers.push_back(consumer);
            ERS_LOG("'" << type << "' common consumer has been created");
        }
    }
}
catch (swrod::BadConfigurationException & ) {
    throw ;
}
catch (std::exception & ex) {
    throw swrod::BadConfigurationException(ERS_HERE, "SW ROD configuration failed", ex);
}

Core::~Core() {
    runStopped();
}

const std::vector<std::shared_ptr<DataInput>> & Core::getInputs(uint32_t ROB_id) const {
    for (auto & module : m_modules) {
        if (module.containsROB(ROB_id)) {
            return module.m_inputs;
        }
    }
    throw swrod::BadConfigurationException(ERS_HERE, "No input is found for ROB "
            + std::to_string(ROB_id));
}

void Core::connectToFelix() {
    ERS_LOG("Subscribing");

    for (auto & m : m_modules) {
        m.call(&DataInputHandler::subscribeToFelix);
    }

    ERS_LOG("Connected to FELIX");
}

void Core::disconnectFromFelix() {
    ERS_LOG("Unsubscribing");

    for (auto & m : m_modules) {
        m.call(&DataInputHandler::unsubscribeFromFelix);
    }

    ERS_LOG("Disconnected from FELIX");
}

void Core::runStarted(const RunParams & run_params) {
    ERS_LOG("RunParams = " << run_params);

    ERS_LOG("Starting ROB fragment consumers");
    for (auto it = m_all_consumers.rbegin(); it != m_all_consumers.rend(); ++it) {
        (*it)->runStarted(run_params);
    }

    ERS_LOG("Starting data handlers");
    // The first module contains default L1A handler which must be started the last
    for (auto it = m_modules.rbegin(); it != m_modules.rend(); ++it) {
        it->runStarted(run_params);
    }

    ERS_LOG("Core is running");
}

void Core::runStopped() {
    ERS_LOG("Stopping data handlers");
    for (auto & m : m_modules) {
        m.runStopped();
    }

    ERS_LOG("Stopping ROB fragment consumers");
    for (auto it = m_all_consumers.rbegin(); it != m_all_consumers.rend(); ++it) {
       (*it)->runStopped();
    }

    ERS_LOG("Core has been stopped");
}

std::vector<ISInfo*> Core::getStatistics() {
    std::vector<ISInfo*> result;
    for (auto & b : m_all_builders) {
        ISInfo* i = b->getStatistics();
        if (i) {
           result.push_back(i);
        }
    }

    for (auto & c : m_all_consumers) {
        ISInfo* i = c->getStatistics();
        if (i) {
           result.push_back(i);
        }
    }
    return result;
}

void Core::disableROBs(std::vector<uint32_t> & ROB_ids) {
    ERS_LOG("Disabling ROB(s) " << ROB_ids);

    for (auto it = ROB_ids.begin(); it != ROB_ids.end(); ) {
        auto mod = std::find_if(m_modules.begin(), m_modules.end(),
                [it](auto & m){ return m.containsROB(*it); });

        if (mod == m_modules.end()) {
            ers::error(ROBRemovalFailed(ERS_HERE, *it,
                    "it is not handled by this application"));
            it = ROB_ids.erase(it);
            continue;
        }

        mod->disableROB(*it);
        ERS_LOG("ROB 0x" << std::hex << *it << " has been disabled");
        ++it;
    }

    ERS_LOG("ROB(s) " << ROB_ids << " have been disabled");
}

void Core::enableROBs(std::vector<uint32_t> & ROB_ids, uint32_t lastL1ID) {
    ERS_LOG("Enabling ROB(s) " << ROB_ids);

    for (auto it = ROB_ids.begin(); it != ROB_ids.end(); ) {
        auto mod = std::find_if(m_modules.begin(), m_modules.end(),
                [it](auto & m){ return m.containsROB(*it); });

        if (mod == m_modules.end()) {
            ers::error(ROBRecoveryFailed(ERS_HERE, *it,
                    "it is not handled by this application"));
            it = ROB_ids.erase(it);
            continue;
        }

        mod->enableROB(*it, lastL1ID);
        ERS_LOG("ROB 0x" << std::hex << *it << " has been enabled");
        ++it;
    }

    ERS_LOG("ROB(s) " << ROB_ids << " have been re enabled");
}

void Core::disableInputLinks(std::vector<InputLinkId> & link_ids) {
    ERS_LOG("Disabling input link(s) " << link_ids);

    std::vector<InputLinkId> still_enabled(link_ids);
    for (auto & m : m_modules) {
        if (still_enabled.empty()) {
            break;
        }
        m.disableLinks(still_enabled);
    }

    // The 'still_enabled' vector contains now only those links,
    // which have NOT been disabled
    for (auto l : still_enabled) {
        link_ids.erase(std::find(link_ids.begin(), link_ids.end(), l));
    }

    // The 'link_ids' vector contains now only those links,
    // which have been successfully disabled

    ERS_LOG("Input link(s) " << link_ids
            << " have been disabled");
}

void Core::enableInputLinks(std::vector<InputLinkId> & link_ids, uint32_t lastL1ID) {
    ERS_LOG("Enabling input link(s) " << link_ids);

    std::vector<InputLinkId> still_disabled(link_ids);
    for (auto & m : m_modules) {
        if (still_disabled.empty()) {
            break;
        }
        m.enableLinks(still_disabled, lastL1ID);
    }

    // The 'still_disabled' vector contains now only those links,
    // which have NOT been disabled
    for (auto l : still_disabled) {
        link_ids.erase(std::find(link_ids.begin(), link_ids.end(), l));
    }

    // The 'link_ids' vector contains now only those links,
    // which have been successfully enabled

    ERS_LOG("Input link(s) " << link_ids
            << " have been successfully re enabled");
}

void Core::resynchAfterRestart(uint32_t lastL1ID) {
    ERS_LOG("Synchronising all components");

    for (auto & m : m_modules) {
        m.resynchAfterRestart(lastL1ID);
    }

    ERS_LOG("All components have been re synchronised");
}

void Core::userCommand(const daq::rc::UserCmd & cmd) {
    ERS_LOG("User command '" << cmd.commandName()
            << "' has been received");

    for (auto & c : m_all_consumers) {
        c->userCommand(cmd);
    }

    ERS_LOG("User command '" << cmd.commandName()
            << "' has been passed to all consumers");
}

///////////////////////////////////////////////////////////////////////////////////////
//  Core::ROB methods
///////////////////////////////////////////////////////////////////////////////////////

void Core::ROB::addConsumer(const std::shared_ptr<ROBFragmentConsumer> &c) {
    m_builder->addConsumer(c);
    m_consumers.push_back(c);
}

void Core::ROB::disable() {
    m_builder->disable();
    m_enabled = false;
    for (auto & c : m_consumers) {
        c->ROBDisabled(m_id);
    }
}

void Core::ROB::disableLinks(std::vector<InputLinkId> & links) {
    if (not m_enabled) {
        return;
    }
    std::vector<InputLinkId> original_set(links);
    m_builder->disableLinks(links);
    for (auto l : original_set) {
        if (std::find(links.begin(), links.end(), l) == links.end()) {
            m_disabled_elinks.insert(l);
            for (auto & c : m_consumers) {
                c->linkDisabled(l);
            }
        }
    }
}

void Core::ROB::enable(uint32_t lastL1ID) {
    if (m_enabled) {
        return;
    }

    m_builder->enable(lastL1ID, 0);
    m_enabled = true;
    for (auto & c : m_consumers) {
        c->ROBEnabled(m_id);
    }
}

void Core::ROB::enableLinks(std::vector<InputLinkId> & links, uint32_t lastL1ID) {
    if (not m_enabled) {
        return;
    }

    std::vector<InputLinkId> original_set(links);
    m_builder->enableLinks(links, lastL1ID, 0);
    for (auto l : original_set) {
        if (std::find(links.begin(), links.end(), l) == links.end()) {
            m_disabled_elinks.erase(l);
            for (auto & c : m_consumers) {
                c->linkEnabled(l);
            }
        }
    }
}

void Core::ROB::runStarted(const RunParams & run_params) {
    m_builder->runStarted(run_params);

    // Re enables all temporary disabled resources which could have been disabled
    // during the last run. Must be called after the runStarted()
    // The order is important. First the ORB has to be enabled and only then
    // the E-Links which were explicitly disabled.
    std::vector<InputLinkId> links(m_disabled_elinks.begin(), m_disabled_elinks.end());
    enable(-1);
    enableLinks(links, -1);
}

void Core::ROB::runStopped() {
    m_builder->runStopped();
}

///////////////////////////////////////////////////////////////////////////////////////
//  Core::Module methods
///////////////////////////////////////////////////////////////////////////////////////
void Core::Module::addConsumer(const std::shared_ptr<ROBFragmentConsumer> & c) {
    for (auto & pair : m_ROBs) {
        pair.second.addConsumer(c);
    }
}

Core::ROB & Core::Module::addROB(Core::ROB && rob) {
    if (m_TTC_controller_name.empty()) {
        m_TTC_controller_name = rob.m_TTC_controller_name;
    }
    if (m_TTC_controller_name != rob.m_TTC_controller_name) {
        throw BadConfigurationException(ERS_HERE, "ROB "
            + (boost::format("0x%x") % rob.m_id).str()
            + " has TTCControllerName parameter set to '" + rob.m_TTC_controller_name
            + "' which is different from the value of the other ROBs belonging to the same module");
    }

    if (not m_update_ECR.has_value()) {
        m_update_ECR = rob.m_update_ECR;
    }
    if (m_update_ECR != rob.m_update_ECR) {
        throw BadConfigurationException(ERS_HERE, "ROB "
            + (boost::format("0x%x") % rob.m_id).str()
            + " has UpdateECRCounter parameter set to '" + (rob.m_update_ECR ? "true" : "false")
            + "' which is different from the value of the other ROBs belonging to the same module");
    }

    auto r = m_ROBs.insert(std::make_pair(rob.m_id, rob));
    if (r.second) {
        return r.first->second;
    }
    throw BadConfigurationException(ERS_HERE, "ROB "
        + (boost::format("0x%x") % rob.m_id).str()
        + " appears more than once in the current configuration");
}

bool Core::Module::containsROB(uint32_t ROB_id) const {
    return m_ROBs.count(ROB_id);
}

void Core::Module::disableLinks(std::vector<InputLinkId> & link_ids) {
    for (auto & pair : m_ROBs) {
        if (link_ids.empty()) {
            break;
        }
        pair.second.disableLinks(link_ids);
    }
}

bool Core::Module::disableROB(uint32_t ROB_id) {
    auto it = m_ROBs.find(ROB_id);
    if (it != m_ROBs.end()) {
        it->second.disable();
        return true;
    }
    return false;
}

void Core::Module::enableLinks(std::vector<InputLinkId> & link_ids, uint32_t lastL1ID) {
    for (auto & pair : m_ROBs) {
        pair.second.enableLinks(link_ids, lastL1ID);
    }
}

bool Core::Module::enableROB(uint32_t ROB_id, uint32_t lastL1ID) {
    auto it = m_ROBs.find(ROB_id);
    if (it != m_ROBs.end()) {
        it->second.enable(lastL1ID);
        return true;
    }
    return false;
}

void Core::Module::resynchAfterRestart(uint32_t lastL1ID) {
    if (m_l1a_handler) {
        m_l1a_handler->resynchAfterRestart(lastL1ID);
    }
    for (auto & pair : m_ROBs) {
        pair.second.m_builder->resynchAfterRestart(lastL1ID);
    }
    if (m_update_ECR.value_or(true)) {
        synchronise(lastL1ID>>24);
    }
}

void Core::Module::runStarted(const RunParams & run_params) {
    for (auto & pair : m_ROBs) {
        pair.second.runStarted(run_params);
    }

    if (m_l1a_handler) {
        m_l1a_handler->runStarted(run_params);
    }

    // Must be called at the end as it takes into account
    // subscribed E-Links but subscriptions can happen in the runStarted() call
    if (not m_status_checker.inRunningState(m_TTC_controller_name)) {
        synchronise(0);
    }
}

void Core::Module::runStopped() {
    if (m_l1a_handler) {
        m_l1a_handler->runStopped();
    }
    for (auto & pair : m_ROBs) {
        pair.second.runStopped();
    }
}

void Core::Module::synchronise(uint8_t ECR) {
    for (auto & in : m_inputs) {
        in->synchronise(ECR);
    }
}

///////////////////////////////////////////////////////////////////////////////////////
//  Core::RCStatusChecker method
///////////////////////////////////////////////////////////////////////////////////////
bool Core::RCStatusChecker::inRunningState(const std::string & controller_name) {
    try {
        auto s = m_rc_sender.status(controller_name);
        return (not s->isTransitioning() && s->fsmState() == "RUNNING");
    } catch (ers::Issue & ex) {
        ers::log(ex);
        return false;
    }
}


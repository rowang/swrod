/*
 * ROBFragmentConsumerBase.cpp
 *
 *  Created on: Jul 23, 2019
 *      Author: kolos
 */


#include <swrod/ROBFragmentConsumerBase.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace swrod::detail;

swrod::ROBFragmentConsumerBase::ROBFragmentConsumerBase(
        const UserFunction& callback,
        QueuingPolicy queuing_policy,
        ForwardingPolicy forwarding_policy,
        uint16_t workers_num,
        int32_t queue_size,
        const std::string & name,
        const std::string & cpu_set) :
    m_forwarding(forwarding_policy),
    m_queuing(queuing_policy),
    m_flush_buffer(true),
    m_running(false),
    m_threads(workers_num, std::bind(&ROBFragmentConsumerBase::run,
            this, callback, std::placeholders::_1), name, cpu_set)
{
    if (queue_size > 0) {
        m_fragments_queue.set_capacity(queue_size);
    }
}

swrod::ROBFragmentConsumerBase::ROBFragmentConsumerBase(
        const boost::property_tree::ptree& config,
        const UserFunction& callback,
        QueuingPolicy queuing_policy,
        ForwardingPolicy forwarding_policy,
        const std::string & name) :
    m_forwarding(forwarding_policy),
    m_queuing(queuing_policy),
    m_flush_buffer(PTREE_GET_VALUE(config, bool, "FlushBufferAtStop")),
    m_running(false),
    m_threads(PTREE_GET_VALUE(config, uint32_t, "WorkersNumber"),
            std::bind(&ROBFragmentConsumerBase::run, this, callback, std::placeholders::_1),
            name,
            PTREE_GET_VALUE(config, std::string, "CPU"))
{
    uint32_t queue_size = PTREE_GET_VALUE(config, uint32_t, "QueueSize");
    if (queue_size > 0) {
        m_fragments_queue.set_capacity(queue_size);
    }
}

swrod::ROBFragmentConsumerBase::ROBFragmentConsumerBase(
        const boost::property_tree::ptree& config,
        const std::vector<UserFunction> & functions,
        QueuingPolicy queuing_policy,
        ForwardingPolicy forwarding_policy,
        const std::string & name) :
    m_forwarding(forwarding_policy),
    m_queuing(queuing_policy),
    m_flush_buffer(PTREE_GET_VALUE(config, bool, "FlushBufferAtStop")),
    m_running(false),
    m_threads([this](auto & ff){
                std::vector<ThreadPool::ThreadFunction> r;
                for (auto & f : ff) {
                    r.push_back(std::bind(&ROBFragmentConsumerBase::run, this, f, std::placeholders::_1));
                }
                return r;
            }(functions),
        name,
        PTREE_GET_VALUE(config, std::string, "CPU"))
{
    uint32_t queue_size = PTREE_GET_VALUE(config, uint32_t, "QueueSize");
    if (queue_size > 0) {
        m_fragments_queue.set_capacity(queue_size);
    }
}

void ROBFragmentConsumerBase::insertROBFragment(const std::shared_ptr<ROBFragment>& fragment) {
    if (m_forwarding == ForwardingPolicy::Immediate) {
        forwardROBFragment(fragment);
    }

    if (!m_running) {
        ERS_DEBUG(4, "Fragment with L1ID = " << fragment->m_l1id
                << " has not been added to the queue: consumer is not active" );
        return;
    }

    if (m_fragments_queue.try_push(fragment)) {
        ERS_DEBUG(4, "Fragment with L1ID = " << fragment->m_l1id
                << " has been added to the queue" );
    } else {
        if (m_queuing == QueuingPolicy::Drop) {
            ERS_DEBUG(4, "Fragment with L1ID = " << fragment->m_l1id
                << " has not been added to the queue: queue is full" );

            if (m_forwarding == ForwardingPolicy::AfterProcessing) {
                forwardROBFragment(fragment);
            }
        }
        else {
            while (not m_fragments_queue.try_push(fragment)) {
                if (not m_running) {
                    return;
                }
                usleep(5);
            }
            ERS_DEBUG(4, "Fragment with L1ID = " << fragment->m_l1id
                    << " has been added to the queue" );
        }
    }
}

void ROBFragmentConsumerBase::runStarted(const RunParams& ) {
    ERS_LOG("Starting the '" << m_threads.getName() << "' ROB fragment consumer");
    if (!m_running) {
        m_threads.start();
        m_running = true;
    }
    ERS_LOG("The '" << m_threads.getName() << "' ROB fragment consumer has been started");
}

void ROBFragmentConsumerBase::runStopped() {
    ERS_LOG("Stopping the '" << m_threads.getName() << "' ROB fragment consumer");
    if (m_running) {
        m_running = false;
        if (m_flush_buffer) {
            m_fragments_queue.clear();
        }
        m_threads.stop();
    }
    ERS_LOG("The '" << m_threads.getName() << "' ROB fragment consumer has been stopped");
}

void ROBFragmentConsumerBase::run(const UserFunction & user_function, const bool & active) {
    while (true) {
        std::shared_ptr<ROBFragment> fragment;
        if (not m_fragments_queue.try_pop(fragment)) {
            if (not active) {
                return;
            }
            usleep(1);
            continue;
        }

        user_function(fragment);

        if (m_forwarding == ForwardingPolicy::AfterProcessing) {
            forwardROBFragment(fragment);
        }
    }
}

/*
 * ROBFragmentBuilderBase.cpp
 *
 *  Created on: Nov 7, 2019
 *      Author: kolos
 */

#include <memory>

#include <boost/format.hpp>

#include <swrod/Core.h>
#include <swrod/ROBFragmentBuilderBase.h>
#include <swrod/exceptions.h>
#include <swrod/detail/WaitFor.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace std::chrono;
using namespace boost::property_tree;

namespace {
    template<typename T>
    std::vector<std::vector<T>> splitVector(const std::vector<T>& vec, size_t n) {
        size_t length = vec.size() / n;
        size_t remain = vec.size() % n;

        size_t begin = 0;
        size_t end = 0;

        std::vector<std::vector<T>> result;
        result.reserve(n);
        for (size_t i = 0; i < n; ++i) {
            end += (remain > 0) ? (length + !!(remain--)) : length;
            result.emplace_back(vec.begin() + begin, vec.begin() + end);
            begin = end;
        }
        return result;
    }
}

namespace swrod {
    template<class T> extern
    std::ostream& operator<<(std::ostream &out, const std::vector<T> &v);
}

ROBFragmentBuilderBase::ROBFragmentBuilderBase(
        const ptree& robConfig,
        const Core & core, const L1AReceiver & l1a_receiver,
        const WorkerFactory & workers_factory, bool header_present) :
    m_ROB_id(PTREE_GET_VALUE(robConfig, uint32_t,
            "Id")),
    m_unique_id((boost::format("ROB-%08x") % m_ROB_id).str()),
    m_use_queue(PTREE_GET_VALUE(robConfig, uint16_t,
            "FragmentBuilder.BuildersNumber")),
    m_flush_buffer(PTREE_GET_VALUE(robConfig, bool,
            "FragmentBuilder.FlushBufferAtStop")),
    m_ROD_header_present(header_present),
    m_l1a_wait_timeout(PTREE_GET_VALUE(robConfig, uint32_t,
            "FragmentBuilder.L1AWaitTimeout")),
    m_resynch_timeout(PTREE_GET_VALUE(robConfig, uint32_t,
            "FragmentBuilder.ResynchTimeout")),
    m_running(false),
    m_enabled(true),
    m_l1a_receiver(l1a_receiver),
    m_fragment_assembler(1000),
    m_transmitters(
            PTREE_GET_VALUE(robConfig, uint16_t, "FragmentBuilder.BuildersNumber"),
            [this](const bool & active) { transmitter(active); },
            (boost::format("ROB-%x-builder") % m_ROB_id).str(),
            PTREE_GET_VALUE(robConfig, std::string, "CPU"))
{
    ERS_DEBUG(1, "Configuring fragment building for ROB 0x" << m_ROB_id);

    InputLinkVector links;
    for (const ptree::value_type & e : PTREE_GET_CHILD(robConfig, "Contains.Contains")) {
        links.push_back(std::make_pair(
                PTREE_GET_VALUE(e.second, InputLinkId, "FelixId"),
                PTREE_GET_VALUE(e.second, uint32_t, "DetectorResourceId")));
    }

    auto & inputs = core.getInputs(m_ROB_id);

    std::vector<InputLinkVector> links_per_slice = splitVector(links, inputs.size());

    for (size_t i = 0; i < links_per_slice.size(); ++i) {
        if (links_per_slice[i].size()) {
            m_workers.push_back(std::unique_ptr<DataInputHandlerBase>(
                    workers_factory(links_per_slice[i], inputs[i])));
        }
    }

    m_ready.set_capacity(PTREE_GET_VALUE(robConfig, uint32_t, "FragmentBuilder.ReadyQueueSize"));

    ERS_DEBUG(1, "Configuration for ROB 0x" << m_ROB_id << " finished");
}

ROBFragmentBuilderBase::~ROBFragmentBuilderBase() {
    runStopped();

    // Clear all containers that may contain fragments before destroying the workers
    // otherwise this may lead to crash as fragments may have a reference to custom
    // memory pools which are owned by the workers
    m_consumer.reset();
    m_ready.clear();
    m_fragment_assembler.clear();
}

void ROBFragmentBuilderBase::fragmentReady(FragmentAssembler::accessor & a) {
    if (m_use_queue) {
        while (m_running) {
            if (m_ready.try_push(a->first)) {
                break;
            }
        }
    } else {
        submitFragment(a);
    }
}

void ROBFragmentBuilderBase::submitFragment(FragmentAssembler::accessor & a) {
    detail::DataHolder dh(std::move(a->second));
    m_fragment_assembler.erase(a);

    forwardROBFragment(std::make_shared<ROBFragment>(
            m_ROB_id, dh.m_l1id, dh.m_bcid, dh.m_trigger_type,
            dh.m_status, dh.m_missed_packets, dh.m_corrupted_packets,
            std::move(dh.m_data), m_ROD_header_present));

    if (dh.m_status) {
        ++m_statistics.corruptedFragments;
        if (not (m_statistics.corruptedFragments % 10000) || m_good_fragments) {
            ers::error(ROBFragmentError(ERS_HERE, m_ROB_id, dh.m_l1id, m_statistics.corruptedFragments));
        }
        m_good_fragments = 0;
    }
    else {
        ++m_good_fragments;
    }

    ERS_DEBUG(4, "A new fragment with l1id = 0x" << std::hex << dh.m_l1id
            << " has been built for ROB " << m_ROB_id);

    std::unique_lock lock(m_built_mutex);
    m_buffer_size = std::max(m_buffer_size, m_fragment_assembler.size());
    m_queue_size = std::max(m_queue_size, (uint64_t)m_ready.size());

    m_last_built_L1ID.set(dh.m_index, dh.m_l1id);
}

void ROBFragmentBuilderBase::transmitter(const bool & active) {
    while (true) {
        uint64_t id;
        if (not m_ready.try_pop(id)) {
            if (!active) {
                return;
            }
            usleep(1);
            continue;
        }

        FragmentAssembler::accessor a;
        if (m_fragment_assembler.find(a, id)) {
            submitFragment(a);
        }
    }
}

void ROBFragmentBuilderBase::runStarted(const RunParams & run_params) {
    ERS_LOG("Starting fragment building for ROB 0x" << std::hex << m_ROB_id);

    if (m_running) {
        ERS_DEBUG(1, "Fragment building for ROB 0x" << std::hex << m_ROB_id << " is already running");
        return;
    }

    m_fragment_assembler.clear();
    m_ready.clear();

    m_statistics = ROBStatistics();
    m_start_of_run = m_last_update = steady_clock::now();
    m_last_built_L1ID = L1ID();
    m_last_received_L1ID = L1ID();

    m_running = true;

    std::for_each(m_workers.begin(), m_workers.end(),
            [&run_params](auto & sa){ sa->runStarted(run_params); } );
    m_transmitters.start();

    ERS_LOG("Event building for ROB 0x" << std::hex << m_ROB_id << " started");
}

void ROBFragmentBuilderBase::runStopped() {
    ERS_LOG("Stopping fragment building for ROB 0x" << std::hex << m_ROB_id);

    if (!m_running) {
        ERS_DEBUG(1, "Fragment building for ROB 0x" << std::hex << m_ROB_id << " is not running");
        return;
    }

    std::for_each(m_workers.begin(), m_workers.end(),
            [](auto & sa){ sa->runStopped(); } );

    m_running = false;

    if (m_flush_buffer) {
        m_ready.clear();
    }

    m_transmitters.stop();

    m_ready.clear();

    ERS_LOG("Event building for ROB 0x" << std::hex << m_ROB_id
            << " stopped, fragments built = " << std::dec << m_last_built_L1ID.getCounter());
}

void ROBFragmentBuilderBase::subscribeToFelix() {
    ERS_LOG("Subscribing for input of ROB 0x" << std::hex << m_ROB_id);

    std::for_each(m_workers.begin(), m_workers.end(),
            [](auto & sa){ sa->subscribeToFelix(); } );

    ERS_LOG("Subscribed for input of ROB 0x" << std::hex << m_ROB_id);
}

void ROBFragmentBuilderBase::unsubscribeFromFelix() {
    ERS_LOG("Unsubscribing from input of ROB 0x" << std::hex << m_ROB_id);

    std::for_each(m_workers.begin(), m_workers.end(),
            [](auto & sa){ sa->unsubscribeFromFelix(); } );

    ERS_LOG("Unsubscribed from input of ROB 0x" << std::hex << m_ROB_id);
}

void ROBFragmentBuilderBase::disable() {
    ERS_LOG("Disable request for ROB 0x" << std::hex << m_ROB_id);
    if (!m_enabled) {
        ERS_LOG("Ignore disable request for the ROB " << std::hex << m_ROB_id << " that is already disabled");
        return;
    }

    std::for_each(m_workers.begin(), m_workers.end(),
            [](auto & sa){ sa->disable(); });

    m_enabled = false;
    ERS_LOG("ROB 0x" << std::hex << m_ROB_id << " has been disabled");
}

void ROBFragmentBuilderBase::enable(uint32_t lastL1ID, uint64_t triggersNumber) {
    ERS_LOG("Enable request for ROB " << std::hex << m_ROB_id);
    if (m_enabled) {
        ERS_LOG("Ignore enable request for the ROB "
                << std::hex << m_ROB_id << " that is already enabled");
        return;
    }

    synchronize(lastL1ID, m_last_received_L1ID);

    m_fragment_assembler.clear();
    m_ready.clear();

    std::for_each(m_workers.begin(), m_workers.end(),
            [this](auto & sa){
                sa->enable(m_last_received_L1ID.getValue(), m_last_received_L1ID.getCounter());
            });

    m_enabled = true;

    ERS_LOG("ROB 0x" << std::hex << m_ROB_id << " has been enabled");
}

void ROBFragmentBuilderBase::disableLinks(std::vector<InputLinkId> & link_ids) {
    ERS_LOG("Disable input links " << link_ids << " for ROB 0x" << std::hex << m_ROB_id);

    std::for_each(m_workers.begin(), m_workers.end(),
            [&link_ids](auto & sa){ sa->disableLinks(link_ids); });

    ERS_LOG("Input links " << link_ids << " have not been disabled");
}

void ROBFragmentBuilderBase::enableLinks(std::vector<InputLinkId> & link_ids,
        uint32_t lastL1ID, uint64_t triggersNumber) {
    ERS_LOG("Enable input links " << link_ids << " for ROB 0x" << std::hex << m_ROB_id);

    const L1ID & data = m_enabled ? m_last_built_L1ID : m_last_received_L1ID;

    synchronize(lastL1ID, data);

    std::for_each(m_workers.begin(), m_workers.end(),
            [&](auto & sa){
                sa->enableLinks(link_ids, lastL1ID, m_last_received_L1ID.getCounter());
            });

    ERS_LOG("Links " << link_ids << " have not been enabled");
}

void ROBFragmentBuilderBase::synchronize(uint32_t lastL1ID, const L1ID & l1id) {

    ERS_LOG("Waiting for event with L1ID = 0x" << std::hex << lastL1ID
                    << ", last built event has L1ID = 0x" << l1id.getValue());

    if (not detail::waitFor(std::chrono::milliseconds(m_resynch_timeout),
            [&l1id, lastL1ID](){ return l1id.getValue() == lastL1ID;})) {
        ers::error(ResynchFailed(ERS_HERE, lastL1ID,
                m_last_built_L1ID.getValue(), m_last_received_L1ID.getValue()));
    }

    ERS_LOG("ROB 0x" << std::hex << m_ROB_id << " got event with L1ID = 0x" << std::hex << lastL1ID
            << ", event counter = " << std::dec << l1id.getCounter());
}

void ROBFragmentBuilderBase::resynchAfterRestart(uint32_t lastL1ID) {
    ERS_LOG("Resynch request for ROB 0x" << std::hex << m_ROB_id << " with L1ID = 0x" << lastL1ID);

    if (m_last_built_L1ID.getCounter() or m_last_received_L1ID.getCounter()) {
        // This is the light-weight resynchronise procedure
        // Lets wait before the fragment with the given L1ID is built
        const L1ID & data = m_enabled ? m_last_built_L1ID : m_last_received_L1ID;
        synchronize(lastL1ID, data);
    }
    else {
        // This is a follow up of the TTC Restart procedure
        m_last_built_L1ID = m_last_received_L1ID = L1ID(lastL1ID);
    }

    std::for_each(m_workers.begin(), m_workers.end(),
            [lastL1ID](auto & sa){
                sa->resynchAfterRestart(lastL1ID);
            });

    ERS_LOG("done");
}

ISInfo * ROBFragmentBuilderBase::getStatistics() {
    m_statistics.corruptedPackets = m_statistics.droppedPackets = m_statistics.missedPackets = 0;
    m_statistics.disabledLinks.clear();
    m_statistics.linksStatistics.clear();
    for (auto & sa : m_workers) {
        for (auto & l : sa->getLinks()) {
            LinkStatistics ls;
            ls.FID = l.m_fid;
            ls.receivedPackets = l.m_packets_counter;
            ls.corruptedPackets = l.m_packets_corrupted;
            ls.droppedPackets = l.m_packets_dropped;
            ls.missedPackets = l.m_packets_missed;
            m_statistics.linksStatistics.push_back(ls);
            m_statistics.corruptedPackets += ls.corruptedPackets;
            m_statistics.droppedPackets += ls.droppedPackets;
            m_statistics.missedPackets += ls.missedPackets;
            if (!l.m_enabled) {
                m_statistics.disabledLinks.push_back(l.m_fid);
            }
        }
    }

    m_statistics.lastL1IDBuilt = m_last_built_L1ID.getValue();
    m_statistics.lastL1IDReceived = m_last_received_L1ID.getValue();
    m_statistics.receivedL1APackets = m_last_received_L1ID.getCounter();
    uint64_t now_fragments_built = m_last_built_L1ID.getCounter();
    uint64_t last_fragments_built = m_statistics.fragmentsBuilt;

    time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
    uint64_t since_start_of_run = duration_cast<milliseconds>(now - m_start_of_run).count();
    uint64_t since_last_update = duration_cast<milliseconds>(now - m_last_update).count();
    m_last_update = now;

    m_statistics.fragmentsBuilt = now_fragments_built;
    m_statistics.averageBuildingRate = since_start_of_run ?
            now_fragments_built * 1000 / since_start_of_run : 0;
    m_statistics.instantBuildingRate = since_last_update ?
            (now_fragments_built - last_fragments_built) * 1000 / since_last_update : 0;
    m_statistics.assemblyBufferSize = m_buffer_size;
    m_statistics.readyQueueSize = m_queue_size;

    m_buffer_size = m_fragment_assembler.size();
    m_queue_size = m_ready.size();

    return &m_statistics;
}


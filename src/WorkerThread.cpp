/*
 * WorkerThread.cpp
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#include <pthread.h>
#include <sched.h>

#include <iostream>
#include <regex>
#include <vector>

#include <ers/ers.h>
#include <swrod/detail/WorkerThread.h>

using namespace swrod::detail;

WorkerThread::WorkerThread(const std::function<void(const bool &)> & function,
        const std::string & name, const std::string & cpu_set) :
        m_name(name.substr(0, 15)),
        m_cpu_set(cpu_set),
        m_function(function),
        m_active(false)
{
}

WorkerThread::~WorkerThread()
{
    stop();
}

void WorkerThread::start()
{
    ERS_DEBUG(1, "Starting the '" << m_name << "' thread");
    std::unique_lock<std::mutex> lock(m_mutex);
    if (!m_active) {
        m_active = true;
        m_thread = std::thread(std::bind(&WorkerThread::execute, this));
    }
    ERS_DEBUG(1, "The '" << m_name << "' thread has been started");
}

void WorkerThread::stop()
{
    ERS_DEBUG(1, "Stopping the '" << m_name << "' thread");
    std::unique_lock<std::mutex> lock(m_mutex);
    if (m_active) {
        m_active = false;
        m_thread.join();
    }
    ERS_DEBUG(1, "The '" << m_name << "' thread has been stopped");
}

void WorkerThread::execute()
{
    std::vector<int> cpus = parseCpuRange(m_cpu_set);
    if (!cpus.empty()) {
        cpu_set_t pmask;
        CPU_ZERO(&pmask);

        for (int cpu : cpus) {
            CPU_SET(cpu, &pmask);
        }

        int s = sched_setaffinity(0, sizeof(pmask), &pmask);
        if (s != 0) {
            ERS_LOG("Setting affinity to CPU rage [" << m_cpu_set << "] failed with error = " << s);
        }
    }

    pthread_setname_np(pthread_self(), m_name.c_str());

    m_function(m_active);
}

std::vector<int> WorkerThread::parseCpuRange(const std::string &range) {
    static const std::regex re(
            R"(((\d+)-(\d+))|(\d+))");
    std::smatch sm;

    std::vector<int> data;
    for (std::string s(range); std::regex_search(s, sm, re); s = sm.suffix()) {
        if (sm[1].str().empty()) {
            data.push_back(std::stoi(sm[0]));
        }
        else {
            for (int i = std::stoi(sm[2]); i <= std::stoi(sm[3]); ++i) {
                data.push_back(i);
            }
        }
    }
    return data;
}

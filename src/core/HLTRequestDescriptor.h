// -*- c++ -*-
#ifndef HLT_REQUEST_DESCRIPTOR_H
#define HLT_REQUEST_DESCRIPTOR_H

#include "ROSDescriptorNP/RequestDescriptor.h"
namespace daq {
   namespace asyncmsg {
      class Session;
   }
}

namespace swrod{
   //! SW ROD sepcific request descriptor class
   class HLTRequestDescriptor : public ROS::RequestDescriptor {
   public:
      HLTRequestDescriptor():m_size(0){}
      void initialise(unsigned int level1Id,
                      const std::vector<unsigned int>* channelList,
                      std::shared_ptr<daq::asyncmsg::Session> destination,
                      unsigned int transactionId){
         m_level1Id=level1Id;
         m_transactionId=transactionId;
         m_destination=destination;
//         m_channels.insert(channelList->begin(),channelList->end());
         m_initialiseTime=std::chrono::system_clock::now();
      }

      //! Send the data to the destination set at initialise.
      virtual void send() final {};

      //! Get the transaction Id of the HLT request associated with this descriptor.
      unsigned int transactionId(){
         return m_transactionId;
      };

      //! Get a pointer to our list of channels that we should be getting data for.
      std::set<unsigned int>* channels(){
         return &m_channels;
      }

      std::weak_ptr<daq::asyncmsg::Session> destination(){ return m_destination;}
      

      void size(unsigned int sz) {m_size=sz;};
      unsigned int size() {return m_size;};

      //! Get the number of channels requested
      unsigned int nChannelsRequested(){
         return m_channels.size();
      }

   private:
      std::set<unsigned int> m_channels;
      unsigned int m_size;
   };
}
#endif

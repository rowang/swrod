// -*- c++ -*-
//
#ifndef SWROD_OUTPUT_MSG_H
#define SWROD_OUTPUT_MSG_H

#include "HLTRequestDescriptor.h"
#include "ROSasyncmsg/RosDataMsg.h"
#include "swrod/ROBFragment.h"

namespace swrod {
   //! SW ROD specific output message. Outputs a ROB fragment containing a ROD Fragment
   class SwrodOutputMsg: public RosDataMsg,
                       public daq::asyncmsg::OutputMessage {
   public:
      explicit SwrodOutputMsg(HLTRequestDescriptor* desc,
                              std::uint32_t runNumber,
                              uint32_t latestL1,
                              std::vector<std::shared_ptr<ROBFragment>>& fragments)
         : RosDataMsg(desc->transactionId()),m_descriptor(desc),
           m_fragments(fragments),
           m_size(0) {
         for (auto fragment : m_fragments) {
            auto tvec=fragment->serialize(runNumber,latestL1);
            for (auto segment : tvec) {
               m_size+=segment.second;
            }
            m_segments.insert(m_segments.end(),tvec.begin(),tvec.end());
         }
         m_descriptor->size(m_size);
      };

      virtual ~SwrodOutputMsg(){
         //std::cout << "ROxOutputMsg destructor\n";
      };

      virtual std::uint32_t size() const {
         return m_size;
      };

      //! Assemble all the parts of the fragment
      virtual void
      toBuffers(std::vector<boost::asio::const_buffer>& buffers) const final {
         //std::cout << "SwrodOutputMsg::toBuffers() xid=" << std::hex << m_descriptor->transactionId() << std::dec;
         for (auto segment : m_segments) {
            buffers.emplace_back(segment.first,segment.second);
         }
      }

      HLTRequestDescriptor* descriptor() const {
         return m_descriptor;
      }

      HLTRequestDescriptor* m_descriptor;
      std::vector<std::shared_ptr<ROBFragment>>  m_fragments;      
      std::vector<std::pair<uint8_t*, uint32_t>> m_segments;
      unsigned int m_size;
   };
}
#endif

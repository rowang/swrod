/*
 * ROBFragmentProcessor.h
 *
 *  Created on: May 29, 2019
 *      Author: kolos
 */

#ifndef SWROD_ROBFRAGMENTPROCESSOR_H_
#define SWROD_ROBFRAGMENTPROCESSOR_H_

#include <chrono>
#include <memory>
#include <mutex>

#include <is/infodynany.h>

#include <swrod/ROBFragmentConsumerBase.h>
#include <swrod/CustomProcessor.h>

namespace swrod {
    class Core;

    class ProcessorPool {
        protected:
            ProcessorPool(const boost::property_tree::ptree & config, const Core& core);

            uint64_t getExecutionTimeNano() const {
                return std::accumulate(m_processors.begin(), m_processors.end(), 0L,
                        [](uint64_t v, auto & p){ return v + p->getExecutionTimeNano(); });
            }

            uint64_t getExecutionCounter() const {
                return std::accumulate(m_processors.begin(), m_processors.end(), 0L,
                        [](uint64_t v, auto & p){ return v + p->getExecutionCounter(); });
            }

            void runStarted(const RunParams& run_params) {
                for_each(m_processors.begin(), m_processors.end(), [&run_params](auto & p){
                    p->resetTiming();
                    (*p)->runStarted(run_params);
                });
            }

            void runStopped() {
                for_each(m_processors.begin(), m_processors.end(), [](auto & p){
                    (*p)->runStopped();
                });
            }

        protected:
            const std::vector<ROBFragmentConsumerBase::UserFunction> & getFunctions() {
                return m_functions;
            }

            struct LockableProcessor {
                LockableProcessor(std::unique_ptr<CustomProcessor> && p) :
                    m_processor(std::move(p)) {}

                void process(ROBFragment & f) const {
                    m_processor->processROBFragment(f);
                }

                void processWithLock(ROBFragment & f) const {
                    std::unique_lock l(m_mutex);
                    process(f);
                }

                void processWithTiming(ROBFragment & f) const {
                    using namespace std::chrono;
                    time_point<steady_clock> start(steady_clock::now());
                    process(f);
                    time_point<steady_clock> stop(steady_clock::now());
                    m_execution_time += duration_cast<nanoseconds>(stop - start).count();
                    ++m_execution_counter;
                }

                void processWithLockAndTiming(ROBFragment & f) const {
                    using namespace std::chrono;
                    time_point<steady_clock> start(steady_clock::now());
                    processWithLock(f);
                    time_point<steady_clock> stop(steady_clock::now());
                    m_execution_time += duration_cast<nanoseconds>(stop - start).count();
                    ++m_execution_counter;
                }

                CustomProcessor * operator->() const {
                    return m_processor.get();
                }

            uint64_t getExecutionCounter() const {
                return m_execution_counter;
            }

            uint64_t getExecutionTimeNano() const {
                return m_execution_time;
            }

            void resetTiming() {
                m_execution_counter = 0;
                m_execution_time = 0;
            }

            private:
                mutable std::mutex m_mutex;
                mutable uint64_t m_execution_time = 0;
                mutable uint64_t m_execution_counter = 0;
                std::unique_ptr<CustomProcessor> m_processor;
            };

            std::vector<std::shared_ptr<LockableProcessor>> m_processors;
            std::vector<ROBFragmentConsumerBase::UserFunction> m_functions;
    };

    class ROBFragmentProcessor : public ProcessorPool,
                                 public ROBFragmentConsumerBase {
    public:
        ROBFragmentProcessor(const boost::property_tree::ptree & config, const Core& core);

        ~ROBFragmentProcessor();

        const std::string& getName() const override {
            return m_unique_id;
        }

        void linkDisabled(const InputLinkId & link_id) override {
            for_each(m_processors.begin(), m_processors.end(),
                    [&link_id](auto & p){ (*p)->linkDisabled(link_id); });
        }

        void linkEnabled(const InputLinkId & link_id) override {
            for_each(m_processors.begin(), m_processors.end(),
                    [&link_id](auto & p){ (*p)->linkEnabled(link_id); });
        }

        void runStarted(const RunParams& run_params) override {
            ProcessorPool::runStarted(run_params);
            ROBFragmentConsumerBase::runStarted(run_params);
        }

        void runStopped() override {
            if (not m_running) {
                return;
            }
            ROBFragmentConsumerBase::runStopped();
            ProcessorPool::runStopped();
            uint64_t counter = getExecutionCounter();
            if (counter) {
                double time = getExecutionTimeNano() / 1000.;
                std::cout << m_unique_id << " processed " << counter << " fragments in "
                        << time << " microseconds, average processing time is "
                        << time / counter << " microseconds" << std::endl;
            }
        }

        void userCommand(const daq::rc::UserCmd & cmd) override {
            for_each(m_processors.begin(), m_processors.end(),
                    [&cmd](auto & p){ (*p)->userCommand(cmd); });
        }

        ISInfo * getStatistics() override;

    private:
        const std::string m_unique_id;
        ISInfoDynAny m_statistics;
    };
}

#endif /* SWROD_ROBFRAGMENTPROCESSOR_H_ */

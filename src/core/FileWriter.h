// -*- c++ -*-
//! Class to handle writing of ROBFragments to file
#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <memory>
#include <thread>
#include <string>
#include <tbb/concurrent_queue.h>
#include <boost/property_tree/ptree.hpp>

#include "swrod/ROBFragmentConsumer.h"
#include "swrod/FragmentCollator.h"
#include "swrod/detail/HeaderHelper.h"

#include "swrod/FileWriterStatistics.h"

namespace EventStorage {
   class DataWriter;
}
class RunParams;

namespace swrod {
   class Core;
   struct ROBFragment;

   class FileWriter : public ROBFragmentConsumer {
   public:
      FileWriter(const boost::property_tree::ptree& config,
                 const boost::property_tree::ptree& coreConfig);

      FileWriter(const boost::property_tree::ptree& config, const Core & core);

      virtual ~FileWriter();

      const std::string & getName() const final {
         return c_consumerName;
      };
      void insertROBFragment(const std::shared_ptr<ROBFragment>& fragment) final;

      void ROBDisabled(unsigned int robId) final;

      void ROBEnabled(unsigned int robId) final;

      void runStarted(const RunParams& runPars) final;
      void runStopped() final;

      bool active() const {
         return m_running;}

      std::string fileName();

      ISInfo* getStatistics() final;
      void resetStats();
   private:
      void write();
      const std::string c_consumerName="FileWriter";
      helper::FragmentCollator m_indexHelper;
      helper::HeaderHelper m_headerHelper;
      tbb::concurrent_bounded_queue<unsigned int> m_writeQueue;
      EventStorage::DataWriter* m_dataWriter;
      std::string m_appName;
      std::string m_fileDir;
      unsigned int m_maxEventsPerFile;
      unsigned int m_maxMegaBytesPerFile;
      unsigned int m_runNumber;
      unsigned int m_maxEvents;
      std::thread m_thread;
      bool m_running;
      bool m_stopping;
      FileWriterStatistics m_statistics;
      bool m_alwaysEnabled;
   };
}
#endif

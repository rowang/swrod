
#include <pthread.h>                             // for pthread_setname_np
#include <sys/uio.h>                             // for iovec
#include <vector>

#include <tbb/tbb_exception.h>                   // for user_abort

#include "swrod/Core.h"
#include "swrod/Factory.h"
#include "swrod/exceptions.h"

#include "FileWriter.h"
#include "swrod/FragmentCollator.h"

#include "EventStorage/DataWriter.h"
#include "EventStorage/EventStorageRecords.h"

#include "EventStorage/RawFileName.h"
#include "RunControl/Common/UserExceptions.h"
#include "rc/RunParams.h"

#include "ers/ers.h"                             // for error, warning, ERS_...
#include "swrod/ROBFragment.h"                   // for ROBFragment, swrod

using namespace boost::property_tree;
using namespace swrod;


namespace {
   Factory<ROBFragmentConsumer>::Registrator
   __reg__("FileWriter",
           [](const boost::property_tree::ptree& config, const
              Core& core) {
              return std::make_shared<FileWriter>(config, core);
           });
}

FileWriter::FileWriter(const ptree& config, const Core & core)
   : FileWriter(config,core.getConfiguration()) {
}

FileWriter::FileWriter(const ptree& config,const ptree& coreConfig)
   :m_dataWriter(0),
    m_running(false),
    m_stopping(false),
    m_alwaysEnabled(false) {
   resetStats();
   try {
      unsigned int nRobs=0;
      for (auto mod :coreConfig.get_child("Modules")) {
         nRobs+=mod.second.get_child("ROBs").size();
      }
      m_indexHelper.expected(nRobs);

      m_appName=coreConfig.get<std::string>("Application.Name");

      unsigned int queueLimit=config.get<unsigned int>("QueueSize");
      m_writeQueue.set_capacity(queueLimit);
      m_alwaysEnabled=config.get<bool>("IgnoreRecordingEnable");
      m_maxEventsPerFile=config.get<unsigned int>("EventStorage.MaxEventsPerFile");
      m_maxMegaBytesPerFile=config.get<unsigned int>("EventStorage.MaxMegaBytesPerFile");
      m_fileDir=config.get<std::string>("EventStorage.DirectoryToWrite");
   }
   catch (ptree_error& err) {
      throw(BadConfigurationException(ERS_HERE,err.what()));
   }
}

FileWriter::~FileWriter(){
   if (m_dataWriter) {
      delete m_dataWriter;
   }
}

void FileWriter::ROBEnabled(unsigned int robId){
   m_indexHelper.enable(robId);
}
void FileWriter::ROBDisabled(unsigned int robId){
   m_indexHelper.disable(robId);
}

void FileWriter::runStarted(const RunParams& runPars){
   if (runPars.recording_enabled || m_alwaysEnabled) {
      if (!runPars.recording_enabled) {
         ers::warning(FileWritingForcedIssue(ERS_HERE));
      }
      EventStorage::run_parameters_record runParamRec;
      runParamRec.marker=RUN_PARAMETERS_MARKER;
      runParamRec.record_size=sizeof(runParamRec);
      runParamRec.run_number=runPars.run_number;
      runParamRec.max_events=runPars.max_events;
      runParamRec.rec_enable=runPars.recording_enabled;
      runParamRec.trigger_type=runPars.trigger_type;
      runParamRec.beam_type=runPars.beam_type;
      runParamRec.beam_energy=runPars.beam_energy;

      std::vector<std::string> userMetaData;

      daq::RawFileName fileName(runPars.T0_project_tag,
                                runPars.run_number,
                                "",
                                runPars.filename_tag,
                                0,
                                m_appName);

      m_dataWriter=new EventStorage::DataWriter(m_fileDir,
                                                fileName.fileNameCore(),
                                                runParamRec,
                                                userMetaData);
      if (!m_dataWriter->good()) {
         // Should we throw an ers::fatal here?
         ers::error(FileStartIssue(ERS_HERE));
         delete m_dataWriter;
         m_dataWriter=0;
         return;
      }
      m_dataWriter->setMaxFileNE(m_maxEventsPerFile);
      m_dataWriter->setMaxFileMB(m_maxMegaBytesPerFile);

      m_headerHelper.runNumber(runPars.run_number);
      m_runNumber=runPars.run_number;
      m_maxEvents=runPars.max_events;
      m_statistics.eventsWritten=0;
      m_running=true;
      m_stopping=false;
      m_thread=std::thread([this] (){write();});
      pthread_setname_np(m_thread.native_handle(),"FileWriter");
   }
}

void FileWriter::runStopped(){
   m_stopping=true;
   if (m_writeQueue.size()<0) {
      // consumer is waiting on pop
      // std::cout << "FileWriter::runStopped() aborting write queue\n";
      m_running=false;
      m_writeQueue.abort();
   }
   // else {
   //    std::cout << "FileWriter::runStopped() allowing write queue to drain\n";
   // }
   if (m_thread.joinable()) {
      m_thread.join();
   }
   if (m_dataWriter != 0) {
      m_statistics.currentFile=m_dataWriter->nameFile(EventStorage::FINISHED);
      delete m_dataWriter;
      m_dataWriter=0;
   }
   m_indexHelper.reset();
   m_writeQueue.clear();
}

void FileWriter::insertROBFragment(
   const std::shared_ptr<ROBFragment>& fragment){
   if (m_dataWriter!=0 && m_running) {
      if (m_indexHelper.insert(fragment)) {
//         std::cout << "FileWriter::insertROBFragment() queueing fragment " << fragment->m_l1id << std::endl;
         try {
            m_writeQueue.push(fragment->m_l1id);
         }
         catch (tbb::user_abort& abortException) {
            ERS_DEBUG(2,"queue aborted by user");
         }
      }
   }
   forwardROBFragment(fragment); 
}

void FileWriter::write() {
   try {
      while (m_running) {
         if (m_stopping && m_writeQueue.size()==0) {
            m_running=false;
            break;
         }
         unsigned int l1Id;
         m_writeQueue.pop(l1Id);
//         std::cout << "FileWriter::write() dequeued fragment " << l1Id << std::endl;

         auto frags=m_indexHelper.take(l1Id);
         if (frags.size()==0) {
            ers::warning(FragmentNotFoundIssue(ERS_HERE,l1Id));
            continue;
         }

         // FullFragmentHeader
         std::shared_ptr<unsigned int[]> ffHead;
         ffHead.reset(m_headerHelper.makeHeader(frags));
         if (!ffHead) {
            // Belt and braces, should not get here as already checked for empty frag vector earlier
            ERS_LOG("No header returned by header helper");
            continue;
         }
         unsigned int totalSize=ffHead.get()[2]*sizeof(unsigned int);
         std::vector<struct iovec> ioVec;
         ioVec.emplace_back(iovec{ffHead.get(),totalSize});

         for (auto fragment : frags) {
            for (auto segment : fragment->serialize(m_runNumber)) {
               ioVec.emplace_back(iovec{segment.first,segment.second});
               totalSize+=segment.second;
            }
         }

         totalSize=totalSize/sizeof(unsigned int);
         ffHead.get()[1]=totalSize;
         ffHead.get()[19]=totalSize-ffHead.get()[2];
         auto nParts=ioVec.size();
         ioVec.emplace_back(iovec{0,0});
         unsigned int sizeWritten;
         auto error=m_dataWriter->putData(nParts, ioVec.data(),sizeWritten);
         if (error!=EventStorage::DWOK) {
            ers::error(FileWritingIssue(ERS_HERE));
         }
         m_statistics.dataOutput+=(float)sizeWritten/1e6;
         m_statistics.eventsWritten++;
         if (m_statistics.eventsWritten==m_maxEvents) {
            daq::rc::MAX_EVT_DONE issue(ERS_HERE,"reached max events");
            ers::warning(issue);
         }
      }
   }
   catch (tbb::user_abort& abortException) {
      return;
   }
}

ISInfo* FileWriter::getStatistics() {
   m_statistics.indexSize=m_indexHelper.size();
   m_statistics.queueSize=m_writeQueue.size();
   if (m_dataWriter) {
      m_statistics.currentFile=m_dataWriter->nameFile(EventStorage::UNFINISHED);
   }
   return &m_statistics;
}
void FileWriter::resetStats() {
   m_statistics.dataOutput=0.0;
   m_statistics.eventsWritten=0;
   m_statistics.currentFile="";
}

std::string FileWriter::fileName(){
   std::string result="";
   if (m_dataWriter) {
      result=m_dataWriter->nameFile(EventStorage::FINISHED);
   }
   return result;
}

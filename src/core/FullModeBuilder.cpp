/*
 * GDBDefaultBuilder.cpp
 *
 *  Created on: May 6, 2019
 *      Author: kolos
 */
#include <memory>

#include <swrod/Core.h>
#include <swrod/Factory.h>

#include "FullModeBuilder.h"

using namespace swrod;

namespace {
    Factory<ROBFragmentBuilder>::Registrator __reg__(
        "FullModeBuilder",
        [](const boost::property_tree::ptree& config, const Core& core) {
            if (config.count("L1AHandler")) {
                return std::shared_ptr<ROBFragmentBuilder>(
                        new FullModeBuilder<true>(config, core));
            } else {
                return std::shared_ptr<ROBFragmentBuilder>(
                        new FullModeBuilder<false>(config, core));
            }
        });
}

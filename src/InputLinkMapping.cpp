/*
 * InputLinkMapping.cpp
 *
 *  Created on: Jan 11, 2022
 *      Author: kolos
 */


#include <swrod/InputLinkMapping.h>
#include <swrod/exceptions.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace boost::property_tree;

swrod::InputLinkMapping::InputLinkMapping(const ptree &config) {

    for (const ptree::value_type & m : PTREE_GET_CHILD(config, "Modules")) {
        const ptree & module_config = m.second;

        for (const ptree::value_type & r : PTREE_GET_CHILD(module_config, "ROBs")) {
            ptree rob_config = r.second;
            for (const ptree::value_type & e : PTREE_GET_CHILD(rob_config, "Contains.Contains")) {
                m_links.insert(Link(
                        PTREE_GET_VALUE(e.second, InputLinkId, "FelixId"),
                        PTREE_GET_VALUE(e.second, uint32_t, "DetectorResourceId")));
            }
        }
    }
}

InputLinkId InputLinkMapping::felixId(DetectorLinkId id) const {
    auto it = m_links.find(id);
    if (it != m_links.end()) {
        return it->m_felix_id;
    }
    throw BadConfigurationException(ERS_HERE, std::to_string(id)
            + " Detector Resource ID is not defined");
}

DetectorLinkId InputLinkMapping::detectorResourceId(InputLinkId id) const {
    auto it = m_links.find(id);
    if (it != m_links.end()) {
        return it->m_detector_id;
    }
    throw BadConfigurationException(ERS_HERE, std::to_string(id)
            + " FELIX ID is not defined");
}

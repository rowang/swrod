/*
 * Pluginmanager.cpp
 *
 *  Created on: Jun 11, 2019
 *      Author: kolos
 */

#include <dlfcn.h>

#include <swrod/exceptions.h>
#include <swrod/DataInput.h>
#include <swrod/Factory.h>
#include <swrod/L1AInputHandler.h>
#include <swrod/ROBFragmentBuilder.h>
#include <swrod/ROBFragmentConsumer.h>
#include <swrod/detail/PluginManager.h>
#include <swrod/detail/ptree.h>

using namespace swrod;
using namespace swrod::detail;
using namespace boost::property_tree;

PluginManager::SharedLibrary::SharedLibrary(
        const std::string & library_name) :
        m_name(library_name),
        m_handle(nullptr)
{
    m_handle = dlopen(m_name.c_str(), RTLD_LAZY | RTLD_GLOBAL);
    ERS_DEBUG(1, m_name << " shared library handler is " << m_handle);

    if (!m_handle) {
        throw PluginException(ERS_HERE, m_name, dlerror());
    }
}

PluginManager::SharedLibrary::~SharedLibrary()
{
    ERS_DEBUG(1, "Closing " << m_handle << " library handler");
    dlclose(m_handle);
}

PluginManager::PluginManager(const boost::property_tree::ptree & config)
{
    // Factory instances have to be created in the main application
    // This prevents their destruction when shared libraries are unloaded

    Factory<DataInput>::instance();
    Factory<L1AInputHandler>::instance();
    Factory<ROBFragmentBuilder>::instance();
    Factory<ROBFragmentConsumer>::instance();

    for (const ptree::value_type & v : PTREE_GET_CHILD(config, "Plugins")) {
        loadSharedLibrary(PTREE_GET_VALUE(v.second, std::string, "LibraryName"));
    }
}

void PluginManager::loadSharedLibrary(const std::string & library_name)
{
    LibrariesMap::iterator it = m_libraries.find(library_name);
    if (it == m_libraries.end()) {
        m_libraries[library_name] = std::make_shared<SharedLibrary>(library_name);
    }
}

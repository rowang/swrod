/*
 * custom_plugin_test.cpp
 *
 *  Created on: Jan 11, 2022
 *      Author: kolos
 */

#include <sys/resource.h>
#include <csignal>
#include <iostream>

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <EventStorage/pickDataReader.h>
#include <eformat/FullEventFragmentNoTemplates.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/infodictionary.h>
#include <monsvc/PublishingController.h>
#include <monsvc/ConfigurationRules.h>

#include <swrod/exceptions.h>
#include <swrod/Core.h>

using namespace boost::program_options;
using namespace boost::property_tree;
using namespace swrod;

void publishStatistics(Core & core) {
    std::string server(core.getConfiguration().get<std::string>("ISServerName"));
    std::string app(core.getConfiguration().get<std::string>("Application.Name"));
    ISInfoDictionary dict(core.getConfiguration().get<std::string>("Partition.Name"));

    for (auto b : core.getBuilders()) {
       auto stats = b->getStatistics();
       if (stats) {
           try {
               dict.checkin(server + ".swrod." + app + "." + b->getName(), *stats);
           } catch (daq::is::Exception& ex) {
               ers::debug(ex, 0);
           }
       }
    }

    for (auto c : core.getConsumers()) {
       auto stats = c->getStatistics();
       if (stats) {
           try {
               dict.checkin(server + "." + c->getName(), *stats);
           } catch (daq::is::Exception& ex) {
               ers::debug(ex, 0);
           }
       }
    }
}

int main(int argc, char ** argv)
{
    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::Exception & ex) {
        ers::fatal(ex);
        return 1;
    }

    options_description description("Options");

    description.add_options()("help,h", "produce help message")
        ("plugin-lib,l", value<std::string>(), "Custom plugin library name")
        ("trigger-extractor-name,x", value<std::string>(),
                "Name of the trigger info extractor function")
        ("data-integrity-checker-name,y", value<std::string>()->default_value(""),
                "Name of the data integrity checker function")
        ("processor-creator-name,z", value<std::string>(),
                "Name of the custom processor creator function")
        ("raw-data-file,f", value<std::string>(),
                "Raw data file to be used as data source.")
        ("fragment-builder-CPUs,b", value<std::string>()->default_value(""),
                "CPU cores for fragment building threads. Empty string means no restrictions.")
        ("fragment-consumer-CPUs,c", value<std::string>()->default_value(""),
                 "CPU cores for fragment consumer threads. Empty string means no restrictions.")
        ("show-rates,v", value<uint32_t>()->default_value(0),
                "display event processing rates: 0 - none, 1 - instantaneous, 2 - average")
        ("publish-stats,S", "publish statistics to DF IS server")
        ("verbose,V", "print ERS messages")
        ("partition-name,p", value<std::string>()->default_value("initial"), "partition name")
        ("proc-number,n", value<uint32_t>()->default_value(1), "number of custom processing threads")
        ("repetitions,r", value<uint32_t>()->default_value(1), "number test repetitions")
        ("dead-links,D", value<uint32_t>()->default_value(0), "number of dead links per ROB to be simulated")
        ("packet-size,s", value<uint32_t>()->default_value(100), "average chunk size")
        ("config-out-file,o", value<std::string>(), "dump configuration to this JSON file")
        ("config-in-file,i", value<std::string>(), "read configuration from this JSON file")
        ("out-dir,d", value<std::string>(), "put recorded data-file to this directory");

    variables_map arguments;
    try {
        store(parse_command_line(argc, argv, description), arguments);
        notify(arguments);
    } catch (error & ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "W ROD custom plugin validating application" << std::endl;
        description.print(std::cout);
        return 0;
    }

    if (not arguments.count("verbose")) {
        setenv("TDAQ_ERS_LOG", "null", 1);
        setenv("TDAQ_ERS_ERROR", "null", 1);
    }

    std::string partition_name = arguments["partition-name"].as<std::string>();
    std::string cpu_1 = arguments["fragment-builder-CPUs"].as<std::string>();
    std::string cpu_2 = arguments["fragment-consumer-CPUs"].as<std::string>();
    uint32_t proc_number = arguments["proc-number"].as<uint32_t>();
    uint32_t repetitions = arguments["repetitions"].as<uint32_t>();
    uint32_t show_rates = arguments["show-rates"].as<uint32_t>();
    uint32_t packet_size = arguments["packet-size"].as<uint32_t>();
    uint32_t dead_links = arguments["dead-links"].as<uint32_t>();

    std::string app_name(getenv("TDAQ_APPLICATION_NAME")
            ? getenv("TDAQ_APPLICATION_NAME") : "swrod_custom_plugin_test");

    //////////////////////////////////////////////////////////
    // Prepare Core configuration
    //////////////////////////////////////////////////////////
    ptree configuration;

    if (arguments.count("config-in-file")) {
        try {
            read_json(arguments["config-in-file"].as<std::string>(), configuration);
            if (configuration.count("Partition")) {
                partition_name = configuration.get<std::string>("Partition.Name");
            }
            if (configuration.count("Application")) {
                app_name = configuration.get<std::string>("Application.Name");
            }
        } catch (json_parser::json_parser_error & ex) {
            std::cerr << ex.what() << std::endl;
            return 1;
        }
    }
    else {
        bool given = arguments.count("plugin-lib")
                && arguments.count("trigger-extractor-name")
                && arguments.count("processor-creator-name")
                && arguments.count("raw-data-file");
        if (!given) {
            std::cerr
            << "If configuration is not taken from a JSON file the following command like options must be provided:\n"
               "plugin-lib, trigger-extractor-name, processor-creator-name, raw-data-file" << std::endl;
            return 1;
        }

        std::string file_name = arguments["raw-data-file"].as<std::string>();
        std::string plugin = arguments["plugin-lib"].as<std::string>();
        std::string trigger_info_extractor_name = arguments["trigger-extractor-name"].as<std::string>();
        std::string data_integrity_checker_name = arguments["data-integrity-checker-name"].as<std::string>();
        std::string processor_creator_name = arguments["processor-creator-name"].as<std::string>();

        DataReader * data_reader = pickDataReader(file_name);
        if (!data_reader || !data_reader->good()) {
            std::cerr << "Cannot read '" + file_name + "' data file." << std::endl;
            return 1;
        }

        struct ROB {
            uint32_t m_id;
            std::vector<uint32_t> m_links;
        };

        std::vector<ROB> robs;
        try {
            char *buf;
            unsigned int size = 0;

            DRError err = data_reader->getData(size, &buf);
            if (err != EventStorage::DROK) {
                std::cerr << "Data file '" + file_name + "' is broken." << std::endl;
                return 1;
            }

            using namespace eformat::read;
            FullEventFragment frag((unsigned int*)buf);
            frag.check();

            std::vector<eformat::read::ROBFragment> fragments;
            frag.robs(fragments);

            for (auto & f : fragments) {
                ROB rob;
                rob.m_id = f.rob_source_id();

                const uint32_t * buf_start = f.rod_data();
                const uint32_t * buf_end = buf_start + f.rod_ndata();

                while (buf_start < buf_end) {
                    uint32_t size = *buf_start & 0xffff;
                    uint32_t id = *(uint32_t*)(buf_start + 1);
                    rob.m_links.push_back(id);
                    buf_start += size;
                }

                robs.push_back(rob);
            }

            delete [] buf;
        } catch (ers::Issue &ex) {
            std::cerr << "Bad data file '" + file_name + "' is: " << ex << std::endl;
            return 1;
        }

        delete data_reader;

        configuration.add("Partition.Name", partition_name);
        configuration.add("Application.FullStatisticsInterval", 1);
        configuration.add("Application.Name", app_name);
        configuration.add("ISServerName", "DF");

        ptree plugins;
        ptree pnode;
        pnode.put("LibraryName", "libswrod_core_test.so");
        plugins.add_child("Plugin", pnode);
        pnode.put("LibraryName", "libswrod_core_unit_test.so");
        plugins.add_child("Plugin", pnode);
        pnode.put("LibraryName", "libswrod_core_impl.so");
        plugins.add_child("Plugin", pnode);
        configuration.add_child("Plugins", plugins);

        ptree input;
        input.add("Type", "FileInput");
        input.add("FileName", file_name);
        input.add("Repetitions", repetitions);
        input.add("DeadLinksNumber", dead_links);
        configuration.add_child("InputMethod", input);

        for (auto & r : robs) {
            ptree module;

            input.put("RobId", r.m_id);
            module.add_child("InputMethod", input);

            module.add("WorkersNumber", 1);
            module.add("CPU", cpu_1);

            ptree rob;
            rob.add("Id", r.m_id);
            rob.add("CPU", cpu_1);

            ptree event_builder;
            event_builder.add("Type", "GBTModeBuilder");
            event_builder.add("BuildersNumber", 1);
            event_builder.add("MaxMessageSize", packet_size);
            event_builder.add("BufferSize", 10000);
            event_builder.add("MinimumBufferSize", 64);
            event_builder.add("L1AWaitTimeout", 1000);
            event_builder.add("DataReceivingTimeout", 1000);
            event_builder.add("ResynchTimeout", 1000);
            event_builder.add("RecoveryDepth", 10);
            event_builder.add("FlushBufferAtStop", false);
            event_builder.add("RODHeaderPresent", false);
            event_builder.add("DropCorruptedPackets", false);
            event_builder.add("ReadyQueueSize", 100000);
            rob.add_child("FragmentBuilder", event_builder);

            ptree data_channel;
            for (auto & eid : r.m_links) {
                ptree link;
                link.add("FelixId", eid);
                link.add("DetectorResourceId", eid);
                data_channel.add_child("Contains.InputLink", link);
            }

            ptree custom_proc;
            custom_proc.add("LibraryName", plugin);
            custom_proc.add("TrigInfoExtractor", trigger_info_extractor_name);
            custom_proc.add("DataIntegrityChecker", data_integrity_checker_name);
            custom_proc.add("ProcessorFactory", processor_creator_name);
            data_channel.add_child("CustomLib", custom_proc);
            rob.add_child("Contains", data_channel);

            ptree consumers;
            if (proc_number) {
                ptree custom_processing;
                custom_processing.add("Type", "ROBFragmentProcessor");
                custom_processing.add("WorkersNumber", proc_number);
                custom_processing.add("QueueSize", 100000);
                custom_processing.add("ProfileExecution", true);
                custom_processing.add("DeferProcessing", false);
                custom_processing.add("FlushBufferAtStop", false);
                custom_processing.add("CPU", cpu_2);
                consumers.add_child("Consumer", custom_processing);
            }

            if (show_rates) {
                ptree counter;
                counter.add("Mode", show_rates);
                counter.add("Type", "ROBFragmentCounter");
                counter.add("QueueSize", 100000);
                counter.add("FlushBufferAtStop", false);
                consumers.add_child("Consumer", counter);
            }

            rob.add_child("Consumers", consumers);
            module.add_child("ROBs.ROB", rob);
            configuration.add_child("Modules.Module", module);
        }

        ptree consumers;
        if (arguments.count("out-dir")) {
            ptree rob_ids;
            for (auto & r : robs) {
                rob_ids.add("ROB.Id", r.m_id);
            }
            ptree file_writer;
            file_writer.add("Type", "FileWriter");
            file_writer.add("swrod.name", "swrod_test");
            file_writer.add("QueueSize", 1024);
            file_writer.add("IgnoreRecordingEnable", true);
            file_writer.add("FlushBufferAtStop", false);
            file_writer.add("EventStorage.DirectoryToWrite",
                    arguments["out-dir"].as<std::string>());
            file_writer.add("EventStorage.MaxEventsPerFile", 0);
            file_writer.add("EventStorage.MaxMegaBytesPerFile", 2560);
            file_writer.add_child("ROBs", rob_ids);
            consumers.add_child("Consumer", file_writer);
        }

        configuration.add_child("Consumers", consumers);
    }

    if (arguments.count("config-out-file")) {
        try {
            write_json(arguments["config-out-file"].as<std::string>(), configuration);
        } catch (json_parser::json_parser_error & ex) {
            std::cerr << ex.what() << std::endl;
            return 1;
        }
    }

    try {
        std::unique_ptr<Core> core;
        core.reset(new Core(configuration));
        core->connectToFelix();

        IPCPartition partition(partition_name);
        std::shared_ptr<monsvc::PublishingController> pc;

        if (arguments.count("publish-stats")) {
            pc.reset(new monsvc::PublishingController(partition, app_name));
            pc->add_configuration_rule(
               *monsvc::ConfigurationRule::from(
                       "Histogramming:.*/=>oh:(10,1,Histogramming," + app_name + ")"));
            pc->start_publishing();
        }

        RunParams rp;
        rp.recording_enabled = true;
        rp.run_number = 12345;
        core->runStarted(rp);

        bool running = true;
        std::thread thread([&]() {
            while (running) {
                usleep(1000000);
                if (arguments.count("publish-stats")) {
                    publishStatistics(*core.get());
                }
            }
        });

        core->runStopped();

        running = false;
        thread.join();

        if (pc) {
            pc->stop_publishing();
        }

        core->disconnectFromFelix();

    } catch (const ers::Issue & ex) {
        ers::fatal(ex);
        return 1;
    }
}

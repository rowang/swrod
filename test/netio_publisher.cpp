/*
 * netio_publisher.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#include <stdio.h>                                          // for getchar
#include <time.h>                                           // for localtime
#include <unistd.h>                                         // for usleep

#include <csignal>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <string>
#include <thread>
#include <vector>

#include <boost/program_options.hpp>

#include <felixbus/bus.hpp>
#include <felixbus/elinktable.hpp>
#include <felixbus/felixtable.hpp>

#include <netio/netio.hpp>

#include <swrod/detail/L1AMessage.h>
#include <swrod/detail/Barrier.h>
#include <swrod/detail/NetioPacketHeader.h>

using namespace netio;
using namespace swrod::detail;

netio::sockcfg createNetioConfig(uint32_t page_size, uint32_t buffer_pages,
        bool zero_copy) {
    netio::sockcfg cfg = netio::sockcfg::cfg();
    cfg(netio::sockcfg::BUFFER_PAGES_PER_CONNECTION, buffer_pages);
    cfg(netio::sockcfg::PAGESIZE, page_size);
    if (zero_copy) {
        cfg(netio::sockcfg::ZERO_COPY);
    }
    return cfg;
}

class DataPublisher {
public:

    DataPublisher(swrod::test::Barrier & barrier, const std::string & backend,
            uint32_t port, uint32_t page_size, uint32_t buffer_pages,
            bool zero_copy, uint32_t message_size, uint32_t ecr_interval,
            const std::vector<netio::tag> & tags, bool send_duplicates) :
            m_barrier(barrier),
            m_context(backend),
            m_socket(&m_context, port, createNetioConfig(page_size, buffer_pages, zero_copy)),
            m_channels(tags),
            m_first_channel(0),
            m_last_channel(tags.size()),
            m_current_channel(m_first_channel),
            m_channels_number(tags.size()),
            m_ecr_interval(ecr_interval),
            m_message_size(message_size),
            m_active(false),
            m_send_duplicates(send_duplicates),
            m_io_thread([this]() { m_context.event_loop()->run_forever(); })
    {
        m_data = new uint8_t[message_size];
        for (size_t i = 0; i < message_size; ++i) {
            m_data[i] = 0;
        }
        *((uint16_t*) m_data) = message_size;
    }

    virtual ~DataPublisher() {
        m_active = false;
        m_context.event_loop()->stop();
        m_io_thread.join();
        m_thread.join();
        delete [] m_data;
    }

    void start() {
        m_active = true;
        m_thread = std::thread(std::bind(&DataPublisher::run, this));
    }

    uint64_t getTotalCount() const {
        return m_total_count;
    }

    uint64_t getTotalSize() const {
        return m_total_size;
    }

private:
    virtual void run() {
        static const uint16_t BCID_MASK = 0x0fff;

        netio::message msg(m_data, m_message_size);

        m_barrier.wait();

        while (m_active) {
            ++m_count;

            for (uint32_t channel = m_first_channel; channel < m_last_channel; ++channel) {
                *(uint32_t*)(m_data + 4) = m_channels[channel];
                *(uint32_t*)(m_data + 8) = m_l1a_packet.message.l1id;
                *(uint16_t*)(m_data + 14) = m_l1a_packet.message.bcid;
                *(uint32_t*)(m_data + 16) = 0xffff; // L1ID bit mask
                m_socket.publish(m_channels[channel], msg);
                m_total_size += m_message_size;
                ++m_total_count;

                if (m_send_duplicates && m_count % 8888 == 0) {
                    m_socket.publish(m_channels[channel], msg);
                }
            }

            if (m_count % m_ecr_interval == 0) {
                m_l1a_packet.message.l1id = 0;
                m_l1a_packet.message.bcid = 0;
                m_l1a_packet.message.ecr++;
                m_barrier.wait();
            } else {
                m_l1a_packet.message.l1id++;
                m_l1a_packet.message.bcid = m_l1a_packet.message.l1id & BCID_MASK;
            }
        }
    }

protected:
    struct L1APacket {
        NetioPacketHeader header;
        L1AMessage message;
    };

    swrod::test::Barrier & m_barrier;
    uint8_t * m_data;
    netio::context m_context;
    netio::publish_socket m_socket;

    std::vector<netio::tag> m_channels;
    uint32_t m_first_channel;
    uint32_t m_last_channel;
    uint32_t m_current_channel;
    uint32_t m_channels_number;
    uint32_t m_ecr_interval;
    uint32_t m_message_size;

    uint32_t m_subscriptions = 0;
    uint64_t m_count = 0;
    uint64_t m_total_count = 0;
    uint64_t m_total_size = 0;

    L1APacket m_l1a_packet;
    bool m_active;
    bool m_send_duplicates;
    std::thread m_io_thread;
    std::thread m_thread;
};

class L1APublisher : public DataPublisher {
public:
    using DataPublisher::DataPublisher;

    void run() override {
        static const uint16_t BCID_MASK = 0x0fff;

        m_l1a_packet.header.size = sizeof(m_l1a_packet);
        m_l1a_packet.header.elink_id = m_channels[m_current_channel];
        netio::message l1a_msg((uint8_t*) &m_l1a_packet, sizeof(m_l1a_packet));

        m_barrier.wait();
        while (m_active) {
            m_socket.publish(m_channels[m_current_channel], l1a_msg);
            m_total_count++;
            m_total_size += sizeof(m_l1a_packet);

            ++m_count;
            if (m_send_duplicates && m_count % 8888 == 0) {
                m_socket.publish(m_channels[m_current_channel], l1a_msg);
            }

            if (m_count % m_ecr_interval == 0) {
                m_l1a_packet.message.l1id = 0;
                m_l1a_packet.message.bcid = 0;
                m_l1a_packet.message.ecr++;
                m_barrier.wait();
            } else {
                m_l1a_packet.message.l1id++;
                m_l1a_packet.message.bcid = m_l1a_packet.message.l1id & BCID_MASK;
            }
        }
    }
};

volatile std::sig_atomic_t signal_status = 0;

void signal_handler(int s) {
    signal_status = s;
}

using namespace boost::program_options;

int main(int ac, char *av[])
{
    options_description description("Options");

    description.add_options()("help,h", "produce help message")
        ("port,P", value<uint32_t>()->default_value(12345), "port number")
        ("first-elink,f", value<uint64_t>()->default_value(1), "first elink id")
        ("l1a-elink,a", value<uint64_t>()->default_value(7777), "L1A elink id, 0 for no L1A")
        ("elinks-number,e", value<uint32_t>()->default_value(192), "e-links number per worker thread")
        ("packet-size,s", value<uint32_t>()->default_value(40), "packet size")
        ("ecr-interval,l", value<uint32_t>()->default_value(500000), "ECR interval")
        ("host,H", value<std::string>()->default_value("127.0.0.1"),
            "IP address were netio data will be published")
        ("netio-backend,b", value<std::string>()->default_value("posix"), "Netio backed: posix or fi_verbs")
        ("netio-page-size,B", value<uint32_t>()->default_value(100000), "Netio page size in bytes")
        ("netio-l1a-page-size,A", value<uint32_t>()->default_value(4098), "Netio page size for sending L1A messages")
        ("netio-pages-number,C", value<uint32_t>()->default_value(32), "Netio pages number")
        ("zero-copy,Z", "use Netio zero-copy mode")
        ("send-duplicate-packets,D", "occasionally send duplicate packets for debugging")
        ("wait-input,W", "wait for an input from the terminal before starting to send data")
        ("workers,w", value<uint32_t>()->default_value(1), "worker threads number");

    variables_map arguments;
    try {
        store(parse_command_line(ac, av, description), arguments);
        notify(arguments);
    } catch (error & ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "Test application for the 'swrod' package" << std::endl;
        description.print(std::cout);
        return 0;
    }

    uint32_t elinks_per_worker = arguments["elinks-number"].as<uint32_t>();
    uint32_t workers_number = arguments["workers"].as<uint32_t>();
    uint32_t packet_size = arguments["packet-size"].as<uint32_t>();
    uint32_t ecr_interval = arguments["ecr-interval"].as<uint32_t>();
    uint64_t first_elink_id = arguments["first-elink"].as<uint64_t>();
    uint64_t l1a_elink_id = arguments["l1a-elink"].as<uint64_t>();
    uint32_t port_number = arguments["port"].as<uint32_t>();
    std::string host = arguments["host"].as<std::string>();
    std::string backend = arguments["netio-backend"].as<std::string>();
    uint32_t netio_page_size = arguments["netio-page-size"].as<uint32_t>();
    uint32_t netio_l1a_page_size = arguments["netio-l1a-page-size"].as<uint32_t>();
    uint32_t netio_pages_number = arguments["netio-pages-number"].as<uint32_t>();
    bool zero_copy = arguments.count("zero-copy");
    bool wait_input = arguments.count("wait-input");
    bool send_duplicates = arguments.count("send-duplicate-packets");

    felix::bus::FelixTable felixTable;
    felix::bus::ElinkTable elinkTable;

    swrod::test::Barrier barrier(workers_number + (l1a_elink_id ? 1 : 0));
    std::vector<std::shared_ptr<DataPublisher>> workers;
    uint32_t elinks_start = first_elink_id;
    for (uint32_t i = 0; i < workers_number; ++i) {
        std::string uuid = felixTable.addFelix(
                "tcp://" + host + ":" + std::to_string(port_number),
                false, true, netio_pages_number, netio_page_size);

        std::vector<netio::tag> tags;
        for (uint32_t e = 0; e < elinks_per_worker; ++e) {
            uint64_t fid = (i << 24) + ((e / 8) << 16) + ((e % 8) << 8) + first_elink_id;
            tags.push_back(fid);
            elinkTable.addElink(fid, uuid);
        }

        workers.push_back(
            std::make_shared<DataPublisher>(barrier, backend, port_number++,
                    netio_page_size, netio_pages_number, zero_copy,
                    packet_size, ecr_interval,
                    tags, send_duplicates));

        elinks_start += elinks_per_worker;
    }

    if (l1a_elink_id) {
        std::string uuid = felixTable.addFelix(
            "tcp://" + host + ":" + std::to_string(port_number));

        elinkTable.addElink(l1a_elink_id, uuid);
        auto l1a_worker = std::make_shared<L1APublisher>(barrier, backend, port_number++,
                            netio_l1a_page_size, netio_pages_number, zero_copy,
                            packet_size, ecr_interval,
                            std::vector<netio::tag>(1, l1a_elink_id), send_duplicates);
        workers.push_back(l1a_worker);
    }

    felix::bus::Bus bus;
    bus.connect();
    bus.publish("FELIX", felixTable);
    bus.publish("ELINKS", elinkTable);

    if (wait_input) {
        std::cout << "press <Enter> to start sending data ... ";
        getchar();
    }

    for (auto w : workers) {
        w->start();
    }

    uint64_t old_size = 0;
    uint64_t old_count = 0;

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    while (signal_status == 0) {
        usleep(1000000);

        struct timeval tv;
        gettimeofday(&tv, NULL);
        time_t curtime = tv.tv_sec;
        char buffer[16];
        strftime(buffer, 16, "%T.", localtime(&curtime));

        uint64_t size = std::accumulate(workers.begin(), workers.end(), (uint64_t) 0,
                [](uint64_t r, auto & s) {return r + s->getTotalSize();});
        uint64_t count = std::accumulate(workers.begin(), workers.end(), (uint64_t) 0,
                [](uint64_t r, auto & s) {return r + s->getTotalCount();});

        std::cout << buffer << std::setw(6) << std::setfill('0') << tv.tv_usec << " : " << size
            << " bytes sent, message rate = " << (count - old_count) / 1000000.
            << " MHz, throughput = " << (size - old_size) / 1e6 << " MB/s" << std::endl;
        old_count = count;
        old_size = size;
    }

    workers.clear();

    return 0;
}

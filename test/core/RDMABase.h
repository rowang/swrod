/*
 *  RDMABase.hpp
 */

#ifndef SWROD_RDMABASE_HPP_
#define SWROD_RDMABASE_HPP_

#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/eventfd.h>
#include <unistd.h>
#include <rdma/rdma_cma.h>

#include <iostream>
#include <thread>

enum message_id {
    MSG_INVALID = 0, MSG_ACK, MSG_READY, MSG_DONE
};

struct message {
    int id = 0;
};

void rc_die(const char *message);

#define TEST_NZ(x) do { if ( (x)) rc_die("error: " #x " failed (returned non-zero): " ); } while (0)
#define TEST_Z(x)  do { if (!(x)) rc_die("error: " #x " failed (returned zero/null): "); } while (0)

namespace swrod {
    class RDMABase {
    public:
        RDMABase() :
                m_stopped(true), m_poller_stopped(true), m_context(0), m_comm_id(0),
                m_qp(0), m_pd(0), m_cq(0), m_comp_channel(0), m_ec(0) {
            m_event_fd = eventfd(0, EFD_NONBLOCK);
            TEST_Z(m_ec = rdma_create_event_channel());
            TEST_NZ(rdma_create_id(m_ec, &m_comm_id, NULL, RDMA_PS_TCP));
        }

        virtual ~RDMABase() {
            rdma_destroy_id(m_comm_id);
            rdma_destroy_event_channel(m_ec);
        }

        void runEventLoop();

        void stopEventLoop() {
            if (!m_stopped) {
                m_stopped = true;
            }
            stop_poller();
        }

        void post_receive(ibv_mr * mgr);

        void send_remote(ibv_mr * mgr);

        void signal() {
            eventfd_write(m_event_fd, 1);
        }
    protected:
        ::ibv_pd * get_pd() {
            return m_pd;
        }

        void build_connection(rdma_cm_id * idm_comm_id);

        void build_qp_attr(ibv_qp_init_attr *qp_attr);

        void build_params(rdma_conn_param *params);

        void disconnect();

        void poll_cq();

        int poll_fd(int fd);

        void stop_poller() {
            if (!m_poller_stopped) {
                m_poller_stopped = true;
                m_poller_thread.join();
            }
        }

        virtual void signal_received() { }
        virtual void on_pre_conn() = 0;
        virtual void on_connect() = 0;
        virtual void on_completion(ibv_wc & wc) = 0;
        virtual void on_disconnect() = 0;

    protected:
        const uint32_t m_queue_length = 32;
        const uint32_t m_chunks_to_acknowledge = 10;
        const uint32_t m_max_chunks_to_send = 20;
        bool m_stopped;
        bool m_poller_stopped;
        int m_event_fd;
        ibv_context * m_context;
        rdma_cm_id * m_comm_id;
        ibv_qp * m_qp;
        ibv_pd * m_pd;
        ibv_cq * m_cq;
        ibv_comp_channel * m_comp_channel;
        rdma_event_channel * m_ec;
        std::thread m_poller_thread;
    };
}

#endif /* SWROD_RDMABASE_HPP_ */

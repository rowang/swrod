/*
 * FileInput.cpp
 *
 *  Created on: Jan 11, 2022
 *      Author: kolos
 */
#include <string>

#include <EventStorage/pickDataReader.h>

#include <swrod/Core.h>
#include <swrod/Factory.h>
#include <swrod/detail/ptree.h>

#include "FileInput.h"

using namespace swrod;
using namespace swrod::test;

namespace {
    Factory<DataInput>::Registrator __reg__(
            "FileInput",
            [](const ptree& config, const Core& core) {
                return std::make_shared<FileInput>(config, core);
            });
}

FileInput::FileInput(const ptree &config, const Core &core) :
    m_ROB_id(PTREE_GET_VALUE(config, uint32_t, "RobId")),
    m_repetitions(PTREE_GET_VALUE(config, uint32_t, "Repetitions")),
    m_dead_links(PTREE_GET_VALUE(config, uint32_t, "DeadLinksNumber")),
    m_mapping(core.getLinkMapping()),
    m_thread(std::bind(&FileInput::run, this, std::placeholders::_1),
            "FileInput", config.get<std::string>("CPU", ""))
{
    std::string file_name = PTREE_GET_VALUE(config, std::string, "FileName");
    DataReader * data_reader = pickDataReader(file_name);
    if (!data_reader || !data_reader->good()) {
        throw BadConfigurationException(
                ERS_HERE, "Cannot read '" + file_name + "' data file.");
    }

    while (data_reader->good()) {
        try {
            char *buf;
            unsigned int size = 0;

            DRError err = data_reader->getData(size, &buf);
            if (err != EventStorage::DROK) {
                throw BadConfigurationException(
                        ERS_HERE, "Data file '" + file_name + "' is broken.");
            }
            FullEventFragment frag((unsigned int*)buf);
            frag.check();
            m_data_buffer.emplace_back((unsigned int*)buf);

            std::vector<eformat::read::ROBFragment> robs;
            frag.robs(robs);

            auto it = std::find_if(robs.begin(), robs.end(), [this](auto & r) {
                return r.rob_source_id() == m_ROB_id;
            });

            if (it != robs.end()) {
                m_fragments.push_back(*it);
            }
        } catch (ers::Issue &ex) {
            throw BadConfigurationException(
                    ERS_HERE, "Bad data file '" + file_name + "' is given.", ex);
        }
    }

    delete data_reader;

    if (m_fragments.empty()) {
        throw BadConfigurationException(ERS_HERE, "Data file '" + file_name + "' has"
                "no fragments for " + std::to_string(m_ROB_id) + " ROB");
    }
}

void FileInput::subscribeToFelix(const InputLinkId & fid) {
    DetectorLinkId id = m_mapping.detectorResourceId(fid);
    m_subscrptions.insert(std::make_pair(id, fid));
}

void FileInput::unsubscribeFromFelix(const InputLinkId & fid) {
    DetectorLinkId id = m_mapping.detectorResourceId(fid);
    m_subscrptions.erase(id);
}

void FileInput::run(const bool & ) {
    size_t r = 0;

    while (r < m_repetitions) {
        for (auto & f : m_fragments) {
            const uint32_t * buf_start = f.rod_data();
            const uint32_t * buf_end = buf_start + f.rod_ndata();

            uint32_t dead_cnt = m_dead_links;
            while (buf_start < buf_end) {
                uint32_t size = *buf_start & 0xffff;

                if (not dead_cnt) {
                    uint32_t id = *(uint32_t*)(buf_start + 1);
                    auto it = m_subscrptions.find(id);
                    if (it != m_subscrptions.end()) {
                        dataReceived(it->second, (uint8_t*)(buf_start + 2), size * 4 - 8, 0);
                    }
                } else {
                    --dead_cnt;
                }

                buf_start += size;
            }
        }
        ++r;
    }
}

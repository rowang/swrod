/*
 * InternalDataGenerator.cpp
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#include <tuple>
#include <vector>

#include <ers/ers.h>

#include "InternalDataGenerator.h"

#include <swrod/Factory.h>

using namespace swrod;
using namespace swrod::test;
using namespace boost::property_tree;

namespace {
    Factory<DataInput>::Registrator __reg__(
            "InternalData",
            [](const ptree& config, const Core& ) {
                return std::make_shared<InternalDataGenerator>(config);
            });
}

InternalDataGenerator::InternalDataGenerator(const boost::property_tree::ptree& config) :
    m_packet_size(config.get<uint16_t>("PacketSize")),
    m_pileup(config.get<uint16_t>("Pileup", 1)),
    m_l1id_bit_mask(config.get<uint32_t>("L1idBitMask", -1)),
    m_ROBs_per_module(config.get<uint32_t>("ROBsPerModule", 1)),
    m_worker_index(config.get<uint32_t>("WorkerIndex")),
    m_full_mode(config.get<bool>("IsFullMode", false)),
    m_packets_counter(0),
    m_triggers_counter(0),
    m_running(false),
    m_actions_pending(0),
    m_thread([this](const bool & a) { m_full_mode ? runFullMode(a) : runGBTMode(a); },
             "InternalData", config.get<std::string>("CPU", ""))
{
    m_packet = new uint8_t[m_packet_size];
    memset(m_packet, 0, m_packet_size);
    *((uint16_t*)m_packet) = 1; // good checksum value
    *(((uint32_t*)m_packet) + 2) = m_l1id_bit_mask;

    m_l1a_gen = InternalL1AGenerator::instance();

    if (m_full_mode && m_worker_index) {
        m_ttc_queue_id = m_l1a_gen->getTTCQueue();
    }
    else {
        m_ttc_queue_id = m_l1a_gen->allocateTTCQueue();
    }

    m_thread.start();
}

InternalDataGenerator::~InternalDataGenerator() {
    m_thread.stop();
    delete[] m_packet;
    if (not (m_full_mode && m_worker_index)) {
        m_l1a_gen->releaseTTCQueue();
    }
}

void InternalDataGenerator::subscribeToFelix(const InputLinkId& link) {
    std::unique_lock lock(m_mutex);
    m_links.push_back(link);
}

void InternalDataGenerator::unsubscribeFromFelix(const InputLinkId& link) {
    std::unique_lock lock(m_mutex);
    m_links.erase(std::remove(m_links.begin(), m_links.end(), link), m_links.end());
}

void InternalDataGenerator::runStarted(const RunParams & rp) {
    std::unique_lock lock(m_mutex);
    DataInput::runStarted(rp);
    m_packets_counter = 0;
    m_triggers_counter = 0;
    m_links_per_ROB = m_links.size() / m_ROBs_per_module;

    beforeStart();
    m_running = true;

    // This is necessary if SW ROD is configured to work in data-driven mode
    // in which case it will not call runStarted for the Internal L1A generator
    // but this is required to have data generators working
    m_l1a_gen->startGeneration(rp);
}

void InternalDataGenerator::runStopped() {
    std::unique_lock lock(m_mutex);

    // This is necessary if SW ROD is configured to work in data-driven mode
    m_l1a_gen->stopGeneration();

    m_running = false;
    m_condition.wait(lock, [this]{ return !m_triggers_counter; });
    DataInput::runStopped();
    afterStop();
}

void InternalDataGenerator::runFullMode(const bool& active) {
    while (active) {
        if (m_actions_pending) {
            executeActions();
            --m_actions_pending;
        }

        std::unique_lock lock(m_mutex);
        if (not m_running) {
            if (m_triggers_counter) {
                ERS_DEBUG(1, "Triggers sent = " << m_triggers_counter
                        << " packets sent = " << m_packets_counter);
                m_triggers_counter = 0;
                m_packets_counter = 0;
                m_condition.notify_one();
            }
            lock.unlock();
            usleep(5);
            continue;
        }

        uint32_t l1id;
        uint16_t bcid;
        if (!m_l1a_gen->getTTCInformation(m_ttc_queue_id, l1id, bcid)) {
            continue;
        }

        if (m_links.empty()) {
            continue;
        }

        uint32_t offset = m_triggers_counter++ % m_links_per_ROB;
        for (uint32_t i = 0; i < m_ROBs_per_module; ++i) {
            uint32_t link = i*m_links_per_ROB + offset;
            generatePacket(m_links[link % m_links.size()], l1id & m_l1id_bit_mask, bcid);
            ++m_packets_counter;
        }
    }
}

void InternalDataGenerator::runGBTMode(const bool& active) {
    std::vector<std::tuple<uint32_t, uint16_t>> ttc(
            m_pileup, std::make_tuple(0, 0));

    const size_t triggers_skipped = 0;
    size_t pileup_counter = 0;

    while (active) {
        if (m_actions_pending) {
            executeActions();
            --m_actions_pending;
        }

        std::unique_lock lock(m_mutex);
        if (not m_running) {
            if (m_triggers_counter) {
                ERS_DEBUG(1, "Triggers sent = " << m_triggers_counter
                        << " packets sent = " << m_packets_counter);
                m_triggers_counter = 0;
                m_packets_counter = 0;
                m_condition.notify_one();
            }
            lock.unlock();
            usleep(5);
            continue;
        }

        while (pileup_counter < ttc.size()) {
            if (!m_l1a_gen->getTTCInformation(m_ttc_queue_id,
                    std::get<0>(ttc[pileup_counter]), std::get<1>(ttc[pileup_counter]))) {
                break;
            }
            ++pileup_counter;
        }

        if (not pileup_counter) {
            continue;
        }

        if (m_triggers_counter < triggers_skipped) {
            m_triggers_counter += pileup_counter;
            pileup_counter = 0;
            continue;
        }

        for (size_t l = 0; l < m_links.size(); ++l) {
            if (triggers_skipped > (m_triggers_counter + l)) {
                continue;
            }
            for (size_t i = 0; i < pileup_counter; ++i) {
                auto [l1id, bcid] = ttc[i];
                generatePacket(m_links[l], l1id & m_l1id_bit_mask, bcid);
            }
            ++m_packets_counter;
        }

        m_triggers_counter += pileup_counter;
        pileup_counter = 0;
    }
}

void InternalDataGenerator::generatePacket(InputLinkId link, uint32_t l1id, uint16_t bcid) {
    *(uint32_t*)m_packet = l1id;
    *(uint16_t*)(m_packet + 6) = bcid;
    dataReceived(link, m_packet, m_packet_size, 0);
}

/*
 * FileInput.h
 *
 *  Created on: Jan 11, 2022
 *      Author: kolos
 */

#ifndef TEST_CORE_FILEINPUT_H_
#define TEST_CORE_FILEINPUT_H_

#include <memory>
#include <vector>
#include <boost/property_tree/ptree.hpp>

#include <eformat/FullEventFragmentNoTemplates.h>

#include <swrod/DataInput.h>
#include <swrod/InputLinkMapping.h>
#include <swrod/detail/tsl/robin_map.h>
#include <swrod/detail/WorkerThread.h>

using namespace eformat::read;
using namespace boost::property_tree;

namespace swrod {
    class Core;
    namespace test {

        class FileInput : public DataInput {
        public:
            FileInput(const ptree &config, const Core &core);

            void actionsPending() override {
                executeActions();
            }

            void runStarted(const RunParams & rp) override {
                DataInput::runStarted(rp);
                m_thread.start();
            }

            void runStopped() override {
                DataInput::runStopped();
                m_thread.stop();
            }

            void subscribeToFelix(const InputLinkId & link) override;

            void unsubscribeFromFelix(const InputLinkId & link) override;

        private:
            void run(const bool & active);

        private:
            typedef tsl::robin_map<DetectorLinkId, InputLinkId> Subscrptions;

            const uint32_t m_ROB_id;
            const uint32_t m_repetitions;
            const uint32_t m_dead_links;
            const InputLinkMapping & m_mapping;

            Subscrptions m_subscrptions;
            std::vector<std::unique_ptr<unsigned int[]>> m_data_buffer;
            std::vector<eformat::read::ROBFragment> m_fragments;
            detail::WorkerThread m_thread;
        };
    }
}

#endif /* TEST_CORE_FILEINPUT_H_ */

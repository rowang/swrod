/*
 * ROBFragmentCounter.cpp
 *
 *  Created on: Jun 19, 2019
 *      Author: kolos
 */

#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>

#include <chrono>
#include <mutex>

#include <boost/format.hpp>

#include "ROBFragmentCounter.h"

#include <swrod/Core.h>
#include <swrod/Factory.h>

using namespace swrod;
using namespace boost::property_tree;

namespace {
    std::mutex g_screen_mutex;

    Factory<ROBFragmentConsumer>::Registrator __reg__(
            "ROBFragmentCounter",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<ROBFragmentCounter>(config, core);
            });
}

uint32_t ROBFragmentCounter::s_offset = 0;

ROBFragmentCounter::ROBFragmentCounter(
        const boost::property_tree::ptree & config, const Core & core) :
                m_period(config.get<uint32_t>("Period", 1000)),
                m_offset(s_offset++),
                m_ROB_id((boost::format("ROB-%08x") %config.get<uint32_t>("RobConfig.Id", 0)).str()),
                m_show_average(config.get<uint32_t>("Mode", 1) == 2),
                m_running(false),
                m_timer(std::bind(&ROBFragmentCounter::timerCallback, this), m_period, "FragmentCounter")
{ }

ROBFragmentCounter::~ROBFragmentCounter() {
    runStopped();
    s_offset--;
}

void ROBFragmentCounter::insertROBFragment(const std::shared_ptr<ROBFragment> & fragment) {
    if (not m_counter++) {
        m_start = std::chrono::steady_clock::now();
    }
    forwardROBFragment(fragment);
}

void ROBFragmentCounter::runStarted(const RunParams & ) {
    if (!m_running) {
        m_running = true;
        m_timer.start();
    }
}

void ROBFragmentCounter::runStopped() {
    if (m_running) {
        m_running = false;
        m_timer.stop();
    }
}

void ROBFragmentCounter::print() {
    static const uint32_t width = 5;

    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    while (m_history.size() > (w.ws_col - 30)/width) {
        m_history.pop_front();
    }

    std::unique_lock<std::mutex> g_lock(g_screen_mutex);
    // move cursor to the appropriate line and clear it
    std::cout << "\033[" << (w.ws_row - m_offset) << ";1H" << "\033[K";
    std::cout << m_ROB_id << " rates (kHz): ";
    for (std::deque<uint32_t>::iterator it = m_history.begin(); it != m_history.end(); ++it) {
        std::cout << std::setw(width) << std::right << *it;
    }
    // move cursor behind the low-right corner
    std::cout << "\033[" << w.ws_row << ";" << w.ws_col + 1 << "H" << ' ' << std::flush;
}

void ROBFragmentCounter::timerCallback() {

    if (m_show_average) {
        uint32_t rate = 0;
        if (m_start.time_since_epoch().count()) {
            auto now = std::chrono::steady_clock::now();
            uint64_t elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                    now - m_start).count();
            rate = elapsed ? m_counter/elapsed : 0;
        }
        m_history.push_back(rate);
    } else {
        uint32_t rate = (m_counter - m_last_counter)/m_period;
        m_last_counter = m_counter;
        m_history.push_back(rate);
    }

    print();
}

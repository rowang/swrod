/*
 * TestPlugin.cpp
 *
 *  Created on: May 29, 2019
 *      Author: kolos
 */

#include <tuple>

#include <boost/property_tree/ptree.hpp>

#include <ers/ers.h>
#include <is/infoT.h>
#include <RunControl/Common/RunControlCommands.h>

#include <swrod/exceptions.h>
#include <swrod/CustomProcessor.h>
#include <swrod/GBTChunk.h>

#include "ProcessorStatistics.h"

extern "C"
std::tuple<uint32_t, uint32_t, uint16_t>
femuTriggerInfoExtractor(const uint8_t * data, uint32_t size) {
    return std::tuple(*(data + 3), 0xff, 0xffff);
}

extern "C"
std::optional<bool> femuDataIntegrityChecker(const uint8_t * data, uint32_t size) {
    return true;
}

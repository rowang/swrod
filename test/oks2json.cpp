/*
 * oks2json.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: kolos
 */

#include <string>

#include <boost/program_options.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <swrod/exceptions.h>

#include "application/Configuration.h"

using namespace boost::program_options;
using namespace boost::property_tree;

int main(int ac, char *av[]) {
    options_description description("Options");

    description.add_options()("help,h", "produce help message")
        ("database-path,d", value<std::string>()->required(),
                "Database path: 'oksconfig:<xml_file_name>' or 'rdbconfig:<rdb_server_name>[@partition_name]'")
        ("json-file,o", value<std::string>()->required(), "Output file name")
        ("partition,p", value<std::string>()->required(), "Partition name")
        ("swrod-app-name,a", value<std::string>()->required(), "SwRodApplication ID in OKS database");

    variables_map arguments;
    try {
        store(parse_command_line(ac, av, description), arguments);
        notify(arguments);
    } catch (error & ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "Converts SW ROD OKS configuration to Json format" << std::endl;
        description.print(std::cout);
        return 0;
    }

    try {
        swrod::Configuration config(
                arguments["database-path"].as<std::string>(),
                arguments["partition"].as<std::string>(),
                arguments["swrod-app-name"].as<std::string>());

        std::string out_file_name = arguments["json-file"].as<std::string>();
        json_parser::write_json(out_file_name.c_str(), config.propertyTree());
    } catch (swrod::Exception & ex) {
        ers::fatal(ex);
        return 1;
    } catch (ers::Issue & ex) {
        ers::fatal(ex);
        return 1;
    } catch (ptree_error & ex) {
        ers::fatal(swrod::BadConfigurationException(ERS_HERE, ex.what(), ex));
        return 1;
    }

    return 0;
}

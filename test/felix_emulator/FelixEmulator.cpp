/*
 *  FelixEmulator.cpp
 *
 *  Created on: Mar 24, 2020
 */

#include "FelixEmulator.h"
#include <swrod/Factory.h>

using namespace swrod;

namespace {
    Factory<ROBFragmentBuilder>::Registrator __reg__(
            "GBTModeBuilder",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::shared_ptr<ROBFragmentBuilder>(
                        new FelixEmulator(config, core));
            });

    Factory<ROBFragmentBuilder>::Registrator __reg2__(
            "FullModeBuilder",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::shared_ptr<ROBFragmentBuilder>(
                        new FelixEmulator(config, core));
            });
}

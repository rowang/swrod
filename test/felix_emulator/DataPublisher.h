/*
 * DataPublisher.h
 *
 *  Created on: Mar 24, 2020
 *      Author: kolos
 */

#ifndef FELIX_EMULATOR_DATAPUBLISHER_H_
#define FELIX_EMULATOR_DATAPUBLISHER_H_
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <felixbus/bus.hpp>
#include <felixbus/elinktable.hpp>
#include <felixbus/felixtable.hpp>

#include <swrod/exceptions.h>
#include <swrod/Core.h>
#include <swrod/DataInputHandlerBase.h>
#include "test/core/InternalL1AGenerator.h"

#include "NetioSocket.h"

namespace swrod {
    class DataPublisher : public DataInputHandlerBase {
    public:
        DataPublisher(const std::string & id, const InputLinkVector & links,
                const boost::property_tree::ptree & config,
                const std::shared_ptr<DataInput> & input,
                uint32_t page_size = 1048576, uint32_t n_pages = 128) :
            DataInputHandlerBase(id, links, config, input,
                    detail::InputCallback::make<DataPublisher, Link,
                                          &DataPublisher::dataReceived>(*this)),
            m_felix_bus("FELIX", "ZSYS_INTERFACE"),
            m_socket(page_size, n_pages)
        {
            std::string uuid = m_felix_table.addFelix("tcp://" + getInterface()
                   + ":" + std::to_string(m_socket.getPort()));
            for (auto & link : links) {
                m_elink_table.addElink(link.first, uuid);
            }
            m_felix_bus.connect();
            m_felix_bus.publish("FELIX", m_felix_table);
            m_felix_bus.publish("ELINKS", m_elink_table);
        }

        void dataReceived(Link & link, const uint8_t* data, uint32_t size, uint8_t , uint32_t ) {
            m_socket.publish(link.m_fid, data, size);
        }

    private:
        static std::string getInterface() {
            static const char * default_interface = "eth0";
            const char * env = ::getenv("ZSYS_INTERFACE");
            std::string interface = env ? env : default_interface;

            int fd = socket(AF_INET, SOCK_DGRAM, 0);

            struct ifreq ifr;
            ifr.ifr_addr.sa_family = AF_INET;
            strncpy(ifr.ifr_name, interface.c_str(), IFNAMSIZ-1);
            int r = ioctl(fd, SIOCGIFADDR, &ifr);
            close(fd);
            if (r == -1) {
                throw swrod::BadConfigurationException(ERS_HERE,
                        "Can't resolve '" + interface + "' interface to an IP address");
            }

            std::string ip = std::string(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
            ERS_LOG( "'" << interface << "' interface has been resolved to " << ip << " IP address");
            return ip;
        }

    private:
        boost::property_tree::ptree m_config;
        felix::bus::FelixTable m_felix_table;
        felix::bus::ElinkTable m_elink_table;
        felix::bus::Bus m_felix_bus;
        NetioSocket m_socket;
    };
}

#endif /* FELIX_EMULATOR_DATAPUBLISHER_H_ */

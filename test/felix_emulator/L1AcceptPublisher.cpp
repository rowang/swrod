/*
 * L1AcceptPublisher.cpp
 *
 *  Created on: May 7, 2019
 *      Author: kolos
 */

#include "L1AcceptPublisher.h"
#include <swrod/Core.h>
#include <swrod/Factory.h>

using namespace swrod;

namespace {
    Factory<L1AInputHandler>::Registrator __reg__(
            "L1AInputHandler",
            [](const boost::property_tree::ptree &config, const Core &core) {
                return std::make_unique<L1AcceptPublisher>(config, core);
            });
}

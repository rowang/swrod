/*
 * netio_next_subscriber.cpp
 *
 *  Created on: Apr 24, 2019
 *      Author: kolos
 */

#include <dlfcn.h>
#include <string.h>

#include <csignal>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <regex>
#include <string>
#include <thread>
#include <vector>

#include <boost/program_options.hpp>

#include <felixbus/bus.hpp>
#include <felixbus/elinktable.hpp>
#include <felixbus/felixtable.hpp>

#include <netio/netio.h>

typedef std::tuple<uint32_t, uint32_t, uint16_t> (*TriggerInfoExtractor)(const uint8_t *);

TriggerInfoExtractor info_extractor;

bool subscribeToFelix(felix::bus::FelixTable & felix_table,
        felix::bus::ElinkTable & elink_table,
        netio_subscribe_socket & socket, netio_context & context,
        const std::string & interface,
        const uint64_t& link) {
    std::string uuid = elink_table.getFelixId(link);
    std::string address = felix_table.getAddress(uuid);

    struct netio_buffered_socket_attr attr;
    attr.num_pages = felix_table.getNetioPages(uuid);
    attr.pagesize = felix_table.getNetioPageSize(uuid);
    attr.watermark = attr.pagesize * 0.9;

    std::regex regex("tcp://([^:]+):([0-9]+)");
    std::smatch match_result;

    if (std::regex_match(address, match_result, regex) && match_result.size() == 3) {
        std::string host = match_result[1].str();
        uint32_t port = std::stoi(match_result[2].str());
        netio_subscribe_socket_init(&socket, &context, &attr, interface.c_str(), host.c_str(), port);
        netio_subscribe(&socket, link);
        std::clog << "Subscribed to '" << address << "' address for link = "
                << link << " uuid = '" << uuid << "'" << std::endl;
        return true;
    } else {
        std::clog << "ERROR: Subscription to '" << address << "' address for link = "
                << link << " uuid = '" << uuid << "' has FAILED" << std::endl;
        return false;
    }
}

struct ELink {
    ELink() :
        m_last_data_packet_size(0) {
    }

    void savePacket(const uint8_t * data, uint32_t size) {
        if (m_last_data_packet_size < size) {
            m_last_data_packet.reserve(size);
        }
        memcpy(&m_last_data_packet[0], data, size);
        m_last_data_packet_size = size;
    }

    uint64_t m_counter = 0;
    uint32_t m_last_l1id = ~0;
    uint16_t m_last_bcid = ~0;
    std::vector<uint8_t> m_last_data_packet;
    uint32_t m_last_data_packet_size;
};

struct L1A {
    uint32_t m_l1id = ~0;
    uint16_t m_bcid = ~0;
};

const uint32_t c_l1a_buffer_size = 1000000;
std::vector<L1A> l1as(c_l1a_buffer_size);
ELink data_elink;
ELink l1a_elink;

bool l1a_subscribed = false;
bool stopped = false;
bool verbose = false;
std::mutex mutex;

void dump(const uint8_t * data, uint16_t size)
{
    std::clog << std::hex << std::setfill('0');
    for (uint16_t i = 0; i < size; ++i) {
        std::clog << std::setw(2) << (int)data[i] << " ";
    }
    std::clog << std::dec << std::endl;
}

void l1aMessageReceived(netio_subscribe_socket*, netio_tag_t linkid, void * ptr, size_t size) {
    uint8_t * data = (uint8_t*)ptr + 1;
    --size;

    uint32_t l1id = *((uint32_t*)(data + 4));
    uint16_t bcid = *((uint16_t*)(data + 2)) & 0xfff;

    if (verbose) {
        std::unique_lock lock(mutex);
        std::cout << "Got L1A packet from e-link 0x" << std::hex << linkid
                << ": packet #" << std::dec << l1a_elink.m_counter << std::hex
                << " with [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "]" << std::endl;
    }

    uint32_t expected_l1id = l1a_elink.m_last_l1id + 1;
    if (expected_l1id != l1id) {
        expected_l1id = (l1a_elink.m_last_l1id & 0xff000000) + 0x01000000;
        if (expected_l1id != l1id) {
            std::unique_lock lock(mutex);
            uint32_t linkid = *((uint32_t*)(data+4));
            std::clog << "Error for l1A e-link 0x" << std::hex << linkid
                    << ": packet #" << std::dec << l1a_elink.m_counter << std::hex
                    << " has [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "], "
                    << " last [L1ID;BCID] = [0x" << l1a_elink.m_last_l1id << ";0x"
                    << l1a_elink.m_last_bcid << "]"
                    << std::endl;
            std::clog << "current packet : ";
            dump(data, size);
            std::clog << "previous packet: ";
            dump(&l1a_elink.m_last_data_packet[0], *((uint16_t*)&l1a_elink.m_last_data_packet[0]));
        }
    }
    l1as[l1a_elink.m_counter%c_l1a_buffer_size].m_l1id = l1id;
    l1as[l1a_elink.m_counter%c_l1a_buffer_size].m_bcid = bcid;
    l1a_elink.savePacket(data, size);
    l1a_elink.m_last_l1id = l1id;
    l1a_elink.m_last_bcid = bcid;
    ++l1a_elink.m_counter;
}

void dataMessageReceived(netio_subscribe_socket*, netio_tag_t linkid, void * ptr, size_t size) {
    uint8_t * data = (uint8_t*)ptr + 1;
    --size;

    L1A & l1a = l1as[data_elink.m_counter%c_l1a_buffer_size];

    try {
        auto [l1id, l1id_mask, bcid] = info_extractor(data);

        if (verbose) {
            std::unique_lock lock(mutex);
            std::cout << "Got data packet from e-link 0x" << std::hex << linkid
                    << ": packet #" << std::dec << data_elink.m_counter << std::hex
                    << " with [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "]" << std::endl;
        }

        while (data_elink.m_counter >= l1a_elink.m_counter) {
            if (stopped) {
                return;
            }
            usleep(5);
        }
        if ((l1a.m_l1id & l1id_mask) != l1id
                || (bcid != 0xffff && bcid != l1a.m_bcid)) {
            std::unique_lock lock(mutex);
            std::clog << "Error for e-link 0x" << std::hex << linkid
                    << ": packet #" << std::dec << data_elink.m_counter << std::hex
                    << " has [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "], "
                    << " last [L1ID;BCID] = [0x" << data_elink.m_last_l1id << ";0x"
                    << data_elink.m_last_bcid << "], "
                    << " L1A packet [L1ID;BCID] = [0x" << l1a.m_l1id << ";0x"
                    << l1a.m_bcid << "]" << std::endl;
            std::clog << "current packet : ";
            dump(data, size);
            std::clog << "previous packet: ";
            dump(&data_elink.m_last_data_packet[0], data_elink.m_last_data_packet_size);
        }

        data_elink.m_last_l1id = l1a.m_l1id;
        data_elink.m_last_bcid = bcid;
        data_elink.savePacket(data, size);
        data_elink.m_counter++;
    }
    catch (std::exception & ex) {
        std::unique_lock lock(mutex);
        std::clog << "Got exception '" << ex.what() << "'"
                << " for e-link 0x" << std::hex << linkid
                << ": packet #" << std::dec << data_elink.m_counter << std::hex
                << " last [L1ID;BCID] = [0x" << data_elink.m_last_l1id << ";0x"
                << data_elink.m_last_bcid << "], "
                << " L1A packet [L1ID;BCID] = [0x" << l1a.m_l1id << ";0x"
                << l1a.m_bcid << "]" << std::endl;
        std::clog << "current packet : ";
        dump(data, size);
        std::clog << "previous packet: ";
        dump(&data_elink.m_last_data_packet[0], data_elink.m_last_data_packet_size);
    }
}

void dataMessageReceivedNoTTC(netio_subscribe_socket*, netio_tag_t linkid, void * ptr, size_t size) {
    uint8_t * data = (uint8_t*)ptr + 1;
    --size;

    try {
        auto [l1id, l1id_mask, bcid] = info_extractor(data);

        if (verbose) {
            std::unique_lock lock(mutex);
            std::cout << "Got data packet from e-link 0x" << std::hex << linkid
                    << ": packet #" << std::dec << data_elink.m_counter << std::hex
                    << " with [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "]" << std::endl;
        }

        uint32_t expected_l1id = data_elink.m_last_l1id + 1;
        if ((expected_l1id & l1id_mask) != l1id) {
            expected_l1id = (data_elink.m_last_l1id & 0xff000000) + 0x01000000;
            if ((expected_l1id & l1id_mask) != l1id) {
                std::unique_lock lock(mutex);
                std::clog << "Error for e-link 0x" << std::hex << linkid
                        << ": packet #" << std::dec << data_elink.m_counter << std::hex
                        << " has [L1ID;BCID] = [0x" << l1id << ";0x" << bcid << "], "
                        << " last [L1ID;BCID] = [0x" << data_elink.m_last_l1id << ";0x"
                        << data_elink.m_last_bcid << "]"
                        << std::endl;
                std::clog << "current packet : ";
                dump(data, size);
                std::clog << "previous packet: ";
                dump(&data_elink.m_last_data_packet[0], data_elink.m_last_data_packet_size);
                expected_l1id = (expected_l1id & ~l1id_mask) | l1id;
            }
        }

        data_elink.m_last_l1id = expected_l1id;
        data_elink.m_last_bcid = bcid;
        data_elink.savePacket(data, size);
        data_elink.m_counter++;
    }
    catch (std::exception & ex) {
        std::unique_lock lock(mutex);
        std::clog << "Got exception '" << ex.what() << "'"
                << " for e-link 0x" << std::hex << linkid
                << ": packet #" << std::dec << data_elink.m_counter << std::hex
                << " last [L1ID;BCID] = [0x" << data_elink.m_last_l1id << ";0x"
                << data_elink.m_last_bcid << "]" << std::endl;
        std::clog << "current packet : ";
        dump(data, size);
        std::clog << "previous packet: ";
        dump(&data_elink.m_last_data_packet[0], data_elink.m_last_data_packet_size);
    }
}

volatile std::sig_atomic_t signal_status = 0;

void signal_handler(int s) {
    signal_status = s;
}

using namespace boost::program_options;

int main(int ac, char *av[])
{
    std::vector<uint64_t> eids;

    options_description description("Options");

    description.add_options()("help,h", "produce help message")
        ("interface,i", value<std::string>()->required(), "SW ROD data receiving IP address")
        ("plugin-lib,l", value<std::string>()->required(), "SW ROD plugin library name")
        ("trigger-extractor-name,t", value<std::string>()->required(), "Name of the trigger info extractor function")
        ("l1a-elink,a", value<uint64_t>()->default_value(0), "FID of L1A e-link, 0 for no L1A")
        ("verbose,v", "print information for any incoming packet")
        ("data-elink,e", value<uint64_t>()->required(), "FID of a data e-link to subscribe");

    variables_map arguments;
    try {
        store(parse_command_line(ac, av, description), arguments);
        notify(arguments);
    } catch (error & ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (arguments.count("help")) {
        std::cout << "Test application for the 'swrod' package" << std::endl;
        description.print(std::cout);
        return 0;
    }

    uint64_t l1a_fid = arguments["l1a-elink"].as<uint64_t>();
    uint64_t data_fid = arguments["data-elink"].as<uint64_t>();
    std::string interface = arguments["interface"].as<std::string>();
    std::string plugin = arguments["plugin-lib"].as<std::string>();
    std::string trigger_info_extractor_name = arguments["trigger-extractor-name"].as<std::string>();
    verbose = arguments.count("verbose");

    void * handle = dlopen(plugin.c_str(), RTLD_LAZY | RTLD_GLOBAL);

    if (!handle) {
        std::clog << "Can not load " << plugin << " shared library: " << dlerror() << std::endl;
        return 1;
    }

    void * fptr = dlsym(handle, trigger_info_extractor_name.c_str());
    if (!fptr) {
        std::clog << "Can not resolve " << trigger_info_extractor_name
                << " function: " << dlerror() << std::endl;
        return 1;
    }
    info_extractor = reinterpret_cast<decltype(info_extractor)>(fptr);

    felix::bus::Bus felix_bus;
    felix::bus::FelixTable felix_table;
    felix::bus::ElinkTable elink_table;
    felix_bus.setVerbose(false);
    felix_bus.setZyreVerbose(false);

    felix_bus.connect();
    felix_bus.subscribe("FELIX", &felix_table);
    felix_bus.subscribe("ELINKS", &elink_table);

    sleep(2);

    netio_context data_context;
    netio_init(&data_context);

    netio_subscribe_socket data_socket;
    std::thread data_thread([&data_context]() { netio_run(&data_context.evloop);});

    data_elink = ELink();
    int errors = 0;
    if (!subscribeToFelix(felix_table, elink_table, data_socket, data_context, interface, data_fid)) {
        ++errors;
    } else {
        data_socket.cb_msg_received = l1a_fid ? &dataMessageReceived : &dataMessageReceivedNoTTC;
    }

    netio_context l1a_context;
    netio_init(&l1a_context);

    netio_subscribe_socket l1a_socket;
    std::thread l1a_thread([&l1a_context]() { netio_run(&l1a_context.evloop);});

    if (l1a_fid) {
        if (!subscribeToFelix(felix_table, elink_table, l1a_socket, l1a_context, interface, l1a_fid)) {
            ++errors;
        } else {
            l1a_socket.cb_msg_received = &l1aMessageReceived;
            l1a_subscribed = true;
        }
    }

    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);

    while (!errors && signal_status == 0) {
        sleep(1);
    }

    stopped = true;

    netio_terminate(&l1a_context.evloop);
    l1a_thread.join();

    netio_terminate(&data_context.evloop);
    data_thread.join();

    return errors;
}

/*
 * Requester.h
 *
 *  Created on: Mar 11, 2021
 *      Author: kolos
 */

#ifndef TEST_HLT_REQUESTER_REQUESTER_H_
#define TEST_HLT_REQUESTER_REQUESTER_H_

#include <memory>
#include <thread>

#include <felix_proxy/ClientThread.h>

#include "RequesterSession.h"

namespace swrod {
namespace hlt {
    class Requester {

    public:
        Requester(std::vector<std::shared_ptr<RequesterSession>> data_sessions,
                std::shared_ptr<ClearSession> clear_session, uint32_t l1_rate,
                uint32_t l1id_reset_interval, uint32_t eb_request_fraction,
                uint32_t l2_request_fraction, bool apply_correction, uint64_t l1a_fid = 0,
                const std::string & interface = "");

        ~Requester();

    private:

        void netioMessageReceived(uint64_t link, const uint8_t * data, uint32_t size, uint8_t status);

        void run();

        void send(uint32_t l1id);

    private:
        const std::vector<std::shared_ptr<RequesterSession>> m_data_sessions;
        const std::shared_ptr<ClearSession> m_clear_session;
        const uint32_t m_l1_rate;
        const uint32_t m_l1id_reset_interval;
        const uint32_t m_eb_request_fraction;
        const uint32_t m_l2_request_fraction;
        const bool m_apply_correction;
        bool m_running;

        uint64_t m_counter;
        uint64_t m_eb_sent;
        uint64_t m_l2_sent;

        std::thread m_thread;
        std::unique_ptr<felix_proxy::ClientThread> m_felix;
    };
}
}

#endif /* TEST_HLT_REQUESTER_REQUESTER_H_ */

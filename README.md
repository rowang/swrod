# SW ROD
This package provides the implementation of the SW ROD readout application for the ATLAS TDAQ system that followed the [Phase-I SW ROD Architectural Analysis and Design](https://cds.cern.ch/record/2671987) document, which in turn is based on the requirements collected from detector community, which are described in the [Phase-I SW ROD User Requirements](https://edms.cern.ch/document/1832704/1.10) document. The package is distributed as part of the [ATLAS TDAQ software release](http://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/). A software development for custom SW ROD components shall follow [the TDAQ software development procedure](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltCMake).

## Introduction
The SW ROD is envisaged to act in the ATLAS data flow chain as the data-handling interface between the FELIX readout system and the ATLAS High-Level Trigger (HLT). The SW ROD implements ROB fragment building and formatting, which in the Run-1/2 systems were done by the detector specific Readout Driver (ROD) components. The SW ROD also furnishes the same buffering and HLT request processing capabilities as provided by the Readout System (ROS) of the legacy DAQ. 

### SW ROD Application
SW ROD functionality is provided by a number of homogeneous SW processes running on a cluster of commodity computers. All processes originate from the same binary executable, called **swrod_application**, but they diverge by using different configuration parameters such as a set of FELIX E-Links for receiving data, data processing algorithms, HLT request processing parameters and so on.

The SW ROD application is fully integrated with the TDAQ online infrastructure. It implements the Run Control Finite State Machine (FSM) and is configured using the standard TDAQ Configuration service. It also implements the Event Monitoring Sampler interface and publishes its operational statistics to the TDAQ Information Service.

## Integrating Custom Processing into the SW ROD Application
The SW ROD Application provides a framework for executing detector specific code that is required for: 
 * extracting TTC information (L1ID and BCID) from input data packets;
 * verifying integrity of input data packets; 
 * applying custom detector-specific processing to fully assembled ROB fragments.
 
These procedures have to be implemented by a custom detector specific shared library, which should provide three functions with well-defined signatures as described in the following sections. Such a library is advertised to the SW ROD Application via an object of the **SwRodCustomProcessingLib** OKS configuration class that shall define names of the custom functions. Such an object has to be linked with each data channel, aka ROD, that is represented by an instance of the **SwRodDataChannel** OKS class.

> **NOTE:** Custom functions must be declared using **extern "C"** specifier that guarantees that the functions run-time symbolic references will be the same as the function names.

### TTC Information Extraction Function
A function that implements TTC information extraction is mandatory and shall be implemented along the lines demonstrated by the following example:

~~~cpp
extern "C"
std::tuple<uint32_t, uint32_t, uint16_t> testTriggerInfoExtractor(const uint8_t * data, uint32_t size) {
  uint8_t type = data[0] >> 6;
  if (type == 3) {
    throw swrod::Exception(ERS_HERE, "Data packet is corrupt - incorrect packet type");
  }
  if (type == 1) {
    return std::tuple(
        *((uint8_t*)(data + 1)),   // L1ID
        0xff,                      // L1ID size is 8 bits
        0xffff);                   // no BCID
  }
  else {
    return std::tuple(
        *((uint16_t*)(data + 2)),  // L1ID
        0xffff,                    // L1ID size is 16 bits
        *((uint16_t*)data) >> 4);  // BCID
  }
}
~~~

A TTC information extraction function has two parameters - a pointer to the memory block that contains data packet to be processed and the size of this data packet in bytes. The first parameter points to the beginning of the data packet payload meaning that any meta-data, like for example FELIX header or communication protocol header, are intentionally omitted. This function has to return a 3-tuple with the values corresponding to the given data packet:
 * 32-bit value that contains all available bits of the extended L1ID from this data packet.
 * 32-bit mask that defines how many bits of extended L1ID are provided by the given data packet. The bits which are present in the L1ID value shall be set to 1 in this mask, while all other bits have to be set to 0. For example if the data packet contains only the 8 least significant bits of the extended L1ID then the mask value has to be set to **0xFF**. If a full extended L1ID is present in the given packet then the mask has to be set to **0xFFFFFFFF**
 * 16-bit BCID value from the given data packet. If no BCID is present in the data packet the value must be set to **0xFFFF**.
 
This function may also throw **swrod::Exception** if TTC information can not be reliably extracted from the given packet. Any implementation of the SW ROD fragment assembling algorithm has to handle such an exception gracefully. A custom TTC information extraction function may use existing **swrod::Exception** class that is declared in the **swrod/exceptions.h** file or it can declare a custom exception class that in this case must inherit from **swrod::Exception**. 

### Data Integrity Check Function
Optionally, custom plugin library can provide a function to be used to check integrity of a given data packet. If data packet format contains some kind of CRC value that was calculated using a known algorithm then such a function can apply the same algorithm to this packet and compare its result with that value. The following pseudo-code illustrates this idea:

~~~cpp
extern "C"
std::optional<bool> testDataIntegrityChecker(const uint8_t * data, uint32_t size) {
  if (!contains_checksum_value(packet)) {
    return std::nullopt; // packet has no checksum
  }
  else {
    return (calculate_checksum(packet) == get_checksum_value(packet));
  }
}
~~~

It is up to the fragment building algorithm implementation how to use this function. For performance reasons existing algorithm implementations call this function only in case of TTC information mismatch between data and L1A packets.

### Custom Processing 
The last function that may be provided by a custom plugin library is also optional and should be implemented only if a detector specific processing has to be applied to the ROB fragments produced by the fragment building algorithm. In this case such a function has to implement a factory for the detector specific class that implements **swrod::CustomProcessor** interface. The following code shows a possible implementation of such a function:

~~~cpp
extern "C"
swrod::CustomProcessor * createTestCustomProcessor(const boost::property_tree::ptree & config) {
    return new TestCustomProcessor(config.get<int64_t>("CustomLongIntAttribute"));
}
~~~

> **Note**: The **config** object passed to this function contains configuration parameters declared in the corresponding instance of the **SwRodDataChannel** OKS class. It is possible to add arbitrary number of custom parameters to the **config** object by creating a new OKS class that inherits **SwRodDataChannel** and declares these parameters as its attributes. To get the value of a custom attribute one should use the *boost::property_tree::ptree::get()* function providing the corresponding attribute type as template parameter and the attribute name as the function argument.

A custom class that inherits the **swrod::CustomProcessor** interface has to implement the _processROBFragment(swrod::ROBFragment & )_ pure virtual function. For scalability reasons SW ROD may create multiple instances of the **swrod::CustomProcessor** class as defined by the **WorkersNumber** attribute of the corresponding **SwRodCustomProcessor** configuration object. Each instance will be used in a dedicated thread that implies that a **swrod::CustomProcessor** interface implementation shall not worry about thread safety unless it uses some global resources. It is guaranteed that each object of the custom processing class will be used by exactly one thread so there is no need to protect access to the object's local attributes.

The _processROBFragment(swrod::ROBFragment & )_ function will be called for every ROB fragment produced by the fragment building algorithm of the corresponding ROB. The ROB fragment is passed to this function as a reference to an instance of the **swrod::ROBFragment** class. This class contains a number of constant attributes, which describe read-only properties of the fragment. There is also a number of mutable attributes, which can be freely modified by the custom processing procedure, namely:
 * **m_detector_type** - 32-bit value that will be set to the _Detector Event Type_ field of the ROD header
 * **m_rod_minor_version** - 16-bit value that will be set to the lower 2 bytes of the _Format Version Number_ field of the ROD header
 * **m_status_front** - if set to true the ROD fragment status words will be placed right after the ROD header, otherwise they will be put at the end of the ROD fragment. Default value is true.
 * **m_status_words** - an originally empty vector of status words. Custom processor may add any number of 32-bit words to this vector. All these words will be added to the ROD fragment status section.
 * **m_data** - this is a vector containing the fragment payload that may be split into a number of memory blocks. Normally this number is equal to the number of data receiving threads, but this is not always guaranteed as this may depend on a particular fragment building algorithm implementation. Each memory chunk is represented by an object of **swrod::ROBFragment::DataBlock** class that provides a few functions to access and modify the data it contains. A format of the memory chunk depends on the fragment building algorithm that was used to produce this ROB fragment. Detailed description of the format of data fragments produced by the existing SW ROD algorithm implementations is given in the next chapters.
 
#### ROB Fragment Memory Management
A custom processing function may need to reformat the ROB fragment payload in a way that would require to increase its size. In such a case it is strongly recommended to set the corresponding **MaxMessageSize** parameter of the **SwRodFragmentBuilder** configuration object such that the memory blocks allocated by the algorithm will have enough spare space to accommodate the extra amount of data. The size of the memory blocks allocated by the default algorithm implementations is equal to the product of the **MaxMessageSize** parameter and the number of input links that have been used to build this memory block. If, by any reason, such configuration can not be done, then the custom processing function may allocate a new contiguous memory block of sufficient size using either the standard C++ **new** operator or a custom memory management routine and copy reformatted data into the new block. The following example shows how this can be done.

~~~cpp
class TestCustomProcessor: public swrod::CustomProcessor {
public:
    explicit TestCustomProcessor(uint64_t tag) {
        m_tag = tag;
    }
    void processROBFragment(swrod::ROBFragment & fragment) override {
        fragment.m_status_words.push_back(0x0a);
        fragment.m_status_words.push_back(0x0b);
        fragment.m_status_words.push_back(0x0c);
        fragment.m_rod_minor_version = 15;
        fragment.m_detector_type = 0x222;
        fragment.m_status_front = false;

        for (auto & block : fragment.m_data) {
            swrod::GBTChunk::Header header{0, 0, 0, 0xffffffff};
            if (!block.append(header, &m_tag, sizeof(m_tag))) {
                // The memory block is not large enough to accommodate extra data
                // Allocate a new memory block of sufficient size, copy the
                // original data there and add the custom tag at the end

                // The size is in 4-byte words
                uint32_t size = block.dataSize() +
                        ((sizeof(m_tag) + sizeof(swrod::GBTChunk::Header) + 3)>>2);
                uint32_t * data = new uint32_t[size];
                memcpy(data, block.dataBegin(), block.dataSize()<<2);

                block = swrod::ROBFragment::DataBlock(data, size, block.dataSize());
                block.append(header, &m_tag, sizeof(m_tag));
            }
        }
    }

private:
    uint64_t m_tag;
};

~~~

Custom processing function may also change the size of the **m_data** vector by either adding new memory blocks or removing the obsolete ones. If for example **m_data** contains multiple memory blocks that would have to be completely re shuffled, it may be more efficient to allocate a single memory block of sufficient size and copy the content of the original memory blocks into the new one using a desired translation. Finally original memory blocks shall be replaced with the new one in the **m_data** vector.

### Using Custom Plugin Test application
The **swrod** packages provides an application that can be used for validation and profiling of a custom plugin. The application is called **swrod_custom_plugin_test** and can be used in the following way:
  * For the first time it has to be given five parameters: a name of the data file to be used as data source, a name of the shared library that implements the plugin to be tested and the names of the three custom functions which this plugin implements. Optionally one can use _-o <json file name>_ option to save the new configuration to the given Json file.
  * If the Json configuration file has been produced the test application can be started with this file as a sole input parameter using _-i <json file name>_ command line option.

## Fragment Building Algorithms
A SW ROD fragment building algorithm implementation is supposed to collect data packets, which have the same L1 Trigger Identifier (L1ID), from a given set of input links and combine them along with the corresponding TTC information into a new object of the **swrod::ROBFragment** class. This class provides _serialize()_ function that can be used to convert the **swrod::ROBFragment** data into a number of contiguous memory chunks, which hold the content of this ROB  fragment that is formatted in accordance to the [ATLAS Event Format specification](https://cds.cern.ch/record/683741). The high-level structure of a ROB fragment is shown in the following table.

 Field       | Description
 :--------   | --------
 ROB Header  | Information is mostly taken from L1A packet |
 ROD Header  | Some information is duplicated from the ROB header. Other information can be added by detector custom processing
 ROD Data    | The format is detector specific and may also depend on the fragment building algorithm implementation
 ROD Trailer | Contains detector specific information that can be added by by detector custom processing

Data for the ROB and ROD headers are mostly taken from the Level 1 Trigger Accept (L1A) message that corresponds to the given L1ID and are normally independent of the algorithm implementation. The ROD data portion of the fragment contains an aggregation of the data chunks received via multiple input links from the detector Front-End electronics, which internally use detector specific format. The way in which these data chunks are aggregated depends on the algorithm implementation. 

SW ROD package provides two fragment building algorithms that can be used to handle data received from FELIX in the following ways:
 * GBT mode algorithm aggregates data chunks from all E-links associated with the given SwRodDataChannel object into a single ROD Data block using chunks' L1IDs for alignment. The algorithm takes at most one data chunk from a given E-link for the same ROB fragment. Receiving more than one data chunk in a raw with the same L1ID from the same E-link is considered an error. The algorithm expects that every individual data chunk has a size that is a multiple of 4 bytes. If this is not the case then the algorithm adds padding zeros at the end of the chunk to make it 4-byte aligned.
 * FULL mode algorithm treats every individual data chunk from any E-link associated with the given SwRodDataChannel as a completely built ROD Data, that may also optionally contain ROD Header and ROD Trailer elements. This algorithm also expects that every incoming data chunk has a size that is multiple of 4 bytes. If this is not the case the algorithm adds padding zeros at the end of the chunk to make it 4-byte aligned.

A new algorithm can be implemented and added to the SW ROD as a plugin if required.

Both GBT and FULL mode algorithms can be used either in TTC-driven or in data-driven mode. For the latter no Level1 Accept packets are received by the algorithms. The mode of operation depends on the state of the **L1AHandler** relationships of the **SwRodConfiguration** and **SwRodModule** configuration objects. If both relationships are empty event builders will not subscribe for L1A messages and will run in data-driven mode. Otherwise they will subscribe for the TTC E-Link to get L1 Accept messages and use them for fragment building.

### SwRodFragmentBuilder configuration class
This is an abstract class that defines a number of parameters that are common for any fragment building algorithm implementation. These parameters are:
 * **BuildersNumber** - The number of dedicated threads, which are notified when a new ROB fragment data payload is ready and take care of creating a new instance of **swrod::ROBFragment** class for this data and passing it to the ROB Fragment Consumers. These threads are used to disentangle fragment builders from fragment consumers and reduce a possible impact of slow consumers on fragment building performance. If **BuildersNumber** number is set to zero, then no building threads are used, in which case one of the fragment builder's working threads will execute the above procedure. 
 * **FlushBufferAtStop** - Defines fragment builder behavior during Stop-of-Run procedure. If set to 1, the algorithm stops data processing immediately upon receiving SoR command. The data which were present in the internal buffers are flushed. If set to 0, the algorithm keeps processing data from its internal buffers until they get empty. 
 * **L1AWaitTimeout** - Building a ROB fragment may require information from the corresponding L1 Accept packet. The timeout defines the maximum time in milliseconds to wait for L1A packet when some information from this packet is required. If the required L1A packet does not arrive within this timeout fragment building procedure continues using default values for the missing information. 
 * **MaxMessageSize** - The maximum size in bytes of a single data hunk that may arrive from an individual input link. The algorithm will use this value to preallocate memory blocks for the ROB fragments to be built. The size of these blocks is equal to a product of the **MaxMessageSize** and a number of input links which provide data to be aggregated to a single ROB fragment. A value of this parameter has to be carefully chosen. If it is too low this can cause ROB fragments truncation. If it is too high this may result in excessive memory footprint, which may affect the algorithm performance.
 * **ReadyQueueSize** - The size of the queue that is used to hold references to completely aggregated data blocks. This queue is used by the building threads that are defined via **BuildersNumber** parameters. The building threads take the references from this queue and use them to create new instances of the **swrod::ROBFragment** class. If **BuildersNumber** is set to zero, this parameter is ignored as no queue will be used in this case.
 * **ResynchTimeout** - This value defines time in milliseconds to wait for building of the ROB fragments that correspond to the last L1ID produced before the Trigger was put on hold. This timeout is used for the stopless recovery procedure to make sure that the fragment builder will be properly synchronized with the rest of the read-out system when the Trigger is resumed.

### GBT Fragment Building Algorithm
The **ROD data** produced by this algorithm is split into a number of a contiguous memory blocks, with each block containing copies of data packets received from a subset of the E-Links associated with the given ROB in the SW ROD configuration. The number of such blocks is equal to the number of data receiving threads defined via the **WorkersNumber** attribute of the corresponding **SwRodModule** instance. By default the algorithm uses one reader thread for all the E-Links and therefore puts all data packets into a single memory block. 
Each memory block contains a sequence of data packets received from the associated E-Links with each packet preceded with a meta-information header that has the following structure:
 * **size** - 16-bit value that contains a total size (in 4-byte words) of the data packet including the size of the header itself.  
 * **felix_status** - 8-bit status of the data packet provided by FELIX
 * **swrod_status** - 8 bit status of the data packet assigned by the fragment building algorithm. This status may contain any combination of the following flags:
    * **swrod::GBTChunk::Status::Ok (0)** -  no errors detected for this packet
    * **swrod::GBTChunk::Status::Corrupt (1)** - custom TTC information extraction has thrown exception for this packet
    * **swrod::GBTChunk::Status::CRCError (2)** - custom data integrity checking function has returned false for this packet
    * **swrod::GBTChunk::Status::L1IdMismatch (4)** - the packets L1 ID does not match L1 ID from the corresponding L1A packet
    * **swrod::GBTChunk::Status::BCIdMismatch (8)** - the packets BC ID does not match BC ID from the corresponding L1A packet
 * **link_id** - 32-bit detector resource ID that identifies the origin of this data packet

> **Note:** The _link_id_ field contains **DetectorResourceId** value that corresponds to the FELIX E-Link ID as defined by the respective instance of the **SwRodInputLink** OKS configuration class.

#### SwRodGBTModeBuilder configuration class
This OKS configuration class inherits from the **SwRodFragmentBuilder** and adds a few configuration parameters that are specific for the FULL mode algorithm, namely:
 * **BufferSize** - Defines the maximum size of the main aggregation buffer in terms of a number of ROB fragments, which can be built simultaneously. This parameter also indirectly affects the data input timeout, i.e. a time to wait before terminating aggregation of a ROB fragment that still misses data chunks from one or several input links. The timeout value can be explicitly defined in the SW ROD configuration via the **DataReceivingTimeout** but at the same time incomplete fragments may be produced by the GBT algorithms if the aggregation buffer gets full, which would happen when **BufferSize** value divided by the current input data rate is smaller than the explicitly given timeout. In this case effective timeout can be calculated with the following formula:
 
~~~math
EffectiveTimeout(seconds) = min(DataReceivingTimeout / 1000, BufferSize / InputRate(Hz))
~~~
 * **DataReceivingTimeout** - defines a a timeout in milliseconds for ROB fragment data aggregation. If the given number of milliseconds is passed after receiving the first data chunk for a particular ROB fragment this fragment is considered as built and it will be passed to the fragment consumers even if it does not contain data chunks from all the E-links associated with the given ROB. By default this attribute is set to zero, which disables the time-based timeout. In this case effective timeout will be defined by the buffer size as explained in the previous paragraph.
 * **MinimumBufferSize** - Defines the minimum size of the main aggregation buffer in terms of a number of ROB fragments. The buffer will always try to shrink itself to this value (but not beyond) whenever the number of concurrently built fragment goes down.
 * **DropCorruptedPackets** - This parameter defines what to do with data chunks that can not be unanimously attributed to any ROB fragment due to containing corrupt data or arriving too late. If this parameter is set to 1 then such data chunks will be dropped. Otherwise the chunks will be assigned to the fist of the currently being built ROB fragments, in which case the corresponding error bits, that explain the origin of the error, will be set to the **swrod_status** status word of the chunk's local header and a corresponding error bits will be set to status word of the ROB fragment header.
 * **RecoveryDepth** - If an incoming data chunk contains L1ID and BCID values that don't match the algorithm's expectations, the algorithm will check if this mismatch can be explained by an assumption that a number of previous data packets from the same E-link has been missed. To check this hypothesis the algorithm will try to match the chunk's L1ID and BCID to to the corresponding values of the known L1 Accept packets. The **RecoveryDepth** parameter defines a number of L1A packets to be checked as this is the only reliable way to terminate this procedure if the mismatch was caused by data corruption and not by data chunks been missed.

#### Error Handling
The default GBT event aggregation algorithm implements error handling as described by the [SW ROD Error Use Cases](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/SWRodInputErrors) wiki page. The algorithm may produce incomplete ROB fragments, i.e. fragments that miss data chunks from one or several E-Links. This may happen if data from these E-Links did not arrive within the timeout, that is defined in the algorithm configuration. 

### FULL Mode Fragment Building Algorithm
This algorithm receives data packets from the given set of E-Links defined in the corresponding SW ROD configuration. It assumes that each data packet contains a fully aggregated **ROD data** payload that may optionally include **ROD header** and **ROD trailer** elements. The latter is controlled by the **RODHeaderPresent** attribute of the **SwRodFullModeBuilder** configuration object. The algorithm combines these data packets with the corresponding TTC information into objects of the **swrod::ROBFragment** class.

#### SwRodFullModeBuilder configuration class
This OKS configuration class inherits from the **SwRodFragmentBuilder** and adds the **RODHeaderPresent** parameter. If this parameter is set to **1** then the algorithm assumes that each incoming data packet already contains **ROD header** and **ROD trailer** parts and therefore will not add them. Otherwise the algorithm will generate both **ROD header** and **ROD trailer** by itself.

## Configuring a SW ROD Application
A SW ROD application has to be configured using OKS configuration service. For convenience all OKS classes that can be used for this purpose have their names started with **SwRod** prefix. These classes are defined in the **daq/schema/swrod.schema.xml** OKS schema file that has to be included by any SW ROD OKS configuration file. A fully functional example of a SW ROD configuration can be found in the 
[data/SwRodSegment.data.xml](data/SwRodSegment.data.xml) file located in the **swrod** package.

SW ROD configuration schema facilitates splitting of a SW ROD application configuration between a number of files, which can be maintained independently by detector and TDAQ experts. The aim is to make clear distinction between detector specific part of the configuration and the common part that has to be maintained centrally by the TDAQ. The following diagram shows a recommended way of handling a SW ROD configuration. The green boxes in this diagram represent classes that should be instantiated in the detector specific part of configuration. The boxes with yellow background denote the classes which have to be used for creating the TDAQ portion of the SW ROD configuration that will be under responsibility of the TDAQ team. More details will be given in the following sections.

![](doc/configuration.png "SW ROD configuration")
<table height=0><tr><td>
@image latex doc/configuration.png "SW ROD configuration" width=400px
</td></tr></table>

> **Note:** **SwRodApplication**, **SwRodModule** and **SwRodRob** classes inherit from the legacy read-out configuration classes to make the new SW ROD configuration compatible with the legacy one.

### Detector Specific Configuration
Detector experts are expected to maintain objects of the following three classes:
 * **SwRodInputLink** is used to define a set of E-Links for receiving data
 * **SwRodDataChannel** defines a mapping of E-Links to ROB fragments
 * **SwRodCustomProcessingLib** class provides configuration of detector specific custom processing plugins

#### SwRodInputLink class
This class is used to describe a set of input E-Links for a particular detector. It also implements the mapping of FELIX-specific E-Link IDs to detector specific Resource IDs. This class has three attributes:
 * **FelixId** - ID of this link as defined by FELIX system. Such an ID shall be unique within ATLAS.
 * **DetectorResourceId** - ID of the detector read-out element that is connected to this FELIX link. Detector resource ID shall be unique for a given sub-detector.
 * **DetectorResourceName** - human-readable name of the detector read-out element

It is recommended to put all instances of this class into a dedicate OKS configuration file (or a set of files), which then can be effectively shared by SW ROD configuration segments.

#### SwRodDataChannel class
An object of this class defines a set of input links for a given ATLAS data channel (ROD) as well as a custom processing plugin that has to be used for this channel. It has the following relationships:
 * **Contains** - this relationships is inherited from **ResourceSetAND** class and has to contain references to the objects of the **SwRodInputLink** class that represent the corresponding E-Links
 * **CustomLib** - the reference to an instance of the **SwRodCustomProcessingLib** class

It is recommended to put all instances of this class into a separate OKS configuration file, which has to include files defining objects of the **SwRodInputLink** class.

#### SwRodCustomProcessingLib class
This class should be used for configuring custom detector plugins for the SW ROD. It has the following attributes:
 * **LibraryName** - the name of the shared library that implements the custom plugin
 * **TrigInfoExtractor** - the name of the mandatory trigger information extraction function. 
 * **DataIntegrityChecker** - the name of the optional data integrity checking function. If the plugin does not provide this function then this attribute shall be left empty.
 * **ProcessorFactory** - the name of the optional custom processor factory function. If the plugin does not provide this function then this attribute shall be left empty.

Each instance of the **SwRodDataChannel** class has to be linked with an appropriate instance of the **SwRodCustomProcessingLib** to provide the necessary information for data processing. It is recommended to keep an instance of this class in the same OKS configuration file that declares the corresponding data channels.

### TDAQ Specific Configuration
TDAQ configuration defines a number of TDAQ specific parameters for all SW ROD applications, in particular:
 * computers where the SW ROD applications will be running
 * the mapping of the data channels (RODs) to the SW ROD applications
 * HLT request handling and Event Monitoring configuration parameters

#### SwRodApplication Class
An instance of the **SwRodApplication** class is an entry point to a SW ROD configuration. It serves multiple purposes:
 * defines the standard Run Control Application parameters for the **swrod_application** process
 * points to an instance of the **SwRodConfiguration** class that defines the TDAQ specific portion of the SW ROD configuration
 * contains a set of **SwRodRob** objects which are used to provide the standard ATLAS ROB-to-ROD mapping where RODs are represented by the corresponding instances of the **SwRodDataChannel** class that are declared by the detector specific portion of the SW ROD configuration.

#### SwRodConfiguration Class
The **SwRodConfiguration** class defines the following parameters:
 * **Plugins** - a list of **SwRodPluginLib** objects, which reference shared libraries that provide implementation of the core SW ROD interfaces. By default this list shall contain a reference to the  **libswrod_core_impl.so** library, which provides default implementations of these interfaces.
 * **L1AHandler** - a pointer to an instance of a class that inherits **SwRodL1AInputHandler** interface. By default this relationship should point to an instance of the **SwRodDefaultL1AHandler** class, which provides default implementation of L1 Accept message handler.
 * **Consumers** - a list of objects implementing **SwRodFragmentConsumer** interface. Consumers from this list will get all fragments for all ROBs produced by the current SW ROD application. Note that the order of objects in this list matters.  ROB fragments will be passed to the consumers in the same order as they are linked to the **SwRodConfiguration** instance. For example if the **SwRodEventSampler** consumer precedes the **SwRodCustomProcessor** one then any monitoring task connected to the current SW ROD application will ROB fragments without custom processing been applied.
 * **InputMethod** - a reference to an object implementing **SwRodInputMethod** interface. For getting data from FELIX one should reference to an instance of the **SwRodNetioInput** or **SwRodNetioNextInput** class depending on the version of the FELIX software being used.

![](doc/swrod-configuration.png "Top level configuration of a SW ROD application")
<table height=0><tr><td>
@image latex doc/swrod-configuration.png "Top level configuration of a SW ROD application" width=400px
</td></tr></table>

#### SwRodFragmentConsumer interface implementations
**SwRodFragmentConsumer** is an abstract base class for any resource that deals with fully built ROB fragments. It declares three attributes:
 * **Type** - this is a string ID of a specific fragment builder type that is used to create an instance of the respective consumer at run time. Each sub-class of the **SwRodFragmentConsumer** shall provide an unique type name to be used for its instantiation.
 * **WorkersNumber** - number of worker threads for this consumer
 * **CPU** - affinity of the worker threads of this consumer will be set to the given CPU cores. This is a string parameter that contains numbers separated by commas and may include ranges. For example: 0,5,7,9-11.

SW ROD provides multiple default implementations of the **SwRodFragmentConsumer** interface that can be configured by using the following OKS classes: 
 * **SwRodFileWriter** - an instance of this class writes data produced by the SW ROD application to a standard ATLAS raw data file in a form of  standard ATLAS events that combine fragments from all ROBs handled by this SW ROD application based on their L1IDs.
 * **SwRodHLTRequestHandler** - an instance of this class responds to the standard HLT data and clear requests in the same way as ROS. From the HLT point of view a SW ROD application is indistinguishable from a ROS one.
 * **SwRodEventSampler** - can be used to create an instance of Event Sampler for the given SW ROD application. Event Sampler collates fragments from all ROBs handled by this SW ROD application into a single ATLAS event based in their L1IDs and serves such events to a monitoring application via the standard TDAQ Event Monitoring interface. The monitoring application can connect to the Event Sampler using it's _SamplerType_ and _SamplerName_ attributes. The **SamplerType** attribute for the SW ROD Event Sampler is always set to the "SWROD" string while the **SamplerName** attribute is set to the ID of the SW ROD application (i.e. the ID of the corresponding **SwRodApplication** instance in the OKS configuration).
 * **SwRodCustomProcessor** - an instance of this class applies detector specific custom processing to fully built ROB fragments.

Fragment Consumers can be attached to the SW ROD application at two levels: per ROB via **SwRodRob**::_Consumers_ relationship as well as per application via the **SwRodConfiguration**::_Consumers_ relationship. However there are some implementation specific restrictions that should be taken into account when configuring consumers. They are summarized in the following table. 

 SwRodConsumer        	| SwRodRob 	| SwRodConfiguration
 :-------------   		| :------:  | :-----------:
 SwRodFileWriter 		| Yes		| Yes
 SwRodHLTRequestHandler | Yes 		| No
 SwRodEventSampler	    | No		| Yes, limited to one instance per SW ROD
 SwRodCustomProcessor   | Yes 		| No

#### SwRodInputMethod interface implementations
**SwRodInputMethod** is an abstract class that defines interface for getting input data into SW ROD. SW ROD provides several implementations of this interface:
 * **SwRodInternalDataGenerator** - in memory data generator that is used for tests
 * **SwRodNetioInput** - can be used to receive data via NetIO protocol. This class is kept for backward compatibility only and shall no longer been used. Use **SwRodFelixInput** instead. 
 * **SwRodNetioNextInput** - can be used to receive data via netio-next protocol. This class is kept for backward compatibility only and shall no longer been used. Use **SwRodFelixInput** instead. 
 * **SwRodFelixInput** - can be used to receive data via the **FelixClient** interface. 

The latter has a number of attributes which can be used to configure **FelixClient** communication interface, namely:
 * **DataNetwork** - name or IP address of the network that shall be used for receiving data
 * **FelixBusGroupName** - FELIX Bus group name
 * **FelixBusDirectory** - name of the file system directory which is used by the FELIX Bus for storing its internal data.
 * **FelixBusInterface** - this attribute is no longer used.
 * **FelixBusTimeout** - timeout, in milliseconds for link resolution via FELIX bus. 

#### SwRodRob Class
Each instance of the **SwRodRob** class provides configuration for a specific ROB or in another words defines portion of the detector readout from which data have to be combined to a single fragment (ROB fragment). This class has the following parameters:
 * **Id** - ROB identifier that must be unique across all SW ROD and ROS components. This ID is used by HLT for 
 requesting the corresponding ROB fragments.
 * **FragmentBuilder** - a reference to an instance of a class that inherits **SwRodFragmentBuilder** interface. Such an object provides configuration for the data aggregation algorithm, which is used to build fragments for the given ROB.
 * **Consumers** - a list of objects implementing **SwRodFragmentConsumer** interface. Consumers from this 
 list will receive all fragments that are built for the given ROB.
 * **Contains** - this relationship is inherited from the **ResourceSetAND** class and is used to reference an instance of the **SwRodDataChannel** class. This relationship provides the sole link between the detector and TDAQ specific portions of the SW ROD application configuration.
 
> **Note:** For compatibility with the legacy read-out system configuration **Contains** is a multi-value relationship that potentially could reference more than one **Resource** object. However for a valid SW ROD configuration this relationship must point to exactly one unique **SwRodDataChannel** instance.

#### SwRodModule Class
This class provides data receiving configuration for a given set of **SwRodRob** objects, which are referenced via its **Contains** relationship. This class has the following attributes:
 * **WorkersNumber** - number of threads for receiving input data. 
 * **CPU** - a string that defines CPU affinity of the data receiving threads. A value may contain CPU numbers separated by commas and may optionally include ranges. For example: 0,5,7,9-11. If this string is empty then no affinity will be assigned.
 * **L1AHandler** - if this relationship is not empty then the a dedicated receiver of L1 Accept data will be created to be used exclusively by the ROBs that belong to this **SwRodModule**. If this relationship is empty then these ROBs will use the global L1 Accept receiver that is created from the object referenced by the **L1AHandler** relationship of the **SwRodConfiguration** instance. If this relationship is empty as well then the fragment building algorithms for the ROBs belonging to this **SwRodModule** will operate in data-driven mode.
 * **Contains** - this relationship is inherited from the **ResourceSetAND** class and is used to reference objects of the **SwRodRob** class, which will share input parameters defined by the **SwRodModule** instance.

## Customizing SW ROD application
SW ROD declares three abstract interfaces for its main components:
 * **DataInput** - this interface can used for receiving input data from a given source, e.g. from FELIX
 * **ROBFragmentBuilder** - this interface can used to implement an algorithm of building of ROB fragments from the data received via **DataInput** interface
 * **ROBFragmentConsumer** - this interface can used for implementing a sopecific processing of fully built ROB fragments

Default implementations of these interfaces are provided by the **libswrod_core_impl.so** library and can be used via the the corresponding classed of the SW ROD configuration. SW ROD also provides a way of integrating custom implementations of these interfaces into the standard SW ROD application.

### Making a Custom Interface Implementation
The following example shows a simple implementation of the **ROBFragmentConsumer** interface, which just counts the incoming ROB fragments.

~~~cpp
class ROBFragmentCounter : public swrod::ROBFragmentConsumer {
public:
    ROBFragmentCounter(const boost::property_tree::ptree & config, const swrod::Core & core) 
     : m_counter(0),
       m_ROB_id(-1) {
       m_output_frequency = config.get<uint32_t>("OutputFrequency");
       if (config.count("RobConfig")) {
           m_ROB_id = config.get<uint32_t>("RobConfig.Id");
       }
    }

    void insertROBFragment(const std::shared_ptr<swrod::ROBFragment> & fragment) override {
       if ((++m_counter % m_output_frequency) == 0) {
           std::cout << m_counter << " fragments have been built for ROB " << m_ROB_id << std::endl;
       };
       forwardROBFragment(fragment);
    }

    void runStarted(const RunParams & run_params) {
       m_counter = 0;
    }
private:
    uint32_t m_output_frequency;
    uint32_t m_ROB_id;
    uint64_t m_counter;
};
~~~

An instance of this consumer can be used at the level of an individual ROB as well as at the level of a whole SW ROD application.  Each time a new ROB fragment is produced it is passed to the instance of the **ROBFragmentCounter** class via the _insertROBFragment()_ function. In this example the implementation of this function increments the fragment counter and then forwards the given ROB fragment to the other consumers by calling the _forwardROBFragment()_ function. In addition every **m_output_frequency** fragments the function prints the fragment counter to the standard output. The value of the **m_output_frequency** parameter is taken from the OKS configuration. The following section explains how that is implemented.

### Configuring Custom Interface Implementation
The **ROBFragmentCounter** class constructor gets a reference to the **boost::property_tree::ptree** instance that represents parameters taken from the corresponding OKS class. For a new type of consumer a new OKS class that inherits from the **SwRodFragmentConsumer** has to be declared. This can be done using the following procedure:
 * run the OKS schema editor and create a new OKS schema file
 * add include of the **daq/schema/swrod.schema.xml** OKS schema file into the new file
 * create a new class (in this case **ROBFragmentCounter**) inheriting it from the **SwRodFragmentConsumer** class
 * add a new attribute(s) (in this case one attributed called **OutputFrequency**) to the new class
 * save the new schema file

The new schema file has to be included by the SW ROD configuration. After that one can create a new instance of the **ROBFragmentCounter** class and add it either to a **SwRodRob** or to a **SwRodConfiguration** instances depending on whether counting has to be done at the level of an individual ROB or for a whole SW ROD application.

> **Note**: **ROBFragmentCounter** constructor implementation uses the _"RobConfig"_ configuration object that is obtained by calling _config.get_child("RobConfig")_ function with the given **boost::property_tree::ptree** instance. The _"RobConfig"_ parameter is available only if consumer configuration object was linked to an instance of the **SwRodRob** class via its _Contains_ relationship. In this case this parameter will contain the corresponding **SwRodRob** configuration. For an instance of the consumer that is attached to the **SwRodConfiguration** object the _"RobConfig"_ configuration parameter is not be set and an attempt to call the _config.get_child("RobConfig")_ function will yield an exception.

### Registering Custom Interface Implementation with the SW ROD
Finally the SW ROD application has to be made aware of the new interface implementation in order to be able to use it at run time. To achieve this the corresponding interface implementation class has to be registered with the **swrod::Core** singleton as shown in the following example.

~~~cpp
using namespace swrod;

namespace {
    Factory<ROBFragmentConsumer>::Registrator __reg_custom plugin_(
            "ROBFragmentCounter",
            [](const boost::property_tree::ptree& config, const Core& core) {
                return std::make_shared<ROBFragmentCounter>(config, core);
            });
}
~~~

This code creates a new factory object that will be used for creating new instances of the **ROBFragmentCounter** class. The factory will be registered with the _"ROBFragmentCounter"_ name. This name will be used in the OKS configuration as described in the next section. Note that the base class of the new component (**ROBFragmentConsumer**) must be used as template parameter of the **Factory** class.

This code has to be compiled and linked together with the **ROBFragmentCounter** class implementation into a shared library that will be dynamically loaded by SW ROD application. Let's assume that such a library is called **libswrod_custom_test.so**. To make this library known to the SW ROD application a new instance of the **SwRodPluginLib** class has to be created in the corresponding OKS configuration and the shared library name has to be set to its **LibraryName** attribute. One can either use a full path-name of the shared library or use a short file-name and add the library location to the _LD_LIBRARY_PATH_ environment variable. Finally the new instance of the **SwRodPluginLib** class has to be linked with the **SwRodConfiguration** object via the _Plugins_ relationship.

## Testing SW ROD Custom Processing library
A custom implementation of the **DataInput** interface can be used to validate detector specific custom processing plugins. This chapter explains how that can be done.

### Implementing internal data generator for SW ROD
The simplest way of providing custom input to SW ROD is to implement internal data generator that produces data with desired formatting in memory of the SW ROD application. The **swrod** package contains an example of such generator in **test/core/InternalDataGenerator.h(cpp)** files. One can customize internal data generation by declaring a new class that inherits **swrod::test::InternalDataGenerator** and overrides **generatePacket(InputLinkId link, uint32_t l1id, uint16_t bcid)** virtual function. This function is called for every new packet to be produced and is expected to make a new packet and pass it to the **dataReceived(InputLinkId link, const uint8_t * data, uint32_t size, uint8_t status)** function. The following example shows how this can be done.

~~~cpp
 void MyDataGenerator::generatePacket(InputLinkId link, uint32_t l1id, uint16_t bcid) {
    // For efficiency m_packet memory block had been preallocated in the constructor
    // Here we just calculate the size of the new packet. 
    // It must not exceed the size of the m_packet memory block
    uint32_t new_packet_size = ...;  
    
    // set TTC values to the appropriate places of the new packet, for example
    *((uint32_t*) (m_packet + 2)) = l1id;
    *((uint16_t*) (m_packet + 6)) = bcid;   
    
    dataReceived(link, m_packet, new_packet_size, 0);
}
~~~

Note that if a custom implementation produces packets of fixed size it can calculate packet size only once in the class constructor. A custom implementation that needs to generate packets of varying size has to calculate a new packet size each time a new packet is produced. Finally the new packet has to be passed to the **dataReceived()** function that in turn will pass it to the SW ROD fragment builder. 

The **swrod::test::InternalDataGenerator** class provides another virtual function that is named **beforeStart()**. This function is called each time a new run is about to be started and can be used to reset internal counters of the custom data generator. 

The new data generator has to be advertised to the SW ROD by creating and registering a new object factory as shown by the following example.

~~~cpp
namespace {
    Factory<DataInput>::Registrator __reg__(
            "MyDataGenerator",
            [](const boost::property_tree::ptree& config, const Core& ) {
                return std::make_shared<MyDataGenerator>(config);
            });
}
~~~

Finally the new generator class has to be compiled to a shared library and the library name has to be set to the **LibraryName** attribute of the new instance of the **SwRodPluginLib** object created in OKS configuration. This OKS object has to be linked with the **SwRodConfiguration** via the _Plugins_ relationship. This will make the new input method implementation known to the SW ROD application. 

In order to use the new generator a new instance of the **SwRodInternalDataGenerator** class has to be created and its **Type** attribute has to be set to the same _"MyDataGenerator"_ string, that was used for registering the corresponding class with the SW ROD plugins factory. Finally this object has to be linked either with the  **SwRodConfiguration** or **SwRodModule** via the **InputMethod** relationship.

> **Note**: An internal data generator has to be used in conjunction with **InternalL1AGenerator** class provided by the **swrod** package. The **InternalL1AGenerator** produces L1A packets which are used as seeds for data packets generation.


